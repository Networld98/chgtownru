<?php

add_action( 'init', 'load_slider' );
function load_slider() {
	if ( !is_admin() ) {
		wp_register_script( 'slider', THEME_URL . '/js/slider/basic-jquery-slider.min.js', array( 'jquery' ), '1', true );
		wp_enqueue_script( 'slider' );
		wp_register_style( 'slider', THEME_URL . '/js/slider/basic-jquery-slider.css', false, '1', 'all' );
		wp_enqueue_style( 'slider' );
	}
}

?>