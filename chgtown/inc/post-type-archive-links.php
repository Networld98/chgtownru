<?php

class zelenin_post_type_archive_link {

	function __construct() {
		add_action( 'admin_init', array( $this, 'add_meta_box' ) );
		add_action( 'wp_ajax_add-post-type-archive-links', array( $this, 'ajax_add_post_type' ) );
		add_filter( 'wp_setup_nav_menu_item',  array( $this, 'setup_archive_item' ) );
		add_filter( 'wp_nav_menu_objects', array( $this, 'maybe_make_current' ) );
		add_action( 'admin_head-nav-menus.php', array( $this, 'add_script' ) );
	}

	function add_meta_box() {
		add_meta_box( 'post-type-archives', 'Post types', array( $this, 'metabox' ), 'nav-menus', 'side', 'low' );
	}

	function metabox() {
		global $nav_menu_selected_id;
		$post_types = get_post_types( array( 'public' => true, '_builtin' => false ), 'object' ); ?>

			<ul id="post-type-archive-checklist">
				<?php foreach ( $post_types as $type ) { ?>
					<li><label><input type="checkbox" value="<?php echo esc_attr( $type->name ); ?>"> <?php echo esc_attr( $type->labels->name ); ?> </label></li>
				<?php } ?>
			</ul>
			<p class="button-controls">
				<span class="add-to-menu"><input type="submit" id="submit-post-type-archives" <?php disabled( $nav_menu_selected_id, 0 ); ?> value="<?php esc_attr_e( 'Add to menu' ); ?>" name="add-post-type-menu-item" class="button-secondary submit-add-to-menu"></span>
			</p>
	<?php }

	function add_script() { ?>
		<script>
			(function($) {
				$(function() {

					$( '#submit-post-type-archives' ).on( 'click', function(event) {

						event.preventDefault();
						var postTypes = [];

						$( '#post-type-archive-checklist li :checked' ).each(function() {
							postTypes.push( $(this).val() );
						});

						$.post(
							ajaxurl,
							{
								action: 'add-post-type-archive-links',
								post_types: postTypes
							},
							function( response ) {
								$( '#menu-to-edit' ).append(response);
							}
						);

					});

				});

			})(jQuery);
		</script>
	<?php }

	function ajax_add_post_type() {

		if ( !current_user_can( 'edit_theme_options' ) ) die('-1');
		require_once ABSPATH . 'wp-admin/includes/nav-menu.php';
		if ( empty( $_POST['post_types'] ) ) exit;

		$item_ids = array();
		foreach ( (array) $_POST['post_types'] as $post_type ) {

			$post_type_obj = get_post_type_object( $post_type );

			if( !$post_type_obj ) continue;

			$menu_item_data= array(
				'menu-item-title' => esc_attr( $post_type_obj->labels->name ),
				'menu-item-type' => 'post_type_archive',
				'menu-item-object' => esc_attr( $post_type ),
				'menu-item-url' => get_post_type_archive_link( $post_type ),
			);

			$item_ids[] = wp_update_nav_menu_item( 0, 0, $menu_item_data );

		}

		if ( is_wp_error( $item_ids ) ) die('-1');

		foreach ( (array) $item_ids as $menu_item_id ) {

			$menu_obj = get_post( $menu_item_id );

			if ( !empty( $menu_obj->ID ) ) {
				$menu_obj = wp_setup_nav_menu_item( $menu_obj );
				$menu_obj->label = $menu_obj->title;
				$menu_items[] = $menu_obj;
			}

		}

		if ( !empty( $menu_items ) ) {

			$args = array(
				'after' => '',
				'before' => '',
				'link_after' => '',
				'link_before' => '',
				'walker' => new Walker_Nav_Menu_Edit
			);
			echo walk_nav_menu_tree( $menu_items, 0, (object) $args );

		}
		exit;

	}

	function setup_archive_item( $menu_item ) {

		if( $menu_item->type != 'post_type_archive' ) return $menu_item;
		$post_type = $menu_item->object;
		$menu_item->url =get_post_type_archive_link( $post_type );
		return $menu_item;

	}

	function maybe_make_current( $items ) {

		foreach ( $items as $item ) {

			if( 'post_type_archive' != $item->type ) continue;
			$post_type = $item->object;
			if( !is_post_type_archive( $post_type ) && !is_singular( $post_type ) ) continue;
			$item->current = true;
			$item->classes[] = 'current-menu-item';
			$_anc_id = (int) $item->db_id;
			$active_ancestor_item_ids = array();

			while ( ( $_anc_id = get_post_meta( $_anc_id, '_menu_item_menu_item_parent', true ) ) && !in_array( $_anc_id, $active_ancestor_item_ids ) ) {
				$active_ancestor_item_ids[] = $_anc_id;
			}

			foreach ( $items as $key => $parent_item ) {

				$classes = (array) $parent_item->classes;

				if ( $parent_item->db_id == $item->menu_item_parent ) {
					$classes[] = 'current-menu-parent';
					$items[$key]->current_item_parent = true;
				}

				if ( in_array( intval( $parent_item->db_id ), $active_ancestor_item_ids ) ) {
					$classes[] = 'current-menu-ancestor';
					$items[$key]->current_item_ancestor = true;
				}

				$items[$key]->classes = array_unique( $classes );

			}

		}

		return $items;

	}

}

new zelenin_post_type_archive_link();

?>