<?php

add_action('wp_ajax_refresh_captcha', 'refresh_captcha_handler');
add_action('wp_ajax_nopriv_refresh_captcha', 'refresh_captcha_handler');
function refresh_captcha_handler(){
    ob_start();
    include_once(get_template_directory().'/inc/captcha/generate_captcha.php');
    echo base64_encode(ob_get_clean());
    die(1);
}