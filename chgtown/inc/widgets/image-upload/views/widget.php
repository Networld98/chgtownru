<?php
$title = strip_tags( $instance['title'] );
$image_id = absint( $instance['image_id'] );
$link = strip_tags( $instance['link'] );

if ( $image_id ) :
	echo '<div class="widget">';
	if ( !empty( $title ) ) echo '<h2 class="post-title widget-title">' . $title . '</h2>';
	$image = wp_get_attachment_image_src( $image_id, 'full' );
	$image_url = $image[0];
	echo '<div class="post-content image-block">';
	if ( !empty( $link ) ) echo '<a href="' . $link . '" target="_blank">';
	echo '<img src="' . $image_url . '">';
	if ( !empty( $link ) ) echo '</a>';
	echo '</div>';
	echo '</div>';
endif;