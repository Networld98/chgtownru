<?php

class Fields
{
	private $prefix = null;
	private $type; // settings-page, meta_box, term_meta
	protected $slug;
	protected $name;
	protected $object_type;
	private $dev_mode = false;
	protected $sections = array();
	protected $fields = array();

	public function __construct() {}

	public function init()
	{
		add_action( 'admin_init', array( $this, 'registerSections' ) );
		add_action( 'admin_init', array( $this, 'registerFields' ) );

		// todo: check needed scripts
		add_action( 'admin_print_scripts', array( $this, 'addFieldsScripts' ) );
		add_action( 'admin_print_footer_scripts', array( $this, 'addFieldsFooterScripts' ) );

		// todo: check
		wp_register_style( 'jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/flick/jquery-ui.min.css', array( 'colors' ), '1', 'all' );
	}

	public function setType( $type )
	{
		$this->type = $type;
		return $this;
	}

	public function setId( $id )
	{
		$this->slug = $id;
		return $this;
	}

	public function setPrefix( $prefix )
	{
		$this->prefix = $prefix;
		return $this;
	}

	public function setName( $name )
	{
		$this->name = $name;
		return $this;
	}

	protected function setObjectType( $object_type )
	{
		$this->object_type = $object_type;
	}

	public function setDeveloperMode()
	{
		$this->dev_mode = true;
		return $this;
	}

	public function addSection( $section )
	{
		$this->sections[$section['id']] = $section['label'];
		return $this;
	}

	public function registerSections()
	{
		foreach ( $this->sections as $section_key => $section_value ) {
			add_settings_section( $section_key, $section_value, array( $this, 'fields_section_callback' ), $this->slug );
		}
	}

	public function registerFields()
	{
		foreach ( $this->fields as $section_key => $section_value ) {
			foreach ( $section_value as $key => $field ) {
				register_setting( $this->slug, $field['label'], array( $this, 'fields_sanitize_callback' ) );
				add_settings_field( $key, $field['label'], array( $this, 'renderField'	), $this->slug, $section_key, array( 'setting' => $field ) );
			}
		}
	}

	private function addField( $field )
	{
		$field['id'] = $this->prefix . $field['id'];
		$this->fields[$field['section']][$field['id']] = $field;
	}

	public function addText( $field )
	{
		$field['type'] = 'text';
		$this->addField( $field );
	}

	public function addPassword( $field )
	{
		$field['type'] = 'password';
		$this->addField( $field );
	}

	public function addWpeditor( $field )
	{
		$field['type'] = 'wpeditor';
		$this->addField( $field );
	}

	public function addTextarea( $field )
	{
		$field['type'] = 'textarea';
		$this->addField( $field );
	}

	public function addCheckbox( $field )
	{
		$field['type'] = 'checkbox';
		$this->addField( $field );
	}

	public function addSelect( $field )
	{
		$field['type'] = 'select';
		$this->addField( $field );
	}

	public function addRadio( $field )
	{
		$field['type'] = 'radio';
		$this->addField( $field );
	}

	public function addCheckboxGroup( $field )
	{
		$field['type'] = 'checkbox_group';
		$this->addField( $field );
	}

	public function addUsersCheckboxGroup( $field )
	{
		$field['type'] = 'users_checkbox_group';
		$this->addField( $field );
	}

	public function addTermSelect( $field )
	{
		$field['type'] = 'term_select';
		$this->addField( $field );
	}

	public function addTermRadio( $field )
	{
		$field['type'] = 'term_radio';
		$this->addField( $field );
	}

	public function addRepeatableText( $field )
	{
		$field['type'] = 'repeatable_text';
		$this->addField( $field );
	}

	public function addDate( $field )
	{
		$field['type'] = 'date';
		$this->addField( $field );
	}

	public function addPostSelect( $field )
	{
		$field['type'] = 'post_select';
		$this->addField( $field );
	}

	public function addImage( $field )
	{
		$field['type'] = 'image';
		$this->addField( $field );
	}

	public function addTermFields()
	{
		$this->render( 'add_term_fields' );
	}

	public function editTermFields()
	{
		$this->render( 'edit_term_fields' );
	}

	public function setTermMetaTable()
	{
		global $wpdb;
		$table_name = 'taxonomymeta';
		$wpdb->$table_name = $wpdb->prefix . $table_name;
	}

	private function installTermMetaTable()
	{
		global $wpdb;
		if ( _get_meta_table( 'taxonomy' ) ) {
			$wpdb->query( 'CREATE TABLE `' . $wpdb->taxonomymeta . '` (
			`meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
			`taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT "0",
			`meta_key` varchar(255) DEFAULT NULL,
			`meta_value` longtext,
			PRIMARY KEY ( `meta_id` ),
			KEY `taxonomy_id` ( `taxonomy_id` ),
			KEY `meta_key` ( `meta_key` )
			)' );
		}
	}

	public function removeTaxonomyMetaBox()
	{
		foreach ( $this->fields as $tab_fields ) {
			foreach ( $tab_fields['content'] as $field ) {
				$taxonomy = ( $field['type'] == 'term_select' || $field['type'] == 'term_radio' ) ? $field['taxonomy'] : 'category';
				remove_meta_box( $field . 'div', $this->object_type, 'normal' );
			}
		}
	}

	public function fields_section_callback()
	{
		echo 'fields_section_callback';
	}

	public function fields_sanitize_callback()
	{
		echo 'settings_sanitize_callback';
	}

	public function renderField( $field, $value )
	{
		switch ($field['type']) {

			case 'text': ?>
				<div class="field"><input class="large-text" type="text" name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>" value="<?php echo $value; ?>"></div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true );<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>get_option( '<?php echo $field['id']; ?>' );<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>get_term_meta( $term_id, '<?php echo $field['id']; ?>', true );<?php } ?>
					</div>
				<?php endif;
				break;

			case 'password': ?>
				<div class="field"><input class="large-text" type="password" name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>" value="<?php echo $value; ?>"></div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true );<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>get_option( '<?php echo $field['id']; ?>' );<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>get_term_meta( $term_id, '<?php echo $field['id']; ?>', true );<?php } ?>
					</div>
				<?php endif;
				break;

			case 'wpeditor': ?>
				<div class="field"><?php wp_editor( $value, $field['id'], $field['settings'] ); ?></div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<style>.ed_button{width:auto!important}</style>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true );<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>get_option( '<?php echo $field['id']; ?>' );<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>get_term_meta( $term_id, '<?php echo $field['id']; ?>', true );<?php } ?>
					</div>

				<?php endif;
				break;

			case 'textarea': ?>
				<div class="field"><textarea class="textarea large-text" name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>"><?php echo $value; ?></textarea></div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true );<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>get_option( '<?php echo $field['id']; ?>' );<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>get_term_meta( $term_id, '<?php echo $field['id']; ?>', true );<?php } ?>
					</div>
				<?php endif;
				break;

			case 'checkbox': ?>
				<div class="field"><input type="checkbox" name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>" <?php checked( $value, 1, true ); ?>></div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<style>#<?php echo $field['id']; ?>{width:auto!important}</style>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>if ( get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true ) ) { } else { };<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>if ( get_option( '<?php echo $field['id']; ?>' ) ) { } else { }<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>if ( get_term_meta( $term_id, '<?php echo $field['id']; ?>', true ) ) { } else { }<?php } ?>
					</div>
				<?php endif;
				break;

			case 'select': ?>
				<div class="field">
					<select name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>">
						<option>Select</option>
						<?php foreach ( $field['options'] as $select_key => $select_value ) { ?>
							<option <?php selected( $select_key, $value, true ); ?> value="<?php echo $select_key; ?>"><?php echo $select_value; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true );<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>get_option( '<?php echo $field['id']; ?>' );<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>get_term_meta( $term_id, '<?php echo $field['id']; ?>', true );<?php } ?>
					</div>
				<?php endif;
				break;

			case 'radio': ?>
				<div class="field">
					<?php foreach ( $field['options'] as $radio_key => $radio_value ) { ?>
						<div><input type="radio" name="<?php echo $field['id']; ?>" id="<?php echo $field['id'] . '_' . $radio_key; ?>" value="<?php echo $radio_key; ?>" <?php checked( $radio_key, $value, true ); ?>> <label for="<?php echo $field['id'] . '_' . $radio_key; ?>"><?php echo $radio_value; ?></label></div>
					<?php } ?>
				</div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true );<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>get_option( '<?php echo $field['id']; ?>' );<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>get_term_meta( $term_id, '<?php echo $field['id']; ?>', true );<?php } ?>
					</div>
				<?php endif;
				break;

			case 'checkbox_group': ?>
				<?php if ( empty( $value ) ) $value = array(); ?>
				<div class="field">
				<?php foreach ( $field['options'] as $check_key => $check_value ) { ?>
					<div><input type="checkbox" value="<?php echo $check_key; ?>" name="<?php echo $field['id']; ?>[]" id="<?php echo $field['id'] . '_' . $check_key; ?>" <?php checked( $check_key, in_array( $check_key, $value ) ? $check_key : '', true ); ?>> <label for="<?php echo $field['id'] . '_' . $check_key; ?>"><?php echo $check_value; ?></label></div>
				<?php } ?>
				</div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<style>.field input[type=checkbox]{width:auto!important;float:left;}</style>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>foreach ( get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true ) as $field ) {}<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>foreach ( get_option( '<?php echo $field['id']; ?>' ) as $field ) {}<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>foreach ( get_term_meta( $term_id, '<?php echo $field['id']; ?>', true ) as $field ) {}<?php } ?>
					</div>
				<?php endif;
				break;

			case 'users_checkbox_group': ?>
				<?php
				global $wp_roles;
				if ( empty( $value ) ) $value = array();
				?>
				<div class="field">
				<?php foreach ( $wp_roles->get_names() as $role => $name ) {
					$users = get_users( 'role=' . $role );

					if ( !empty( $users ) ) {
						echo '<h4>' . $name . '</h4>';
						foreach ( $users as $user ) {
							echo '<div><input type="checkbox" value="' . $user->user_login . '" name="' . $field['id'] . '[]" id="' . $user->user_login . '" ' . checked( $user->user_login, in_array( $user->user_login, $value ) ? $user->user_login : '', false ) . '> <label for="' . $user->user_login . '">' . $user->user_login . ' (' . $user->display_name . ')</label></div>';
						}
					}
				} ?>
				</div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>foreach ( get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true ) as $field ) {}<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>foreach ( get_option( '<?php echo $field['id']; ?>' ) as $field ) {}<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>foreach ( get_term_meta( $term_id, '<?php echo $field['id']; ?>', true ) as $field ) {}<?php } ?>
					</div>
				<?php endif;
				break;

			case 'term_select':
			case 'term_radio': ?>
				<?php
				$field['taxonomy'] = isset( $field['taxonomy'] ) ? $field['taxonomy'] : 'category';
				$get_terms = get_terms( $field['taxonomy'], 'hide_empty=0' );
				if ( $this->type == 'meta_box' ) {
					$get_the_terms = get_the_terms( $object_id, $field['taxonomy'] );
					$value = $get_the_terms['0']->term_id;
				}
				?>
				<div class="field">
					<?php if ( $field['type'] == 'term_select' ) : ?>
						<select name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>">
							<option>Select</option>
							<?php foreach ( $get_terms as $get_term ) { ?>
								<option value="<?php echo $get_term->term_id; ?>" <?php selected( $get_term->term_id, $value,true ); ?>><?php echo $get_term->name; ?></option>
							<?php } ?>
						</select>
					<?php endif; ?>
					<?php if ( $field['type'] == 'term_radio' ) : ?>
						<?php foreach ( $get_terms as $get_term ) { ?>
							<div>
								<input type="radio" name="<?php echo $field['id']; ?>" id="<?php echo $get_term->term_id; ?>" value="<?php echo $get_term->term_id; ?>" <?php checked( $get_term->term_id, $value, false ); ?>>
								<label for="<?php echo $get_term->term_id; ?>"><?php echo $get_term->name; ?></label>
							</div>
						<?php } ?>
					<?php endif; ?>
				</div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<?php if ( $this->dev_mode ) { ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>get_the_terms( $post->ID, '<?php echo $field['taxonomy']; ?>' );<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>get_option( '<?php echo $field['id']; ?>' );<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>get_term_meta( $term_id, '<?php echo $field['id']; ?>', true );<?php } ?>
					</div>
				<?php }
				break;

			case 'repeatable_text': ?>
				<div class="field">
					<p><a class="repeatable-add button" href="#">+</a></p>
					<div id="<?php echo $field['id']; ?>-repeatable" class="custom_repeatable">
						<?php
						if ( $value ) {
							foreach ( $value as $row ) { ?>
								<div><span class="sort hndle">|||</span><input type="text" name="<?php echo $field['id']; ?>[]" class="repeatable regular-text" value="<?php echo $row; ?>"/><a class="repeatable-remove button" href="#">-</a></div>
							<?php }
						} else { ?>
							<div><span class="sort hndle">|||</span><input type="text" name="<?php echo $field['id']; ?>[]" class="repeatable regular-text" value=""/><a class="repeatable-remove button" href="#">-</a></div>
						<?php }
						?>
					</div>
				</div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>foreach ( get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true ) as $field ) {}<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>foreach ( get_option( '<?php echo $field['id']; ?>' ) as $field ) {}<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>foreach ( get_term_meta( $term_id, '<?php echo $field['id']; ?>', true ) as $field ) {}<?php } ?>
					</div>
				<?php endif;
				break;

			case 'date': ?>
				<div class="field">
					<input type="text" class="<?php echo $field['id']; ?>-datepicker widefat" name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>" value="<?php echo $value; ?>">
				</div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true );<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>get_option( '<?php echo $field['id']; ?>' );<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>get_term_meta( $term_id, '<?php echo $field['id']; ?>', true );<?php } ?>
					</div>
				<?php endif; ?>
				<script>
					(function ( $ ) {
						$( function () {

							$( '.<?php echo $field['id']; ?>-datepicker' ).datepicker( {
								dateFormat: 'dd.mm.yy',
								<?php if ( $field['locale'] == 'ru' ) { ?>
								dayNames:
								            [
									            'Воскресенье',
									            'Понедельник',
									            'Вторник',
									            'Среда',
									            'Четверг',
									            'Пятница',
									            'Суббота'
								            ],
								dayNamesMin:
								            [
									            'Во',
									            'Пн',
									            'Вт',
									            'Ср',
									            'Чт',
									            'Пт',
									            'Сб'
								            ],
								dayNamesShort:
								            [
									            'Вос',
									            'Пон',
									            'Вто',
									            'Сре',
									            'Чет',
									            'Пят',
									            'Суб'
								            ],
								monthNames:
								            [
									            'Январь',
									            'Февраль',
									            'Март',
									            'Апрель',
									            'Май',
									            'Июнь',
									            'Июль',
									            'Август',
									            'Сентябрь',
									            'Октябрь',
									            'Ноябрь',
									            'Декабрь'
								            ],
								monthNamesShort:
								                   [
									                   'Янв',
									                   'Фев',
									                   'Мар',
									                   'Апр',
									                   'Май',
									                   'Июн',
									                   'Июл',
									                   'Авг',
									                   'Сен',
									                   'Окт',
									                   'Ноя',
									                   'Дек'
								                   ],
								<?php } ?>
								changeMonth:       true,
								changeYear:        true,
								showOtherMonths:   true,
								selectOtherMonths: true,
								defaultDate:       +7,
								firstDay:          1,
								numberOfMonths:    3,
								showButtonPanel:   true
							} );

						} );
					})( jQuery );
				</script><?php
				break;

			case 'post_select': ?>
				<?php
				$get_posts = get_posts( array(
					'post_type' => $field['post_type'],
					'posts_per_page' => -1
				) );
				?>
				<div class="field">
					<select class="widefat" name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>">
						<option value="">Select</option>
						<?php foreach ( $get_posts as $get_post ) { ?>
							<option value="<?php echo $get_post->ID; ?>" <?php selected( $get_post->ID, $value, true ); ?>><?php echo $get_post->post_title; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true );<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>get_option( '<?php echo $field['id']; ?>' );<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>get_term_meta( $term_id, '<?php echo $field['id']; ?>', true );<?php } ?>
					</div>
				<?php endif;
				break;

			case 'image':
				$image = THEME_URL . '/inc/images/image.png'; ?>
				<div class="field">
					<span class="custom_default_image" style="display:none"><?php echo $image; ?></span>
					<?php
					if ( $value ) {
						$image = wp_get_attachment_image_src( $value, 'thumbnail' );
						$image = $image[0];
					}
					?>
					<input type="hidden" name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>" class="custom_upload_image" value="<?php echo $value; ?>">
					<img src="<?php echo $image; ?>" id="<?php echo $field['id']; ?>_img" class="custom_upload_image_new_button custom_preview_image">
					<input type="button" class="custom_upload_image_new_button button" value="Choose image">
					<a href="#" class="custom_clear_image_button">Remove image</a>
				</div>
				<div class="description"><?php echo $field['desc']; ?></div>
				<style>.field input[type=button]{width:auto!important;}</style>
				<?php if ( $this->dev_mode ) : ?>
					<div class="dev">
						<?php if ( $this->type == 'meta_box' ) { ?>$image_id = get_post_meta( $post->ID, '<?php echo $field['id']; ?>', true ); $image_src = wp_get_attachment_image_src( $image_id, 'thumbnail' ); $image_url = $image_src[0];<?php } ?>
						<?php if ( $this->type == 'settings-page' ) { ?>$image_id = get_option( '<?php echo $field['id']; ?>' ); $image_src = wp_get_attachment_image_src( $image_id, 'thumbnail' ); $image_url = $image_src[0];<?php } ?>
						<?php if ( $this->type == 'term_meta' ) { ?>$image_id = get_term_meta( $term_id, '<?php echo $field['id']; ?>', true ); $image_src = wp_get_attachment_image_src( $image_id, 'thumbnail' ); $image_url = $image_src[0];<?php } ?>
					</div>
				<?php endif;
				break;

			case 'heading': // не работает
				//echo '<h3>' . $field['label'] . '</h3>';
				//echo '<div class="description">' . $field['desc'] . '</div>';
				//if ( $this->debug == 1 ) echo '<div class="debug">get_option( \'' . $field['id'] . '\' );</div>';
				break;

			case 'paragraph': // не работает
				//echo '<div class="option"><input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $value . '"></div>';
				// echo '<div class="description">' . $field['desc'] . '</div>';
				// if ( $this->debug == 1 ) echo '<div class="debug">get_option( \'' . $field['id'] . '\' );</div>';
				break;



			case 'hidden': // не работает
				echo '<div class="field"><input type="hidden" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $value . '"></div>';
				echo '<div class="description">' . $field['desc'] . '</div>';
				if ( $this->dev_mode ) {
					echo '<div class="dev">';
					if ( $this->type == 'meta_box' ) echo 'get_post_meta( $post->ID, \'' . $field['id'] . '\', true );';
					if ( $this->type == 'settings-page' ) echo 'get_option( \'' . $field['id'] . '\' );';
					if ( $this->type == 'term_meta' ) echo 'get_term_meta( $term_id, \'' . $field['id'] . '\', true );';
					echo '</div>';
				}
				break;

			case 'file': // не работает
				//echo '<div class="option"><input type="password" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $value . '"></div>';

				//echo '<div class="description">' . $field['desc'] . '</div>';
				//if ( $this->debug == 1 ) echo '<div class="debug">get_option( \'' . $field['id'] . '\' );</div>';
				break;



			case 'time':
				echo '<div class="field">';
				echo '<input type="text" class="' . $field['id'] . '-timepicker widefat" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $value . '">';
				echo '</div>';
				echo '<div class="description">' . $field['desc'] . '</div>';
				if ( $this->dev_mode ) {
					echo '<div class="dev">';
					if ( $this->type == 'meta_box' ) echo 'get_post_meta( $post->ID, \'' . $field['id'] . '\', true );';
					if ( $this->type == 'settings-page' ) echo 'get_option( \'' . $field['id'] . '\' );';
					if ( $this->type == 'term_meta' ) echo 'get_term_meta( $term_id, \'' . $field['id'] . '\', true );';
					echo '</div>';
				} ?>

				<script>
					(function ( $ ) {
						$( function () {
							$( '.<?php echo $field['id']; ?>-timepicker' ).datetimepicker();
						} );
					})( jQuery );
				</script><?php
				break;

			case 'slider':
				$value = $value != '' ? $value : '0';
				echo '<div class="field">';
				echo '<div id="' . $field['id'] . '-slider"></div><input class="slider-input" type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $value . '">';
				echo '</div>';
				echo '<div class="description">' . $field['desc'] . '</div>';
				if ( $this->dev_mode ) {
					echo '<div class="dev">';
					if ( $this->type == 'meta_box' ) echo 'get_post_meta( $post->ID, \'' . $field['id'] . '\', true );';
					if ( $this->type == 'settings-page' ) echo 'get_option( \'' . $field['id'] . '\' );';
					if ( $this->type == 'term_meta' ) echo 'get_term_meta( $term_id, \'' . $field['id'] . '\', true );';
					echo '</div>';
				} ?>

				<style>
					.ui-slider .ui-slider-handle {
						z-index: 1 !important;
					}
				</style>
				<script>
					(function ( $ ) {
						$( function () {

							<?php if ($value == '') $value = $field['min']; ?>

							$( "#<?php echo $field['id']; ?>-slider" ).slider( {
								value: <?php echo $value; ?>,
								min: <?php echo $field['min']; ?>,
								max: <?php echo $field['max']; ?>,
								step: <?php echo $field['step']; ?>,
								slide: function ( event, ui ) {
									jQuery( "#<?php echo $field['id']; ?>" ).val( ui.value );
								}
							} );

						} );
					})( jQuery );
				</script><?php
				break;








			case 'repeatable_images':
				$image = THEME_URL . '/inc/images/image.png';
				echo '<div class="field">';
				echo '<span class="custom_default_image" style="display:none">' . $image . '</span>';
				echo '<a class="repeatable-add button" href="#">+</a><div id="' . $field['id'] . '-repeatable" class="custom_repeatable">';
				if ( $value ) {
					foreach ( $value as $image_id ) {
						$image = wp_get_attachment_image_src( $image_id, 'thumbnail' );
						$image = $image[0];
						echo '<div><span class="sort hndle">|||</span><input name="' . $field['id'] . '[]" type="hidden" class="custom_upload_image repeatable" value="' . $image_id . '" /><img src="' . $image . '" class="custom_upload_image_button custom_preview_image"><input class="custom_upload_image_button button" type="button" value="Choose image"><a class="repeatable-remove button" href="#">-</a></div>';
					}
				} else {
					echo '<div><span class="sort hndle">|||</span><input name="' . $field['id'] . '[]" type="hidden" class="custom_upload_image repeatable" value=""><img class="custom_upload_image_button custom_preview_image" src="' . $image . '"><input class="custom_upload_image_button button" type="button" value="Choose image" /><a class="repeatable-remove button" href="#">-</a></div>';
				}
				echo '</div>';
				echo '</div>';
				echo '<div class="description">' . $field['desc'] . '</div>';
				if ( $this->dev_mode ) {
					echo '<div class="dev">';
					if ( $this->type == 'meta_box' ) echo 'foreach ( get_post_meta( $post->ID, \'' . $field['id'] . '\', true ) as $image_id ) { $image_src = wp_get_attachment_image_src( $image_id, \'thumbnail\' ); $image_url = $image_src[0];}';
					if ( $this->type == 'settings-page' ) echo 'foreach ( get_option( \'' . $field['id'] . '\' ) as $image_id ) { $image_src = wp_get_attachment_image_src( $image_id, \'thumbnail\' ); $image_url = $image_src[0];}';
					if ( $this->type == 'term_meta' ) echo 'foreach ( get_term_meta( $term_id, \'' . $field['id'] . '\', true ) as $image_id ) { $image_src = wp_get_attachment_image_src( $image_id, \'thumbnail\' ); $image_url = $image_src[0];}';
					echo '</div>';
				}
				break;

			case 'color':
				echo '<div class="field">';
				echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $value . '"><a href="#" class="pickcolor ' . $field['id'] . '" id="' . $field['id'] . '-example"></a><input type="button" class="pickcolor button ' . $field['id'] . '" value="Select a color"><div id="' . $field['id'] . 'Div" style="z-index:100;background:#eee;border:1px solid #ccc;position:absolute;display:none;"></div>';
				echo '</div>';
				echo '<div class="description">' . $field['desc'] . '</div>';
				if ( $this->dev_mode ) {
					echo '<div class="dev">';
					if ( $this->type == 'meta_box' ) echo 'get_post_meta( $post->ID, \'' . $field['id'] . '\', true );';
					if ( $this->type == 'settings-page' ) echo 'get_option( \'' . $field['id'] . '\' );';
					if ( $this->type == 'term_meta' ) echo 'get_term_meta( $term_id, \'' . $field['id'] . '\', true );';
					echo '</div>';
				} ?>

				<script>
					(function ( $ ) {
						$( function () {

							var pickColor<?php echo $field['id']; ?> = function ( a ) {
								farbtastic<?php echo $field['id']; ?>.setColor( a );
								$( '#<?php echo $field['id']; ?>' ).val( a );
								$( '#<?php echo $field['id']; ?>-example' ).css( 'background-color', a );
							};

							farbtastic<?php echo $field['id']; ?> = $.farbtastic( '#<?php echo $field['id']; ?>Div ', pickColor<?php echo $field['id']; ?> );
							pickColor<?php echo $field['id']; ?>( $( '#<?php echo $field['id']; ?>' ).val() );
							$( '.<?php echo $field['id']; ?>' ).click( function ( event ) {
								event.preventDefault();
								$( '#<?php echo $field['id']; ?>Div' ).show();

							} );
							$( document ).mousedown( function () {
								$( '#<?php echo $field['id']; ?>Div' ).hide();
							} );

						} );
					})( jQuery );
				</script><?php
				break;

			case 'colorpicker':
				echo '<div class="field">';
				echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $value . '">';
				echo '</div>';
				echo '<div class="description">' . $field['desc'] . '</div>';
				if ( $this->dev_mode ) {
					echo '<div class="dev">';
					if ( $this->type == 'meta_box' ) echo 'get_post_meta( $post->ID, \'' . $field['id'] . '\', true );';
					if ( $this->type == 'settings-page' ) echo 'get_option( \'' . $field['id'] . '\' );';
					if ( $this->type == 'term_meta' ) echo 'get_term_meta( $term_id, \'' . $field['id'] . '\', true );';
					echo '</div>';
				} ?>

				<script>
					(function ( $ ) {
						$( function () {
							$( '#<?php echo $field['id']; ?>' ).wpColorPicker( {
								defaultColor: '#222222',
								change:       function ( event, ui ) {
								},
								clear:        function () {
								},
								hide:         true,
								palettes:     true
							} );
						} );
					})( jQuery );
				</script><?php
				break;

			case 'plugins':
				$plugins = get_plugins();
				echo '<div class="option">';
				echo '<ol>';
				foreach ( $plugins as $plugin ) {
					echo '<li>';
					echo '<a href="' . $plugin['PluginURI'] . '">' . $plugin['Name'] . '</a> ' . $plugin['Version'] . ' (author ' . '<a href="' . $plugin['AuthorURI'] . '">' . $plugin['Author'] . '</a>)';
					echo '</li>';
				}
				echo '</ol>';
				echo '</div>';
				echo '<div class="description">' . $field['desc'] . '</div>';
				break;
		}

		do_action( 'custom_zelenin_fields', $field );
	}

	public function updateFields( $object_id = 0 )
	{
		if ( $this->type == 'meta_box' ) {
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return $post_id;
		}

		if ( $this->type == 'term_meta' ) {
			$object_id = isset( $_REQUEST['tag_ID'] ) ? (int)$_REQUEST['tag_ID'] : $object_id;
			$this->installTermMetaTable();
		}

		foreach ( $this->fields as $section_key => $section_value ) {
			foreach ( $section_value as $key => $field ) {

				$field_value = isset( $_POST[$field['id']] ) ? $_POST[$field['id']] : '';

				if ( is_array( $field_value ) ) {
					$field_value = array_filter( $field_value );
					$field_value = array_values( $field_value );
				}

				if ( $field['type'] == 'text' ) $field_value = esc_attr( stripslashes( $field_value ) );
				if ( $field['type'] == 'wpeditor' ) $field_value = stripslashes( $field_value );
				if ( $field['type'] == 'textarea' ) $field_value = esc_textarea( stripslashes( $field_value ) );
				if ( $field['type'] == 'checkbox' ) $field_value = ( $field_value == 'on' ) ? 1 : 0;
				// ( $field['type'] == 'slider' && is_numeric( $field_value ) ) ? $field_value = floatval( $field_value ) : $field_value = get_option( $field['id'] );
				// if ( $field['type'] == 'repeatable' ) $field_value = esc_attr( stripslashes( $field_value ) );
				if ( $field['type'] == 'color' ) $field_value = esc_attr( $field_value );

				if ( $this->type == 'meta_box' ) {
					!empty( $field_value ) ? update_post_meta( $object_id, $field['id'], $field_value ) : delete_post_meta( $object_id, $field['id'] );
					if ( $field['type'] == 'term_select' || $field['type'] == 'term_radio' ) wp_set_object_terms( $object_id, (int)$field_value, $field['taxonomy'] );
				}

				if ( $this->type == 'settings-page' ) !empty( $field_value ) ? update_option( $field['id'], $field_value ) : delete_option( $field['id'] );

				if ( $this->type == 'term_meta' ) !empty( $field_value ) ? update_term_meta( $object_id, $field['id'], $field_value ) : delete_term_meta( $object_id, $field['id'] );
			}
		}
	}

	public function addFieldsScripts()
	{
		foreach ( $this->fields as $section_key => $section_value ) {
			foreach ( $section_value as $key => $field ) {

				if ( $field['type'] == 'date' ) {
					wp_enqueue_style( 'jquery-ui' );
					wp_enqueue_script( 'jquery-ui-datepicker' );
				}

				if ( $field['type'] == 'time' ) {

					wp_enqueue_style( 'jquery-ui' );

					wp_register_script( 'timepicker', THEME_URL . '/js/timepicker/jquery-ui-timepicker-addon.js', array(
						'jquery',
						'jquery-ui-core',
						'jquery-ui-datepicker'
					), '1', false );
					wp_enqueue_script( 'timepicker' );

					wp_register_style( 'jquery-ui-timepicker-addon', THEME_URL . '/css/jquery-ui-timepicker-addon.css', array( 'colors' ), '1', 'all' );
					wp_enqueue_style( 'jquery-ui-timepicker-addon' );
				}

				if ( $field['type'] == 'slider' ) {

					wp_enqueue_style( 'jquery-ui' );
					wp_enqueue_script( 'jquery-ui-slider' );
				}

				if ( $field['type'] == 'color' ) {
					wp_enqueue_script( 'farbtastic' );
					wp_enqueue_style( 'farbtastic' );
				}

				if ( $field['type'] == 'colorpicker' ) {
					wp_enqueue_script( 'wp-color-picker' );
					wp_enqueue_style( 'wp-color-picker' );
				}

				if ( $field['type'] == 'repeatable_text' or $field['type'] == 'repeatable_images' ) {
					wp_enqueue_script( 'jquery-ui-sortable' );
				}

				if ( $field['type'] == 'repeatable_images' ) {
					wp_enqueue_style( 'thickbox' );
					wp_enqueue_script( 'thickbox' );
				}

				if ( $field['type'] == 'image' ) {
					wp_enqueue_media();
				}
			}
		}
	}

	public function addFieldsFooterScripts()
	{
		foreach ( $this->fields as $section_key => $section_value ) {
			foreach ( $section_value as $key => $field ) {

				if ( ( $field['type'] == 'repeatable_images' ) && !defined( 'FIELDS_MEDIA' ) ) {
					DEFINE( 'FIELDS_MEDIA', true ); ?>
					<script>
						(function ( $ ) {
							$( function () {

								$( '#media-items' ).bind( 'DOMNodeInserted', function () {
									$( 'input[value="Insert into Post"]' ).each(
										function () {
											$( this ).attr( 'value', 'Use This Image' );
										}
									);
								} );

								$( '.custom_upload_image_button' ).on( 'click', function ( event ) {

									event.preventDefault();
									var div = $( this ).closest( 'div' );
									var form_field = div.find( '.custom_upload_image' );
									var preview = div.find( '.custom_preview_image' );

									tb_show( '', 'media-upload.php?post_id=0&type=image&TB_iframe=1' );
									window.send_to_editor = function ( html ) {

										img_url = $( 'img', html ).attr( 'src' );
										classes = $( 'img', html ).attr( 'class' );
										id = classes.replace( /(.*?)wp-image-/, '' );
										form_field.val( id );
										preview.attr( 'src', img_url );
										tb_remove();

									}
								} );

								$( '.custom_clear_image_button' ).on( 'click', function ( event ) {
									event.preventDefault();
									var div = $( this ).closest( 'div' );
									var default_image = div.find( '.custom_default_image' ).text();
									div.find( '.custom_upload_image' ).val( '' );
									div.find( '.custom_preview_image' ).attr( 'src', default_image );
								} );

							} );
						})( jQuery );
					</script>
				<?php
				}

				if ( ( $field['type'] == 'image' || $field['type'] == 'repeatable_images_new' ) && !defined( 'FIELDS_MEDIA_NEW' ) ) {
					DEFINE( 'FIELDS_MEDIA_NEW', true ); ?>
					<script>
						(function ( $ ) {
							$( function () {

								var new_image = {
									'title':  'Upload or Choose Your Custom Image File',
									'button': 'Insert Image into Input Field'
								};

								$( document ).on( 'click', '.custom_upload_image_new_button', function ( event ) {

									event.preventDefault();

									var div = $( this ).closest( 'div' );
									console.log( div );
									var form_field = div.find( '.custom_upload_image' );
									var preview = div.find( '.custom_preview_image' );

									if ( new_media_frame ) {
										new_media_frame.open();
										return;
									}

									var new_media_frame = wp.media.frames.new_media_frame = wp.media( {
										className: 'media-frame new-media-frame',
										frame:     'select',
										multiple:  false,
										title:     new_image.title,
										library:   { type: 'image' },
										button:    { text: new_image.button }
									} );

									new_media_frame.on( 'select', function () {
										var media_attachment = new_media_frame.state().get( 'selection' ).first().toJSON();
										preview.attr( 'src', media_attachment.sizes.thumbnail.url );
										form_field.val( media_attachment.id );
									} );
									new_media_frame.open();
								} );

								$( document ).on( 'click', '.custom_clear_image_button', function ( event ) {
									event.preventDefault();
									var div = $( this ).closest( 'div' );
									var default_image = div.find( '.custom_default_image' ).text();
									div.find( '.custom_upload_image' ).val( '' );
									div.find( '.custom_preview_image' ).attr( 'src', default_image );
								} );

							} );
						})( jQuery );
					</script>
					<style>
						.custom_upload_image_new_button {
							cursor: pointer;
						}
					</style>
				<?php
				}

				if ( ( $field['type'] == 'repeatable_text' || $field['type'] == 'repeatable_images' ) && !defined( 'FIELDS_REPEATABLE' ) ) {
					DEFINE( 'FIELDS_REPEATABLE', true ); ?>
					<script>
						(function ( $ ) {
							$( function () {

								$( '.repeatable-add' ).on( 'click', function ( event ) {

									event.preventDefault();
									var option = $( this ).closest( '.field' );
									var field_location = option.find( '.custom_repeatable div:last' );
									var field = field_location.clone( true );
									var default_image = option.find( '.custom_default_image' ).text();

									$( '.custom_preview_image', field ).attr( 'src', default_image );
									$( '.repeatable', field ).val( '' );
									field.insertAfter( field_location, option );

								} );

								$( '.repeatable-remove' ).on( 'click', function ( event ) {
									event.preventDefault();
									var div = $( this ).closest( 'div' );
									div.remove();
								} );

								$( '.custom_repeatable' ).sortable( {
									opacity: 0.5,
									revert:  true,
									cursor:  'move',
									handle:  '.sort'
								} );

							} );
						})( jQuery );
					</script>
				<?php
				}
			}
		}
	}
}

if ( !function_exists( 'add_term_meta' ) ) :
	function add_term_meta( $term_id, $meta_key, $meta_value, $unique = false )
	{
		return add_metadata( 'taxonomy', $term_id, $meta_key, $meta_value, $unique );
	}
endif;

if ( !function_exists( 'delete_term_meta' ) ) :
	function delete_term_meta( $term_id, $meta_key, $meta_value = '' )
	{
		return delete_metadata( 'taxonomy', $term_id, $meta_key, $meta_value );
	}
endif;

if ( !function_exists( 'get_term_meta' ) ) :
	function get_term_meta( $term_id, $key, $single = false )
	{
		return get_metadata( 'taxonomy', $term_id, $key, $single );
	}
endif;

if ( !function_exists( 'update_term_meta' ) ) :
	function update_term_meta( $term_id, $meta_key, $meta_value, $prev_value = '' )
	{
		return update_metadata( 'taxonomy', $term_id, $meta_key, $meta_value, $prev_value );
	}
endif;