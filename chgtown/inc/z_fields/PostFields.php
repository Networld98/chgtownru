<?php

class PostFields extends Fields
{
	private $context = 'normal';
	private $priority = 'high';

	public function __construct()
	{
	}

	public function init()
	{
		add_action( 'add_meta_boxes', array( $this, 'initMetaBoxes' ) );
		add_action( 'save_post', array( $this, 'updateFields' ) );

		// todo
		foreach ( $this->fields as $section_key => $section_value ) {
			foreach ( $section_value as $key => $field ) {
				if ( $field['type'] == 'term_select' || $field['type'] == 'term_radio' ) {
					add_action( 'admin_menu', array( $this, 'removeTaxonomyMetaBox' ) );
				}
			}
		}

		parent::init();
	}

	public function initMetaBoxes()
	{
		add_meta_box( $this->slug, $this->name, array(
			$this,
			'render'
		), $this->object_type, $this->context, $this->priority );
	}

	public function setContext( $context )
	{
		$this->context = $context;
		return $this;
	}

	public function setPriority( $priority )
	{
		$this->priority = $priority;
		return $this;
	}

	public function setPostType( $post_type )
	{
		$this->setObjectType( $post_type );
		return $this;
	}

	public function render( $type = null )
	{
		?>

		<div id="settings-tabs">
			<?php if ( count( $this->sections ) > 1 ) { ?>
				<h2 class="nav-tab-wrapper">
					<?php foreach ( $this->sections as $section_key => $section_value ) { ?>
						<a class="nav-tab" href="#<?php echo $section_key; ?>"><?php echo $section_value; ?></a>
					<?php } ?>
				</h2>
				<script>
					(function ( $ ) {
						$( function () {

							var tab = $( '#settings-tabs .nav-tab' );
							var tab_content = $( '.tab_content' );
							tab_content.hide();
							tab.first().addClass( 'nav-tab-active' ).show();
							tab_content.first().show();
							tab.on( 'click', function ( event ) {
								event.preventDefault();
								tab.removeClass( 'nav-tab-active' );
								$( this ).addClass( 'nav-tab-active' );
								tab_content.hide();
								var activeTab = $( this ).attr( 'href' );
								$( activeTab ).fadeIn();
							} );

						} );
					})( jQuery );
				</script>
				<style>
					.form-table {
						border-radius: 2px;
						border: 1px solid #d3d3d3;
					}

					#settings-tabs .nav-tab-wrapper {
						padding-bottom: 0px;
					}

					#settings-tabs .nav-tab:hover, #settings-tabs .nav-tab-active {
						border-color: #CCCCCC #CCCCCC #f9f9f9;
					}
				</style>
			<?php } ?>

			<?php foreach ( $this->sections as $section_key => $section_value ) { ?>
				<div id="<?php echo $section_key; ?>" class="tab_content">
					<table class="form-table"> <?php do_settings_fields( $this->slug, $section_key ); ?></table>
				</div>
			<?php } ?>
		</div>



	<?php
	}

	public function renderField( $args )
	{
		global $post;
		$field = $args['setting'];
		$object_id = $post->ID;
		$value = get_post_meta( $object_id, $field['id'], true );

		parent::renderField( $field, $value );
	}
}