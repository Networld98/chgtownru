<?php

// echo get_user_meta( 1, 'twitter', true);
add_filter( 'user_contactmethods', 'user_contactmethods' );
function user_contactmethods( $user_contactmethod ) {
	$user_contactmethods['twitter'] = 'Twitter Username';
	$user_contactmethods['facebook'] = 'Facebook Username';
	$user_contactmethods['vkontakte'] = 'VK Username';
	unset( $user_contactmethods['aim'] );
	unset( $user_contactmethods['yim'] );
	unset( $user_contactmethods['jabber'] );
	return $user_contactmethods;
}

?>