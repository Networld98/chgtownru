<?php

	add_action( 'wp_head', 'webfonts' );
	function webfonts() {
		echo '<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,cyrillic" rel="stylesheet" type="text/css">';
	}

	add_action( 'wp_enqueue_scripts', 'zelenin_webfonts' );
	function zelenin_webfonts() {

		$myriad_pro_family = array(
			'myriadpro-bold',
			//'myriadpro-boldcond',
			//'myriadpro-boldcondit',
			//'myriadpro-boldit',
			'myriadpro-cond',
			//'myriadpro-condit',
			//'myriadpro-it',
			'myriadpro-regular',
			//'myriadpro-semibold',
			//'myriadpro-semiboldit'
		);

		foreach ( $myriad_pro_family as $myriad_pro_font ) {

			wp_register_style( $myriad_pro_font, THEME_URL . '/fonts/myriadpro/' . $myriad_pro_font . '.css', false, '1', 'all' );
			wp_enqueue_style( $myriad_pro_font );

		}

	}

?>