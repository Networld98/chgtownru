<?php
	get_header();
	get_filename();
?>

	<div id="page-main" class="container-group">
		<div id="dynamic-content" class="outline">

			<section id="content" class="container no_clone section-content-area fix">
				<div class="texture">
					<div class="content">
						<div class="content-pad">
							<div id="pagelines_content" class="one-sidebar-right fix">
								<div id="column-wrap" class="fix">
									<div id="column-main" class="mcolumn fix">
										<div class="mcolumn-pad" >

											<!-  ?php get_template_part( 'slider' ); ? -->

<table style="table-layout: fixed; width: 100%;"><tbody><tr>

<td><div style="margin: 0px 5px 0px 5px; font-size:1.4em; color:black;"><a style="color:black;" href="http://chgtown.ru/video/360tv25042016/"><img style="border:#999 1px solid; " src="/pix/copterb.jpg"><br><b>Три часа без дозаправки</b></a></div><div style="margin: 0px 5px 0px 5px; font-size:0.9em; color:#5a5a5a;">Репортаж телеканала 360 Подмосковье</div></td>

<td><div style="margin: 0px 5px 0px 5px; font-size:1.4em; color:black;"><a style="color:black;" href="http://chgtown.ru/video/hor45/"><img style="border:#999 1px solid; " src="/pix/hohorb.jpg"><br><b>Пели, поют, будут петь</b></a></div><div style="margin: 0px 5px 0px 5px; font-size:0.9em; color:#5a5a5a;">Репортаж с юбилея Академического хора Черноголовки</div></td>

<td><div style="margin: 0px 5px 0px 5px; font-size:1.4em; color:black;"><a style="color:black;" href="http://chgtown.ru/news/vanya20let/"><img style="border:#999 1px solid; " src="/pix/vanyab.jpg"><br><b>Ностальгия по панк-року</b></a></div><div style="margin: 0px 5px 0px 5px; font-size:0.9em; color:#5a5a5a;">20 лет группе «Ваня и все, все, все...»</div></td>








</tr></tbody></table>



											<section id="sb_universal" class="copy no_clone section-sb_universal">
												<div class="copy-pad">
													<ul id="list_sb_universal" class="sidebar_widgets fix"><?php if ( ! dynamic_sidebar( 'sidebar-4' ) ) : ?><?php endif; ?></ul>
													<div class="clear"></div>
												</div>
											</section>

										</div>
									</div>
								</div>

								<div id="sidebar-wrap" class="">
									<div id="sidebar1" class="scolumn" >
										<div class="scolumn-pad"></div>
									</div>

									<section id="sb_primary" class="copy no_clone section-sb_primary">
										<div class="copy-pad">
											<ul id="list_sb_primary" class="sidebar_widgets fix"><?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?><?php endif; ?></ul>
											<div class="clear"></div>
										</div>
									</section>
								</div>

							</div>
						</div>
					</div>
				</div>
			</section>

		</div>

		<div id="morefoot_area" class="container-group">
			<section id="sb_footcols" class="container no_clone section-sb_footcols fix">
				<div class="texture">
					<div class="content">
						<div class="content-pad">
							<div class="fcolumns ppfull pprow">
								<div class="fcolumns-pad fix"><?php if ( ! dynamic_sidebar( 'footer' ) ) : ?><?php endif; ?></div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="clear"></div>

	</div>

	</div>
	</div>

<?php
	get_filename();
	get_footer();
?>