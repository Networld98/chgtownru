<?php get_filename(); ?>

<footer id="footer" class="container-group">
	<div class="outline fix">
		<section id="simple_nav" class="container no_clone section-simple_nav fix">
			<div class="texture">
				<div class="content">
					<div class="content-pad">
						<div class="menu-%d0%bc%d0%b5%d0%bd%d1%8e-%d0%b2-%d0%bf%d0%be%d0%b4%d0%b2%d0%b0%d0%bb%d0%b5-container">
							<?php wp_nav_menu( array(
								'container' => 'false',
								'items_wrap' => '<ul class="inline-list simplenav font-sub">%3$s</ul>',
								'theme_location' => 'menu-1'
							) ); ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</footer>

</div>

<?php
wp_footer();
get_filename();
?>

<script type='text/javascript' src='<?php echo THEME_URL; ?>/js/script.superfish.js?ver=1.4.8'></script>
<script type='text/javascript' src='<?php echo THEME_URL; ?>/js/script.bgiframe.js?ver=2.1'></script>
<script type='text/javascript' src='<?php echo THEME_URL; ?>/js/script.cycle.js?ver=2.9994'></script>
<script type='text/javascript' src='<?php echo THEME_URL; ?>/js/script.bootstrap.min.js?ver=2.0.3'></script>
<script type='text/javascript' src='<?php echo THEME_URL; ?>/js/script.blocks.js?ver=1.0.1'></script>

</body>
</html>