<?php
get_header();
get_filename();
?>
	<div id="page-main" class="container-group">
		<div id="dynamic-content" class="outline">

			<section id="content" class="container no_clone section-content-area fix">
				<div class="texture">
					<div class="content">
						<div class="content-pad">
							<div id="pagelines_content" class="one-sidebar-right fix">
								<div id="column-wrap" class="fix">
									<div id="column-main" class="mcolumn fix">
										<div class="mcolumn-pad" >

											<section class="copy no_clone section-postnav" id="postnav">
												<div class="copy-pad">
													<div class="post-nav fix">
														<span class="previous"><?php previous_post_link('%link', $link='%title'); ?></span>
														<span class="next"><?php next_post_link('%link', $link='%title'); ?></span>
													</div>
													<div class="clear"></div>
												</div>
											</section>

											<!--<section id="postsinfo" class="copy no_clone section-postsinfo">
												<div class="copy-pad">
													<div class="current_posts_info">Вы просматриваете категорию: <strong>"<span data-term_id="9" data-taxonomy="category" data-type="input" data-filter="single_cat_title" class="fee-field">Новости</span>"</strong></div>
													<div class="clear"></div>
												</div>
											</section>-->

											<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
												<section id="postloop" class="copy no_clone section-postloop">
													<div class="copy-pad">

														<article <?php post_class( 'fpost'); ?>>
															<div class="hentry-pad">
																<section class="post-meta fix post-nothumb ">
																	<section class="bd post-header fix" >
																		<section class="bd post-title-section fix">

																			<hgroup class="post-title fix">
																				<h2 class="entry-title"><?php the_title(); ?></h2>
																			</hgroup>

																			<div class="metabar">
																				<div class="metabar-pad">
																					<em>
																						<span class="author vcard sc"><span class="fn"><?php the_author_posts_link(); ?></span></span> &middot;
																						<time class="date time published updated sc" datetime="<?php echo get_the_time( 'c' ); ?>"><?php echo get_the_time( get_option( 'date_format' ) ); ?></time>  &middot;
																						<span class="post-comments sc"><a href="<?php echo get_comments_link(); ?>"><?php comments_number( 'Комментарии: 0', 'Комментарии: 1', 'Комментарии: %' ); ?></a></span> &middot;
																						<span class="categories sc"><a href="" rel="category tag"><?php post_type_archive_title(); ?></a></span>   &middot;
																						<span class="tags sc">Содержит метки: <?php echo get_the_term_list( $post->ID, 'tag', $before = '', ', ', $after = '' ); ?></span>
                                                                                        <?php if(current_user_can('administrator')){?>
																						[<a class="post-edit-link" href="<?php echo get_edit_post_link( $post->ID ); ?>" title="Редактировать запись"><span class='editpage sc'>Редактировать</span></a>]
		                                                                                <?php }?>
                                                                                    </em>
																				</div>
																			</div>

																		</section>
																	</section>
																</section>

																<div class="entry_wrap fix">
																	<div class="entry_content">
																		<?php// the_content( 'Весь текст &rarr;' ); ?>
																		<?
                                                                        $content = get_the_content('Весь текст &rarr;');
                                                                        echo wpautop($content); ?>
                                                                        <?php if(current_user_can('administrator')){?>
                                                                            <a class="pledit" href="<?php echo get_edit_post_link( $post->ID ); ?>"><span class="pledit-pad">(<em>edit</em>)</span></a>
																		<?php }?>
                                                                        <div class="p tags">Теги: <?php echo get_the_term_list( $post->ID, 'tag', $before = '', ', ', $after = '' ); ?></div>
																	</div>
																</div>

															</div>
														</article>

														<div class="clear"></div>
													</div>
												</section>

												<?php get_template_part( 'comments' ); ?>

											<?php
											endwhile;
											wp_reset_postdata();
											?>

										</div>
									</div>

								</div>

								<div id="sidebar-wrap" class="">
									<div id="sidebar1" class="scolumn" >
										<div class="scolumn-pad"></div>
									</div>

									<section id="sb_primary" class="copy no_clone section-sb_primary">
										<div class="copy-pad">
											<ul id="list_sb_primary" class="sidebar_widgets fix"><?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?><?php endif; ?></ul>
											<div class="clear"></div>
										</div>
									</section>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

		<div id="morefoot_area" class="container-group">
			<section id="sb_footcols" class="container no_clone section-sb_footcols fix">
				<div class="texture">
					<div class="content">
						<div class="content-pad">
							<div class="fcolumns ppfull pprow">
								<div class="fcolumns-pad fix"><?php if ( ! dynamic_sidebar( 'footer' ) ) : ?><?php endif; ?></div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="clear"></div>

	</div>

	</div>
	</div>

<?php
get_filename();
get_footer();
?>