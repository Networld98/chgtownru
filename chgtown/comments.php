<?php get_filename(); ?>

	<section id="wp-comments" class="copy no_clone section-wp-comments">
		<div class="copy-pad">

			<?php
			global $post, $wp_query;
			$args               = array(
				'post_id' => $post->ID,
				'status'  => 'approve',
				'order'   => 'ASC'
			);
			$wp_query->comments = get_comments( $args );
			// if( !$wp_query->comments && !comments_open() ) return;
			?>
			<div id="comments" class="wp-comments">
				<div class="wp-comments-pad">
					<?php if ( $wp_query->comments ) : ?>

						<h3 id="comments-title">
							<?php printf( _n( 'Комментарии:', 'Комментарии:', get_comments_number(), 'pagelines' ), number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>' ); ?>
						</h3>

						<ol class="commentlist"><?php wp_list_comments( array( 'avatar_size' => 45 ) ); ?></ol>

						<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
							<div class="navigation fix">
								<div
									class="alignleft"><?php previous_comments_link( __( "<span class='meta-nav'>&larr;</span> Старые комментарии", 'pagelines' ) ); ?></div>
								<div
									class="alignright"><?php next_comments_link( __( "Новые комментарии <span class='meta-nav'>&rarr;</span>", 'pagelines' ) ); ?></div>
							</div>
						<?php endif; ?>

					<?php else :
						if ( ! comments_open() ) : ?>
							<p class="nocomments"><?php _e( 'Комментирование закрыто.', 'pagelines' ); ?></p>
						<?php endif; ?>

					<?php endif;
					comment_form( array(
						'must_log_in' => '<p class="must-log-in">Для отправки комментария вы должны <a href="#authform" class="open-auth-popup">авторизоваться</a>.</p>'
					) );
					?>
				</div>
			</div>

			<div class="clear"></div>

		</div>
	</section>

<?php get_filename(); ?>