<?php
get_header();
get_filename();
the_post();
?>
    <div id="page-main" class="container-group">
        <div id="dynamic-content" class="outline">
            <section id="content" class="container no_clone section-content-area fix">
                <div class="texture">
                    <div class="content">
                        <div class="content-pad">
                            <div id="pagelines_content" class="one-sidebar-right fix">
                                <div id="column-wrap" class="fix">
                                    <div id="column-main" class="mcolumn fix">
                                        <div class="mcolumn-pad">
                                            <section id="postloop" class="copy no_clone section-postloop">
                                                <div class="copy-pad">

                                                    <article <?php post_class('fpost'); ?>>
                                                        <div class="hentry-pad">
                                                            <div class="entry_wrap fix">
                                                                <div class="entry_content">
                                                                <?php
                                                                get_template_part('search', 'adverts');
                                                                get_template_part('breadcrumbs', 'adverts');
                                                                $user_id = (int)$post->post_author;
                                                                $user_first_name=get_user_meta($user_id,'first_name',true);
                                                                $userdata = get_userdata($user_id);
                                                                $user_mail=$userdata->data->user_email;
                                                                $user_phone=get_user_meta($user_id,'phone_number',true);
                                                                $fields = get_field_objects(get_the_ID());
//                                                                var_dump(get_field_objects(get_the_ID()));
                                                                ?>
                                                                    <div class="w-kartocka">
                                                                        <span class="w-zag"><?php  the_title()?></span>
                                                                        <div class="time-edit"><?php echo get_the_time(get_option('date_format')).' '.get_the_time('H:i') ?></div>
                                                                        <div class="container">
                                                                            <?php if ($images_group_advance = get_field('user_images_group_advance')) {?>
                                                                            <div id="slider" class="lof-slidecontent">
                                                                                <div class="preload">
                                                                                    <div></div>
                                                                                </div>
                                                                                <!-- MAIN CONTENT -->
                                                                                <div class="lof-main-outer">
                                                                                    <ul class="lof-main-wapper">
                                                                                        <?php
                                                                                        if (get_field('user_images_group_advance')) {
                                                                                            while (has_sub_field('user_images_group_advance')){
                                                                                                $image=wp_get_attachment_url(get_sub_field('user_image_advance'));
                                                                                                echo '<li ><div style="position:relative;display:table;height: 430px;width: 100%;"> <div class="otable">'. wp_get_attachment_image(get_sub_field('user_image_advance'), 'slider-main') .'</div></div></li>';//если что - вернуть <a class="lupa fancybox" href="'.wp_get_attachment_url(get_sub_field('user_image_advance')).'"></a>

                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </ul>

                                                                                    <?php if (count($images_group_advance) > 1) : ?>
                                                                                        <a onclick="return false" href="" class="lof-main-previous">Previous</a>
                                                                                        <a onclick="return false" href="" class="lof-main-next">Next</a>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                                <!-- END MAIN CONTENT -->

                                                                                <!-- NAVIGATOR -->
                                                                                <?php if (count($images_group_advance) > 1) : ?>
                                                                                <div class="lof-navigator-wapper">
                                                                                    <a onclick="return false" href="" class="lof-next">Next</a>
                                                                                    <div class="lof-navigator-outer">
                                                                                        <ul class="lof-navigator">
                                                                                            <?php
                                                                                            if (get_field('user_images_group_advance')) {
                                                                                                while (has_sub_field('user_images_group_advance')){
                                                                                                    echo '<li>'. wp_get_attachment_image(get_sub_field('user_image_advance'), 'slider-preview') . '</li>';

                                                                                                }
                                                                                            }
                                                                                            ?>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <a onclick="return false" href="" class="lof-previous">Previous</a>
                                                                                </div>
                                                                                <?php endif; ?>
                                                                                <!----------------- --------------------->
                                                                            </div>
                                                                            <?php }else{
                                                                                $image=get_template_directory_uri(). '/images/no_photo.png';
                                                                                //echo '<img src="' . THEME_URL . '/images/nophoto_big.png">';
                                                                            }?>
                                                                        </div>
                                                                        <div class="w-miniform w-info-read w-kartform">
                                                                            <ul>
                                                                                <?php
                                                                                    if ($price_advert = get_field('price_advert'))
                                                                                    //get_post_meta(get_the_ID(), 'price_advert', true))
                                                                                {?>
                                                                                    <li  class="w-b-bott">
                                                                                        <ul>
                                                                                            <li class="w-listname"><label>Цена</label></li>
                                                                                            <li class="w-listresult">
                                                                                                <?php
                                                                                                    echo '<label class="w-birk">'. $price_advert .'</label>';
                                                                                                ?>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </li>
                                                                                <?php }?>
                                                                                <li>
                                                                                    <ul>
                                                                                        <li class="w-listname"><label>Контактное лицо</label></li>
                                                                                        <li class="w-listresult"><b><?php echo $user_first_name?></b><label> <a href="#w_user_panel" id="setLetter" class="addNewImage">написать сообщение</a></label></li>
                                                                                    </ul>
                                                                                </li>

                                                                                <li>
                                                                                    <ul>
                                                                                        <li class="w-listname"><label>Телефон</label></li>
                                                                                        <li class="w-listresult"><span id="hidephone" data-phone="<?php echo !empty($user_phone)?$user_phone:'Неизвестно'?>">8 XXX XXX-XX-XX</span>
                                                                                            <label><a href="#" id="visiblephone" class="addNewImage">показать номер</a></label>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li class="w-b-bott"></li>
                                                                                <?php
                                                                                if ($fields){
                                                                                    $i = 0;
                                                                                    echo '<li>';
                                                                                    foreach ($fields as $field) {
                                                                                        //var_dump($field);
                                                                                        if ($field['type'] != 'repeater' && $field['name'] != 'price_advert' && !empty($field['value'])){
                                                                                            if (++$i > 1) echo ', ';
                                                                                            echo ($field['type'] == 'select' ? $field['label']. ': '. $field['choices'][$field['value']] :$field['label']. ': '.  $field['value']);
                                                                                            if ($field['name'] == 'year_vip')
                                                                                                echo 'г.';
                                                                                            elseif ($field['name'] == 'v_dvigatel')
                                                                                                echo 'л.';
                                                                                            elseif ($field['name'] == 'korobka')
                                                                                                echo ' коробка';
                                                                                        }
                                                                                    }
                                                                                    echo '<li>';
                                                                                }
                                                                                ?>

                                                                                <li>
                                                                                    <ul>
                                                                                        <li class="w-listresult w-alllistresult"> <?php the_content();  ?></li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>

                                                                    <div class="w_user_panel" id="w_user_panel">
                                                                        <div class="w-mail-soc">
                                                                            <a href="#" class="addNewImage" id="letter">
                                                                                <img src=" <?php echo get_template_directory_uri().'/images/konvert.png' ?>"/> Написать продавцу</a>
                                                                            <span class="share42init" data-title="<?php the_title_attribute() ?>" data-url="<?php the_permalink() ?>" data-image="<?php  echo $image?>" data-description="<?php echo get_the_content() ?>"> </span>

                                                                        </div>
                                                                        <div class="w_mess_form">
                                                                            <form name="w_mesage_form" class="w-form" id="w_mesage_form" method="post" action="">
                                                                            <ul class="w-mar_5">
                                                                                <li>
                                                                                    <ul>
                                                                                        <li class="w-sm"><label>Ваше имя</label></li>
                                                                                        <li class="w-lg"><input type="text" class="lg-input input_white_form" value="" name="name" id="firstname"></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li>
                                                                                    <ul>
                                                                                        <li class="w-sm"><label>Ваш e-mail</label></li>
                                                                                        <li class="w-lg">
                                                                                            <input type="text" class="lg-input input_white_form" name="email" id="email">
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li>
                                                                                    <ul>
                                                                                        <li class="w-sm"><label>Сообщение</label></li>
                                                                                        <li class="w-lg">
                                                                                            <textarea class="input_white_form" id="message" name="message"></textarea>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li>
                                                                                    <ul>
                                                                                        <li class="w-sm"></li>
                                                                                        <li class="w-lg">
                                                                                            <input type="submit" name="send_message" value="Отправить сообщение">
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                            </ul>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>

                                                    <div class="clear"></div>
                                                </div>
                                            </section>

                                            <?php
                                            wp_reset_postdata();
                                            ?>

                                        </div>
                                    </div>

                                </div>

                                <div id="sidebar-wrap" class="">                                   <div id="sidebar1" class="scolumn">
                                        <div class="scolumn-pad">    </div>
                                    </div>
                                    <section id="sb_primary">
                                        <div class="copy-pad">
                                            <ul id="list_sb_primary"
                                                class="sidebar_widgets fix"><?php if (!dynamic_sidebar('sidebar-1')) : ?><?php endif; ?></ul>
                                            <div class="clear"></div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div id="morefoot_area" class="container-group">
            <section id="sb_footcols" class="container no_clone section-sb_footcols fix">
                <div class="texture">
                    <div class="content">
                        <div class="content-pad">
                            <div class="fcolumns ppfull pprow">
                                <div
                                    class="fcolumns-pad fix"><?php if (!dynamic_sidebar('footer')) : ?><?php endif; ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="clear"></div>

    </div>

    </div>
    </div>
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.validate.min.js"></script>
    <script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.easing.js"></script>
    <script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/script.js"></script>
    <script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/share42/share42.js"></script>
    <script>
        var post_author =<?php echo $post->post_author ?>;
        jQuery().ready(function ($) {
            jQuery.validator.setDefaults({
                submitHandler: function (form) {
                    var name_user = $('#firstname').val();
                    var mail = $('#email').val();
                    var message = $('#message').val() || '';
                    // do other things for a valid form
                    $.post("/wp-admin/admin-ajax.php", {'action': 'send_question', 'name': name_user, 'email': mail, 'mess': message, 'post_author': post_author}, function (data) {
                        if(data=='00'){
                            $('.w_mess_form').slideUp(300);
                            alert('Сообщение отправлено!');
                            $('#firstname').val('');
                            $('#email').val('');
                            $('#message').val('');
                        }else if(data=='10'){
                            alert('Сообщение не отправлено!');
                        }else{
                            alert('Сообщение не отправлено: пользователь не указал свой e-mail!');
                        }
                    })
                    return false;
                }
            });
            $('#letter').click(function () {
                $(this).parent().next('div').slideToggle(300);
                return false;
            });
            $('#setLetter').click(function () {
                $('.w_mess_form').slideDown(300);
            })
            $('#visiblephone').click(function () {
                $(this).hide();
                var _phone=$('#hidephone').data('phone');
                $('#hidephone').html(_phone);
                return false;
            })
            // validate signup form on keyup and submit
            $("#w_mesage_form").validate({
                errorClass: "w-info-error",
                rules: {
                    'name': "required",
                    'message': {
                        required: true
                    },
                    'email': {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    'message': "Введите сообщение",
                    'name': "Введите свое имя",
                    'email': "Введите корректный e-mail"
                }
            });

            var buttons = {
                previous: $('#slider .lof-previous, .lof-main-previous'),
                next    : $('#slider .lof-next, .lof-main-next')
            };

            $obj = $('#slider').lofJSidernews({
                interval: 4000,
                direction: 'opacitys',
                easing: 'easeInOutExpo',
                duration: 1200,
                auto: false,
                maxItemDisplay: 4,
                navPosition : 'vertical',
                navigatorHeight: 108,
                navigatorWidth: 180,
                mainWidth: 720,
                buttons: buttons
            });

            $( '.fancybox' ).fancybox({
                'padding': 0,
                'modal': false
            });

        })
</script>
<?php
get_filename();
get_footer();
?>