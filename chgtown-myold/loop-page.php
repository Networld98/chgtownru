<?php get_filename(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div <?php post_class(); ?>>
		<h2 class="post-title"><?php the_title(); ?></h2>
		<div class="post-content"><?php the_content(); ?></div>
	</div>
<?php endwhile; ?>

<?php get_filename(); ?>