<div id="services-wrapper">
	<h2 class="post-title">Услуги</h2>
	<div id="next-slide"></div>
	<div id="prev-slide"></div>
	<div id="services">
		
		<ul>
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					'post_type' => 'services',
					'posts_per_page' => '20',
					'paged' => $paged
				);
				$services = new WP_Query( $args );
				while ( $services->have_posts() ) : $services->the_post();
			?>
				<li class="slider-item">
					<a rel="slider" class="fancybox" href="<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; ?>"><?php the_post_thumbnail( 'services' ); ?></a>
				</li>			
			<?php
				endwhile;
				wp_reset_postdata();
			?>
		</ul>
	</div>
</div>

jQuery( '#services' ).jCarouselLite({
	btnNext: '#next-slide',
	btnPrev: '#prev-slide',
	visible: 4
});