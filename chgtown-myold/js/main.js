(function ( $ ) {
    $('#allcatalog').click(function(){
        $('.w-catlog_all').toggleClass('visible');
//        $('.w-catlog_all').slideToggle();
        return false;
    });
    $( function () {
        $( '.fancybox' ).fancybox( {
            'padding':    0,
            // 'modal': true,
            'nextEffect': 'fade',
            'prevEffect': 'fade',
            // 'nextEffect': 'none',
            // 'prevEffect': 'none'
        } );

        $( '#cycle.fclone' ).cycle( {
            slideResize: 0,
            fit: 1,
            fx: 'fade',
            sync: 1,
            timeout: 10000,
            speed: 1000,
            cleartype: true,
            cleartypeNoBg: true,
            pager: '#featurenav.fclone'
        } );
        /*jQuery( "div#featurenav.fclone" ).children( "a" ).each( function () {
         if ( jQuery( this ).html() == "1" ) {
         jQuery( this ).attr( "title", "АБТ 2013" );
         jQuery( this ).html( "АБТ 2013" );
         }
         if ( jQuery( this ).html() == "2" ) {
         jQuery( this ).attr( "title", "+7 926 33 88 555" );
         jQuery( this ).html( "+7 926 33 88 555" );
         }
         } );*/

        var cSel = '#cycle.fclone';
        var ppSel = '.playpause.fclone';

        jQuery( ppSel ).click( function () {
            if ( jQuery( ppSel ).hasClass( 'pause' ) ) {
                jQuery( cSel ).cycle( 'pause' );
                jQuery( ppSel ).removeClass( 'pause' ).addClass( 'resume' );
            } else {
                jQuery( ppSel ).removeClass( 'resume' ).addClass( 'pause' );
                jQuery( cSel ).cycle( 'resume', true );
            }
        } );

    } );
    jQuery('input:checkbox[name^="tax_input"]').click(function () {
        console.log(jQuery(this).parents('ul').parent());
    })
})( jQuery );
