<?php

add_action( 'init', 'load_fancybox' );
function load_fancybox() {
	if ( !is_admin() ) {
		wp_register_script( 'fancybox', THEME_URL . '/js/fancybox/jquery.fancybox.pack.js', array( 'jquery' ), '1', true );
		wp_enqueue_script( 'fancybox' );
		wp_register_style( 'fancybox', THEME_URL . '/js/fancybox/jquery.fancybox.css', false, '1', 'all' );
		wp_enqueue_style( 'fancybox' );
	}
}

?>