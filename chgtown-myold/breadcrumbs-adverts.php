<div class="w-dir">
    <ul>
        <li><a href="/adverts/"> Все объявления </a><span class="w-caret">&#9654</span></li>
        <?php
        $child = get_term_by('slug', get_query_var('term'), 'adverts');
        $parent_term = get_term($child->parent, 'adverts');
        if (!is_single()) {
            if (!is_wp_error($parent_term))
                echo '<li><a href="/adverts/' . $parent_term->slug . '">' . $parent_term->name . '</a><span class="w-caret">&#9654</span></li>';
            if ($child !== false)
                echo '<li><a href="/adverts/' . $child->slug . '">' . $child->name . '</a><span class="w-caret">&#9654</span></li>';
        } else {
            $category = wp_get_post_terms($post->ID, 'adverts');
            foreach ($category as $key => $val) {
                if ($key != count($category) - 1)
                    echo '<li><a href="/adverts/' . $val->slug . '">' . $val->name . '</a><span class="w-caret">&#9654</span></li>';
                else
                    echo '<li><a href="/adverts/' . $val->slug . '">' . $val->name . '</a></li>';

            }
            echo '<span class="nav-bread">' . next_post_link('%link', $link = 'Следующее >', true) . '</span>';
        }
        echo '<span class="nav-bread dashed"><a href="#" id="allcatalog">Весь каталог</a></span>';
        ?>
    </ul>
</div>
    <div class="w-catlog_all">
        <?php
        $str = '';
        $terms_array = get_terms('adverts', array('hide_empty' => 0,'orderby' => 'term_order', 'order' => 'ASC', 'parent' => 0));
        foreach ($terms_array as $key => $value) {
                $str.='<ul><li class="w-catalog_activ"><a href="' . get_term_link((int)$value->term_id, 'adverts') . '">'.$value->name.'</a></li>';
                $terms_child_array =get_terms('adverts', array('hide_empty' => 0,'orderby' => 'term_order', 'order' => 'ASC', 'parent' => $value->term_id));

                foreach ($terms_child_array as $ch_key => $ch_value)
                    $str .= '<li ><a  href="' . get_term_link((int)$ch_value->term_id, 'adverts') . '">' . $ch_value->name . '</a></li>';
                $str.='</ul>';

        }
        echo $str;

        ?>
    </div>

