<?php
	get_header();
	get_filename();
?>

<div id="main">

	<div id="content">
		<h1 class="page-title"><?php printf( 'Архив категории: %s', '' . single_cat_title( '', false ) . '' ); ?></h1>
		<?php
			$category_description = category_description();
			if ( ! empty( $category_description ) ) echo '<div class="archive-meta">' . $category_description . '</div>';
			get_template_part( 'loop', 'archive' );
		?>
	</div>
	<div id="sidebar"><?php get_sidebar(); ?></div>

</div>

<?php
	get_header();
	get_filename();
?>