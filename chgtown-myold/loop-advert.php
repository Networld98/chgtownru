<?php

function search_by_title_and_content($where)
{
    if ($_POST['title'] != '') {
        $where_one = 'wp_posts.post_title LIKE "%' . $_POST['title'] . '%"';

        if (!isset($_POST['in_title'])or $_POST['in_title'] != 1)
            $where_one .= ' OR wp_posts.post_content LIKE "%' . $_POST['title'] . '%"';

        $where .= " AND (" . $where_one . ")";
    }
    return $where;
}


$args = array(
    'post_type' => 'advert',
    'post_status' => 'publish',
    'paged' => get_query_var('paged')
);
// Filter start
if (get_query_var('term')) {
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'adverts',
            'field' => 'slug',
            'terms' => get_query_var('term')
        )
    );
}

if (isset($_POST['has_foto'])and $_POST['has_foto'] == 1) {
    $args['meta_query'][] = array(
        'key' => 'user_images_group_advance',
        'value' => 0,
        'type' => 'NUMERIC',
        'compare' => '>'
    );
}

if ($_POST['fields'] !== NULL) {
    $args['meta_query']['relation'] = 'AND';
    foreach ($_POST['fields'] as $key => $val) {
        if (!empty($val))
            if ($key == 'price_advert') {
                $compare = 'BETWEEN';
                $type = 'NUMERIC';
            } else {
                $compare = '=';
                $type = 'CHAR';
            }


        if (!empty($val) && $val != 'null')
            $args['meta_query'][] = array(
                'key' => $key,
                'value' => $val,
                'compare' => $compare,
                'type' => $type
            );
    }
}

add_filter('posts_where_request', 'search_by_title_and_content');
//------------------------------------------

$query = new WP_Query($args);
if ($query->have_posts()) {
    echo '<div class="w-lenta"><ul>';
    while ($query->have_posts()) : $query->the_post(); ?>
        <li class="w-even" <?php post_class('advert'); ?>>
            <div class="w-even-wrap">
                <a class="permalink" href="<?php the_permalink(); ?>"></a>

                <?php
                $post_id = get_the_ID();
                $category = wp_get_post_terms($post_id, 'adverts');
                $category = printCategory($category);
                $fields = get_field_objects();
                ?>
                <ul>
                    <li class="w-day">
                        <span><?php echo date('d.m.Y') == get_the_time(get_option('date_format')) ? 'Сегодня' : get_the_time(get_option('date_format')); ?></span>
                        <p><?php echo get_the_time('H:i'); ?></p></li>
                    <li class="w-img">
                        <a href="<?php the_permalink(); ?>">
                            <?php
                            if ($images = get_field('user_images_group_advance'))
                                echo wp_get_attachment_image($images[0]['user_image_advance'],'slider-preview');
                            else
                                echo '<img src="' . THEME_URL . '/images/no_photo.png">';
                            ?>
                        </a>
                    </li>
                    <li class="w-manual">
                        <a class="title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                        <p>
                            <?php
                            if ($price_advert = get_post_meta($post_id, 'price_advert', true))
                                echo 'Цена: '. $price_advert." | ";
                            ?>
                            <span class="category"> <?php echo $category ?></span>
                        </p>

                        <div class="desc"><?php the_excerpt(); ?></div>
    <!--                    <p><a class="more" href="--><?php //the_permalink(); ?><!--">Подробнее</a></p>-->
                    </li>
                </ul>
            </div>
        </li>
    <?php endwhile;
    echo '</ul></div>';

    wp_reset_query();

    if (function_exists('wp_pagenavi'))
        wp_pagenavi(array('query' => $query));
}
else {
    if (isset($_POST['title']) && trim($_POST['title']) != "") {
        echo '<div class="w-lenta" style="text-align:center">По Вашему запросу "' . trim($_POST['title']) . '" ничего не найдено.</div>';
    } else {
        echo '<div class="w-lenta" style="text-align:center">Нет объявлений!</div>';
    }
}
