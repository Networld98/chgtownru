<?php

add_filter( 'mce_buttons_3', 'add_more_buttons' );
function add_more_buttons( $buttons ) {
	$buttons[] = 'hr,del,sub,sup,fontselect,fontsizeselect,cleanup,styleselect,copy,cut,paste';
	return $buttons;
}

add_filter( 'tiny_mce_before_init', 'my_mce_before_init' );
function my_mce_before_init( $init_array ) {
	$init_array['theme_advanced_styles'] = "First Class=first-class;Other class=other-class";
	return $init_array;
}


// add the shortcode handler for YouTube videos
function addYouTube($atts, $content = null) {
        extract(shortcode_atts(array( "id" => '' ), $atts));
        return '<span class="simple_tooltip" title="'.$id.'">'.$content.'</span>';
}
add_shortcode('youtube', 'addYouTube');

function add_youtube_button() {
     add_filter('mce_external_plugins', 'add_youtube_tinymce_plugin');
     add_filter('mce_buttons', 'register_youtube_button');
}
add_action('init', 'add_youtube_button');

function register_youtube_button($buttons) {
   array_push($buttons, "|", "brettsyoutube");
   return $buttons;
}

function add_youtube_tinymce_plugin($plugin_array) {
   $plugin_array['brettsyoutube'] = get_bloginfo('template_url').'/inc/js/admin.js';
   return $plugin_array;
}

//add_action('admin_head', 'button_js2');
function button_js2() {
        echo '<script type="text/javascript">
        (function() {
    tinymce.create(\'tinymce.plugins.BrettsYouTube\', {
        init : function(ed, url) {
            ed.addButton(\'brettsyoutube\', {
                title : \'brettsyoutube.youtube\',
                image : \'http://s.ytimg.com/yt/favicon-refresh-vfldLzJxy.ico\',
                onclick : function() {
                    var vidId = prompt("YouTube Video", "Enter the id or url for your video");
                    ed.selection.setContent(\'[span id=\'+vidId+\']\' + ed.selection.getContent() + \'[/span]\');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
        getInfo : function() {
            return {
                longname : "Brett\'s YouTube Shortcode",
                author : \'Brett Terpstra\',
                authorurl : \'http://brettterpstra.com/\',
                infourl : \'http://brettterpstra.com/\',
                version : "1.0"
            };
        }
    });
    tinymce.PluginManager.add(\'brettsyoutube\', tinymce.plugins.BrettsYouTube);
})();
        </script>';
}


add_action('media_buttons','add_sc_select');
function add_sc_select(){
    global $shortcode_tags;
     /* ------------------------------------- */
     /* enter names of shortcode to exclude bellow */
     /* ------------------------------------- */
    $exclude = array();
    echo '&nbsp;<select id="sc_select"><option>Shortcode</option>';
    foreach ($shortcode_tags as $key => $val){
            if(!in_array($key,$exclude)){
            $shortcodes_list .= '<option value="['.$key.'][/'.$key.']">'.$key.'</option>';
            }
        }
     echo $shortcodes_list;
     echo '</select>';
}
add_action('admin_head', 'button_js');
function button_js() {
        echo '<script type="text/javascript">
        jQuery(document).ready(function(){
           jQuery("#sc_select").change(function() {
                          send_to_editor(jQuery("#sc_select :selected").val());
                          return false;
                });
        });
        </script>';
}
?>