<?php

class TermFields extends Fields
{
	public function __construct()
	{
	}

	public function init()
	{
		add_action( 'created_' . $this->object_type, array( $this, 'updateFields' ), 10, 1 );
		add_action( 'edited_' . $this->object_type, array( $this, 'updateFields' ), 10, 1 );
		add_action( $this->object_type . '_add_form_fields', array( $this, 'addTermFields' ) );
		add_action( $this->object_type . '_edit_form_fields', array( $this, 'editTermFields' ) );
		add_action( 'init', array( $this, 'setTermMetaTable' ) );

		parent::init();
	}

	public function setTaxonomy( $taxonomy )
	{
		$this->setObjectType( $taxonomy );
		return $this;
	}

	public function render( $type = null )
	{
		foreach ( $this->sections as $section_key => $section_value ) {
			$this->do_settings_fields( $this->slug, $section_key, $type );
		}
	}

	private function do_settings_fields( $page, $section, $type )
	{
		global $wp_settings_fields;
		if ( !isset( $wp_settings_fields ) || !isset( $wp_settings_fields[$page] ) || !isset( $wp_settings_fields[$page][$section] ) ) return;

		foreach ( (array)$wp_settings_fields[$page][$section] as $field ) {

			if ( $type == 'edit_term_fields' ) {

				echo '<tr class="form-field">';

				if ( !empty( $field['args']['label_for'] ) ) {
					echo '<th scope="row" valign="top"><label for="' . $field['args']['label_for'] . '">' . $field['title'] . '</label></th>';
				} else {
					echo '<th scope="row" valign="top">' . $field['title'] . '</th>';
				}

				echo '<td>';
				call_user_func( $field['callback'], $field['args'] );
				echo '</td>';

				echo '</tr>';
			}

			if ( $type == 'add_term_fields' ) {

				echo '<div class="form-field">';

				if ( !empty( $field['args']['label_for'] ) ) {
					echo '<label for="' . $field['args']['label_for'] . '">' . $field['title'] . '</label>';
				} else {
					echo $field['title'];
				}

				call_user_func( $field['callback'], $field['args'] );

				echo '</div>';
			}
		}
	}

	public function renderField( $args )
	{
		$field = $args['setting'];
		$object_id = isset( $_REQUEST['tag_ID'] ) ? (int)$_REQUEST['tag_ID'] : 0;
		$value = get_term_meta( $object_id, $field['id'], true );

		parent::renderField( $field, $value );
	}
}
