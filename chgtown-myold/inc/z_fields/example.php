<?php

require_once 'z_fields.php';

$settings_page = new z_fields();
$settings_page
	->setType( 'settings-page' )
	->setId( 'slug1' )
	->setName( 'Name' )
	->setPrefix( 'to_' )
	->setCapability( 'edit_themes' )
	->setPosition( 1 )
	->setSidebar( 'Title', 'Content' )
	->setDeveloperMode();
$settings_page
	->addSection( array( 'id' => 'tab1', 'label' => 'Tab 1'	) )
	->addSection( array( 'id' => 'tab2', 'label' => 'Tab 2' ) )
	->addSection( array( 'id' => 'tab3', 'label' => 'Tab 3'	) );

$settings_page->addText( array(
	'section' => 'tab1',
	'id' => 'text',
	'label' => 'Text',
	'desc' => 'Description'
) );
$settings_page->addPassword( array(
	'section' => 'tab1',
	'id' => 'password',
	'label' => 'Password',
	'desc' => 'Description'
) );
$settings_page->addWpeditor( array(
	'section' => 'tab1',
	'id' => 'wpeditor',
	'label' => 'WPEditor',
	'desc' => 'Description',
	'settings' => array(
		'wpautop' => 'false',
		'media_buttons' => 'true',
		//'textarea_name' => $editor_id,
		'textarea_rows' => get_option( 'default_post_edit_rows', 10 ),
		'tabindex' => '',
		'editor_css' => '',
		'editor_class' => '',
		'teeny' => 'true',
		'dfw' => 'false',
		'tinymce' => true,//array(
		// 'theme_advanced_buttons1' => 'bold,italic,strikethrough,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyright,|,link,unlink,wp_more,|,spellchecker,fullscreen,wp_adv',
		// 'theme_advanced_buttons2' => 'formatselect,underline,justifyfull,forecolor,|,pastetext,pasteword,removeformat,|,media,charmap,|,outdent,indent,|,undo,redo,wp_help',
		// 'theme_advanced_buttons3' => '',
		// 'theme_advanced_buttons4' => '',
		//),
		'quicktags' => 'true'
	)
) );
$settings_page->addTextarea( array(
	'section' => 'tab1',
	'id' => 'textarea',
	'label' => 'Textarea',
	'desc' => 'Description'
) );
$settings_page->addCheckbox( array(
	'section' => 'tab1',
	'id' => 'checkbox',
	'label' => 'Checkbox',
	'desc' => 'Description'
) );
$settings_page->addSelect( array(
	'section' => 'tab1',
	'id' => 'select',
	'label' => 'Select',
	'desc' => 'Description',
	'options' => array (
		'one' => 'Option 1',
		'two' => 'Option 2',
		'three' => 'Option 3'
	)
) );
$settings_page->addRadio( array(
	'section' => 'tab1',
	'id' => 'radio',
	'label' => 'Radio',
	'desc' => 'Description',
	'options' => array (
		'one' => 'Option 1',
		'two' => 'Option 2',
		'three' => 'Option 3'
	)
) );
$settings_page->addCheckboxGroup( array(
	'section' => 'tab1',
	'id' => 'checkbox_group',
	'label' => 'Checkbox group',
	'desc' => 'Description',
	'options' => array (
		'one' => 'Option 1',
		'two' => 'Option 2',
		'three' => 'Option 3'
	)
) );
$settings_page->addUsersCheckboxGroup( array(
	'section' => 'tab1',
	'id' => 'users_checkbox_group',
	'label' => 'User checkbox group',
	'desc' => 'Description'
) );
$settings_page->addTermSelect( array(
	'section' => 'tab1',
	'id' => 'term_select',
	'label' => 'Term select',
	'desc' => 'Description',
	'taxonomy' => 'category'
) );
$settings_page->addTermRadio( array(
	'section' => 'tab1',
	'id' => 'term_radio',
	'label' => 'Term radio',
	'desc' => 'Description',
	'taxonomy' => 'category'
) );
$settings_page->addRepeatableText( array(
	'section' => 'tab1',
	'id' => 'repeatable_text',
	'label' => 'Repeatable text',
	'desc' => 'Description'
) );
$settings_page->addDate( array(
	'section' => 'tab1',
	'id' => 'date',
	'label' => 'Date',
	'desc' => 'Description',
	'locale' => 'ru'
) );
$settings_page->addPostSelect( array(
	'section' => 'tab1',
	'id' => 'post_select',
	'label' => 'Post select',
	'desc' => 'Description',
	'post_type' => 'staff'
) );
$settings_page->addImage( array(
	'section' => 'tab1',
	'id' => 'image',
	'label' => 'Image',
	'desc' => 'Description'
) );

$settings_page->init();

$post_mb = new z_fields();
$post_mb
	->setType( 'meta_box' )
	->setId( 'slug2' )
	->setPrefix( 'to_' )
	->setName( 'Name' )
	->setPostType( 'post' )
	->setContext( 'normal' )
	->setPriority( 'high' )
	->setDeveloperMode();
$post_mb
	->addSection( array( 'id' => 'tab1', 'label' => 'Tab 1'	) )
	->addSection( array( 'id' => 'tab2', 'label' => 'Tab 2' ) )
	->addSection( array( 'id' => 'tab3', 'label' => 'Tab 3'	) );

$post_mb->addText( array(
	'section' => 'tab1',
	'id' => 'text-input',
	'label' => 'Text input 2',
	'desc' => 'Description',
	'default' => 'Default'
) );

$post_mb->init();

$category = new z_fields();
$category
	->setType( 'term_meta' )
	->setId( 'slug3' )
	->setPrefix( 'to_' )
	->setName( 'Name' )
	->setTaxonomy( 'category' )
	->setDeveloperMode();
$category
	->addSection( array( 'id' => 'tab1', 'label' => 'Tab 1'	) )
	->addSection( array( 'id' => 'tab2', 'label' => 'Tab 2' ) )
	->addSection( array( 'id' => 'tab3', 'label' => 'Tab 3'	) );

$category->addText( array(
	'section' => 'tab1',
	'id' => 'text-input',
	'label' => 'Text input 3',
	'desc' => 'Description',
	'default' => 'Default'
) );
$category->addTextarea( array(
	'section' => 'tab1',
	'id' => 'textarea',
	'label' => 'Textarea',
	'desc' => 'Description',
	'default' => 'Default'
) );
$category->addWpeditor( array(
	'section' => 'tab1',
	'id' => 'wpeditor',
	'label' => 'WPEditor',
	'desc' => 'Description',
	'default' => 'Default'
) );

$category->init();