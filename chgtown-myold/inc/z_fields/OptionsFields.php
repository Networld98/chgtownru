<?php

class OptionsFields extends Fields
{
	private $position = 999.9;
	private $capability = 'edit_themes';
	private $sidebar_label;
	private $sidebar_content;

	public function __construct()
	{
	}

	public function init()
	{
		add_action( 'admin_menu', array( $this, 'initAdminMenu' ) );

		parent::init();
	}

	public function initAdminMenu()
	{
		add_menu_page( $this->name, $this->name, $this->capability, $this->slug, array(
			$this,
			'render'
		), admin_url( 'images/generic.png' ), $this->position );
	}

	public function setPosition( $position )
	{
		$this->position = $position;
		return $this;
	}

	public function setCapability( $capability )
	{
		$this->capability = $capability;
		return $this;
	}

	public function setSidebar( $label, $content )
	{
		$this->sidebar_label = $label;
		$this->sidebar_content = $content;
		return $this;
	}

	private function checkSidebar()
	{
		if ( $this->sidebar_label || $this->sidebar_content ) return true;
		return false;
	}

	public function render( $type = null )
	{
		if ( !current_user_can( $this->capability ) ) wp_die( __( 'You do not have sufficient permissions to manage options for this site.' ) );
		add_settings_error( 'updated', 'settings_updated', __( 'Settings saved.' ), 'updated' );
		if ( isset( $_POST['action'] ) ) $this->updateFields();
		$check_sidebar = $this->checkSidebar(); ?>

		<div class="wrap">
			<?php screen_icon( 'options-general' ); ?>
			<h2><?php echo $this->name; ?></h2>

			<div class="metabox-holder<?php if ( $check_sidebar ) { ?> has-right-sidebar<?php } ?>">
				<div id="post-body">
					<div id="post-body-content">
						<?php if ( isset( $_POST['submit'] ) ) settings_errors( 'updated', true, false ); ?>
						<div class="postbox">
							<h3><span>Main Content Header</span></h3>

							<div class="inside">
								<form method="post" action="">
									<?php
									settings_fields( $this->slug );
									submit_button( 'Save changes', 'primary', 'submit', 'true' );
									?>
									<div id="settings-tabs">
										<?php if ( count( $this->sections ) > 1 ) { ?>
											<h2 class="nav-tab-wrapper">
												<?php foreach ( $this->sections as $section_key => $section_value ) { ?>
													<a class="nav-tab" href="#<?php echo $section_key; ?>"><?php echo $section_value; ?></a>
												<?php } ?>
											</h2>
											<script>
												(function ( $ ) {
													$( function () {
														var tab = $( '#settings-tabs .nav-tab' );
														var tab_content = $( '.tab_content' );
														tab_content.hide();
														tab.first().addClass( 'nav-tab-active' ).show();
														tab_content.first().show();
														tab.on( 'click', function ( event ) {
															event.preventDefault();
															tab.removeClass( 'nav-tab-active' );
															$( this ).addClass( 'nav-tab-active' );
															tab_content.hide();
															var activeTab = $( this ).attr( 'href' );
															$( activeTab ).fadeIn();
														} );
													} );
												})( jQuery );
											</script>
											<style>
												.form-table {
													border-radius: 2px;
													border: 1px solid #d3d3d3;
												}

												#settings-tabs .nav-tab-wrapper {
													padding-bottom: 0px;
												}

												#settings-tabs .nav-tab:hover, #settings-tabs .nav-tab-active {
													border-color: #CCCCCC #CCCCCC #f9f9f9;
												}
											</style>
										<?php } ?>
										<?php foreach ( $this->sections as $section_key => $section_value ) { ?>
											<div id="<?php echo $section_key; ?>" class="tab_content">
												<table class="form-table"> <?php do_settings_fields( $this->slug, $section_key ); ?></table>
											</div>
										<?php } ?>
									</div>
									<?php submit_button( 'Save changes', 'primary', 'submit', 'true' ); ?>
								</form>
							</div>
						</div>
					</div>
				</div>
				<?php if ( $check_sidebar ) { ?>
					<div class="inner-sidebar">
						<div class="postbox">
							<h3><span><?php echo $this->sidebar_label; ?></span></h3>

							<div class="inside"><?php echo $this->sidebar_content; ?></div>
						</div>
					</div>
				<?php } ?>
				<br class="clear">
			</div>
		</div>

	<?php
	}

	public function renderField( $args )
	{
		$field = $args['setting'];
		$value = get_option( $field['id'] );

		parent::renderField( $field, $value );
	}
}