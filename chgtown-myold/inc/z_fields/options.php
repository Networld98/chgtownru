<?php

require_once 'Fields.php';
require_once 'PostFields.php';
require_once 'TermFields.php';
require_once 'OptionsFields.php';

$slider_mb = new PostFields;
$slider_mb
	->setType( 'meta_box' )
	->setId( 'slider' )
	->setPrefix( 'to_' )
	->setName( 'Slider settings' )
	->setPostType( 'slider' )
	->setContext( 'normal' )
	->setPriority( 'high' );
$slider_mb
	->addSection( array( 'id' => 'tab1', 'label' => 'Tab 1'	) );

$slider_mb->addImage( array(
	'section' => 'tab1',
	'id' => 'picture',
	'label' => 'Большая картинка',
	'desc' => ''
) );
$slider_mb->addText( array(
	'section' => 'tab1',
	'id' => 'url',
	'label' => 'Ссылка',
	'desc' => ''
) );

$slider_mb->init();

$box_mb = new PostFields;
$box_mb
	->setType( 'meta_box' )
	->setId( 'boxes' )
	->setPrefix( '' )
	->setName( 'Boxes' )
	->setPostType( 'boxes' )
	->setContext( 'normal' )
	->setPriority( 'high' )
	->setDeveloperMode();
$box_mb
	->addSection( array( 'id' => 'tab1', 'label' => 'Tab 1'	) );

$box_mb->addText( array(
	'section' => 'tab1',
	'id' => 'the_box_icon',
	'label' => 'Картинка',
	'desc' => ''
) );
$box_mb->addText( array(
	'section' => 'tab1',
	'id' => 'the_box_icon_link',
	'label' => 'Ссылка',
	'desc' => ''
) );

$box_mb->init();

$testimonial_mb = new PostFields;
$testimonial_mb
	->setType( 'meta_box' )
	->setId( 'test1' )
	->setPrefix( 'to_' )
	->setName( 'Отзывы' )
	->setPostType( 'testimonial' )
	->setContext( 'normal' )
	->setPriority( 'high' )
	->setDeveloperMode();
$testimonial_mb
	->addSection( array( 'id' => 'tab1', 'label' => 'Tab 1'	) );

$testimonial_mb->addPostSelect( array(
	'section' => 'tab1',
	'id' => 'client',
	'label' => 'Клиент',
	'desc' => '',
	'post_type' => 'portfolio'
) );

$testimonial_mb->addText( array(
	'section' => 'tab1',
	'id' => 'name',
	'label' => 'Имя',
	'desc' => ''
) );
$testimonial_mb->addDate( array(
	'section' => 'tab1',
	'id' => 'date',
	'label' => 'Дата',
	'desc' => ''
) );
$testimonial_mb->addTextarea( array(
	'section' => 'tab1',
	'id' => 'text',
	'label' => 'Текст',
	'desc' => ''
) );
$testimonial_mb->addImage( array(
	'section' => 'tab1',
	'id' => 'photo',
	'label' => 'Фотография',
	'desc' => ''
) );

$testimonial_mb->init();

$category = new TermFields;
$category
	->setType( 'term_meta' )
	->setId( 'slug3' )
	->setPrefix( 'to_' )
	->setName( 'Name' )
	->setTaxonomy( 'category' )
	->setDeveloperMode();
$category
	->addSection( array( 'id' => 'tab1', 'label' => 'Tab 1'	) )
	->addSection( array( 'id' => 'tab2', 'label' => 'Tab 2' ) )
	->addSection( array( 'id' => 'tab3', 'label' => 'Tab 3'	) );

$category->addText( array(
	'section' => 'tab1',
	'id' => 'text-input',
	'label' => 'Text input 3',
	'desc' => 'Description',
	'default' => 'Default'
) );
$category->addTextarea( array(
	'section' => 'tab1',
	'id' => 'textarea',
	'label' => 'Textarea',
	'desc' => 'Description',
	'default' => 'Default'
) );
$category->addWpeditor( array(
	'section' => 'tab1',
	'id' => 'wpeditor',
	'label' => 'WPEditor',
	'desc' => 'Description',
	'default' => 'Default'
) );

$category->init();