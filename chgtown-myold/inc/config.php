<?php

//get_template_part( 'inc/fields2/example-functions' );

$custom_post_types = 1;
$fields = 1;
$shortcodes = 0;
$fancybox = 1; // on/off fancybox
$slider = 0; // on/off slider
$carouselle = 0; // on/off carouselle
$usermeta = 1; // on/off usermeta
$webfonts = 0; // on/off webfonts

if ( $custom_post_types ) get_template_part( 'inc/cpt' );
if ( $fields ) get_template_part( 'inc/z_fields/options' );
if ( $shortcodes ) get_template_part( 'inc/shortcodes' );
if ( $fancybox ) get_template_part( 'js/fancybox/fancybox' );
if ( $slider ) get_template_part( 'js/slider/slider' );
if ( $carouselle ) get_template_part( 'js/carouselle/carouselle' );
if ( $usermeta ) get_template_part( 'inc/usermeta' );
if ( $webfonts ) get_template_part( 'inc/webfonts' );
get_template_part( 'inc/class.shortcodes' );

get_template_part( 'inc/c2l' );
get_template_part( 'inc/post-type-archive-links' );
get_template_part( 'inc/widgets/widgets' );

add_action( 'admin_enqueue_scripts', 'zelenin_admin_enqueue_scripts' );
add_action( 'wp_enqueue_scripts', 'zelenin_wp_enqueue_scripts' );
add_filter( 'admin_footer_text', 'zelenin_footer_admin' );
add_action( 'wp_dashboard_setup', 'zelenin_dashboard_widget' );
// add_filter( 'wp_default_editor', create_function( '', 'return "html";' ) );
add_action( 'template_redirect', 'zelenin_admin_tools', 5 );

function zelenin_wp_enqueue_scripts() {
	wp_register_script( 'main', THEME_URL . '/js/main.js', array( 'jquery' ), '1', true );
	wp_register_style( 'reset', THEME_URL . '/css/reset.css', false, '1', 'all' );
	wp_register_style( 'site', THEME_URL . '/css/site.css', false, '1', 'all' );
	wp_register_style( 'mobile', THEME_URL . '/css/mobile.css', false, '1', 'all' );
	wp_register_style( 'custom', THEME_URL . '/css/custom.css', false, '1', 'all' );
	wp_register_style( 'compiled', THEME_URL . '/css/compiled.css', false, '1', 'all' );

    if (is_single()) {
        wp_register_script( 'carouselle', THEME_URL . '/js/carouselle/jcarousellite_1.0.1.pack.js', array( 'jquery' ), '1', true );
        wp_enqueue_script( 'carouselle' );
    }

    if(is_page('add-advert')){
        wp_enqueue_script('nicefileinput', THEME_URL .'/js/jquery.nicefileinput.min.js');
    }

	wp_enqueue_script( 'main' );
	// wp_enqueue_style( 'reset' );
	wp_enqueue_style( 'site' );
	// wp_enqueue_style( 'mobile' );
	wp_enqueue_style( 'compiled' );
	wp_enqueue_style( 'custom' );
}

function zelenin_admin_enqueue_scripts() {
	wp_register_style( 'admin', THEME_URL . '/css/admin.css', array( 'colors' ), '1', 'all' );
	// wp_enqueue_style( 'admin' );
}

function zelenin_footer_admin () {
	echo 'Developed by Aleksandr Zelenin, <a href="mailto:aleksandr@zelenin.me">aleksandr@zelenin.me</a>, <a target="_blank" href="http://zelenin.me">zelenin.me</a>';
}

function zelenin_dashboard_widget() {
	wp_add_dashboard_widget( 'dashboard_widget', 'Wordpress programmer', 'dashboard_widget' );
	function dashboard_widget() {
		echo 'Developed by Aleksandr Zelenin, <a href="mailto:aleksandr@zelenin.me">aleksandr@zelenin.me</a>, <a target="_blank" href="http://zelenin.me">zelenin.me</a>';
	}
}

function zelenin_admin_tools() {
	if ( isset( $_GET['reset'] ) ) {
		get_template_part( 'inc/reset' );
		die();
	}
	if ( isset( $_GET['info'] ) ) {
		phpinfo();
		die();
	}
}