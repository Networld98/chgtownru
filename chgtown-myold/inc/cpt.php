<?php

add_action( 'init', 'cpt_register' );
function cpt_register() {

	register_post_type( 'news' , array(
		'label' => 'Новости',
		'singular_label' => 'Новость',
		'public' => true,
		'menu_position' => 20,
		'rewrite' => true,
		'has_archive' => true,
		'capability_type' => 'post',
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions' ),
	) );

	register_post_type( 'events' , array(
		'label' => 'Анонсы',
		'singular_label' => 'Анонс',
		'public' => true,
		'menu_position' => 20,
		'rewrite' => true,
		'has_archive' => true,
		'capability_type' => 'post',
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions' ),
	) );

	register_post_type( 'video' , array(
		'label' => 'Видео',
		'singular_label' => 'Видео',
		'public' => true,
		'menu_position' => 20,
		'rewrite' => true,
		'has_archive' => true,
		'capability_type' => 'post',
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions' ),
	) );

	register_post_type( 'comp' , array(
		'label' => 'Конкурсы',
		'singular_label' => 'Конкурс',
		'public' => true,
		'menu_position' => 20,
		'rewrite' => true,
		'has_archive' => true,
		'capability_type' => 'post',
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions' ),
	) );

	register_post_type( 'articles' , array(
		'label' => 'Статьи',
		'singular_label' => 'Статья',
		'public' => true,
		'menu_position' => 20,
		'rewrite' => true,
		'has_archive' => true,
		'capability_type' => 'post',
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions' ),
	) );

	register_post_type( 'slider' , array(
		'label' => 'Слайдер',
		'singular_label' => 'Слайд',
		'public' => true,
		'menu_position' => 19,
		'rewrite' => true,
		'has_archive' => true,
		'capability_type' => 'post',
		'supports' => array( 'title', 'editor' ),
	) );

	register_post_type( 'boxes' , array(
		'label' => 'Boxes',
		'singular_label' => 'Box',
		'public' => true,
		'menu_position' => 30,
		'rewrite' => true,
		'has_archive' => true,
		'capability_type' => 'post',
		'supports' => array( 'title', 'editor' ),
	) );

	register_taxonomy( 'tag', array( 'news', 'video', 'events', 'comp', 'articles' ), array(
		'hierarchical' => false,
		'label' => 'Тэги',
		'singular_label' => 'Тэг',
		'rewrite' => true
	) );


    /* новый код для Объявлений*/
    register_taxonomy('adverts', 'advert',
        array(
            'hierarchical' => true,
            'label' => __('Категории'),
            'capabilities' => array(
                'assign_terms' => 'edit_adverts'
            ),
            'rewrite' => array(
                'slug' => 'adverts'
            )
        )
    );

    register_post_type('advert',
        array(
            'label' =>'Объявления',
            'public' => true,
            'rewrite' => true,
            'map_meta_cap'=>true,
            'capability_type' => 'advert',
            'capabilities' => array(
                'edit_post' => "edit_advert",
                'read_post' => "read_advert",
                'delete_post' => "delete_advert",
                'edit_posts' => "edit_adverts",
                'edit_others_post' => "edit_others_adverts",
                'publish_posts' => "publish_adverts",
                'read_private_posts' => "read_private_adverts",
                'delete_posts' => "delete_adverts",
                'delete_private_posts' => "delete_private_adverts",
                'delete_published_posts' => "delete_published_adverts",
                'delete_others_posts' => "delete_others_adverts",
                'edit_private_posts' => "edit_private_adverts",
                'edit_published_posts' => "edit_published_adverts",
            ),
            'show_ui'              => true,
            'show_in_menu'         => true,
            'show_in_nav_menus'    => true,
            'show_in_admin_bar'    => true,
            'publicly_queryable' => true,
            'taxonomies' => array('adverts'),
            'supports' => array(
                'title',
                'editor',
                'excerpt',
                'post-thumbnails',
                'author'
               /* 'excerpts',
                'custom-fields',
                'revisions'*/
            )
        )
    );

    //remove_role('advert');
    add_role(
        'advert',
        'Объявления'
    );
    $caps = array(
        'read',
        'read_advert',
        'read_private_adverts',
        'edit_adverts',
        'edit_private_adverts',
        'edit_published_adverts',
        'edit_others_adverts',
        'publish_adverts',
        'delete_adverts',
        'delete_private_adverts',
        'delete_published_adverts',
        'delete_others_adverts',
    );
    foreach (array('advert', 'author', 'editor', 'administrator') as $role_name){
        $role = get_role($role_name);

        foreach ($caps as $cap)
            $role->add_cap($cap);
    }
}

add_filter('post_type_link', 'advert_post_type_link', 1, 3);
function advert_post_type_link($link, $post = 0){
    if ($post->post_type == 'advert')
        return site_url('advert/' . $post->ID);
    else
        return $link;
}

add_filter('rewrite_rules_array', 'advert_rewrite_rules');
function advert_rewrite_rules($rules){
    $new_rules = array('(advert)/(\d*)$' => 'index.php?post_type=$matches[1]&p=$matches[2]');
    return $new_rules + $rules;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);
function custom_excerpt_length($length){
    return 20;
}

add_filter('excerpt_more', 'custom_excerpt_more');
function custom_excerpt_more($more){
    return '...';
}
