<?php
session_start();

header('Cache-control: no-cache');

$str = substr(str_shuffle('ABCDEFGHIKLMNOPQRSTVXYZ'), 0, 4);

$_SESSION['captcha'] = $str;

// Create an image from button.png
$image = imagecreatefrompng(dirname(__FILE__).'/images/button.png');

// Set the font color
$color = imagecolorallocate($image, 80, 130, 80);

// Set the font
$font = dirname(__FILE__).'/fonts/Anorexia.ttf';

// Set a random integer for the rotation between -15 and 15 degrees
$rotate = rand(-10, 10);

// Create an image using our original image and adding the detail
imagettftext($image, 24, $rotate, 25, 36, $color, $font, $str);

// Output the image as a png
imagepng($image);

imagedestroy($image);