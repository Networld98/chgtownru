<?php
/*
Plugin Name: Image upload
Plugin URI: http://zelenin.me
Description:
Version: 0.1
Author: Aleksandr Zelenin
Author URI: http://zelenin.me
Author Email: aleksandr@zelenin.me
License:
*/

add_action( 'widgets_init', create_function( '', 'register_widget( "Image_Upload_Widget" );' ) );
class Image_Upload_Widget extends WP_Widget
{
	private $plugin_name = 'Image upload';
	private $plugin_slug;

	public function __construct()
	{
		$this->plugin_slug = preg_replace( '/([\W]+)/', '-', $this->plugin_name );
		parent::__construct( $this->plugin_slug, $this->plugin_name, array(
			'classname' => $this->plugin_slug,
			'description' => ''
		) );
	}

	public function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['image_id'] = $new_instance['image_id'];
		$instance['link'] = $new_instance['link'];

		return $instance;
	}

	public function widget( $args, $instance )
	{
		add_action( 'print_footer_scripts', array(
			$this,
			'add_footer_scripts'
		) );
		require plugin_dir_path( __FILE__ ) . '/views/widget.php';
	}

	public function form( $instance )
	{
		wp_enqueue_media();
		add_action( 'admin_print_footer_scripts', array(
			$this,
			'add_admin_footer_scripts'
		) );
		require plugin_dir_path( __FILE__ ) . '/views/form.php';
	}

	public function add_footer_scripts()
	{
		?>
		<style>
		.image-block img {
			max-width: 100%;
		}
		</style>
	<?php
	}

	public function add_admin_footer_scripts()
	{
		if ( !defined( 'FIELDS_MEDIA_NEW' ) ) {
			DEFINE( 'FIELDS_MEDIA_NEW', true ); ?>
			<script>
				(function ( $ ) {
					$( function () {

						var new_image = {
							'title': 'Upload or Choose Your Custom Image File',
							'button': 'Insert Image into Input Field'
						};

						$( document ).on( 'click', '.custom_upload_image_new_button', function ( event ) {

							event.preventDefault();

							var div = $( this ).closest( 'div' );
							console.log( div );
							var form_field = div.find( '.custom_upload_image' );
							var preview = div.find( '.custom_preview_image' );

							if ( new_media_frame ) {
								new_media_frame.open();
								return;
							}

							var new_media_frame = wp.media.frames.new_media_frame = wp.media( {
								className: 'media-frame new-media-frame',
								frame:     'select',
								multiple:  false,
								title:     new_image.title,
								library:   { type: 'image' },
								button:    { text: new_image.button }
							} );

							new_media_frame.on( 'select', function () {
								var media_attachment = new_media_frame.state().get( 'selection' ).first().toJSON();
								preview.attr( 'src', media_attachment.sizes.thumbnail.url );
								form_field.val( media_attachment.id );
							} );
							new_media_frame.open();
						} );

						$( document ).on( 'click', '.custom_clear_image_button', function ( event ) {
							event.preventDefault();
							var div = $( this ).closest( 'div' );
							var default_image = div.find( '.custom_default_image' ).text();
							div.find( '.custom_upload_image' ).val( '' );
							div.find( '.custom_preview_image' ).attr( 'src', default_image );
						} );

					} );
				})( jQuery );
			</script>
			<style>
				.custom_upload_image_new_button {
					cursor: pointer;
				}
			</style>
		<?php
		}
	}
}