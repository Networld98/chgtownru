<?php
$defaults = array(
	'image_id' => null,
	'title' => '',
	'link' => ''
);
$instance = wp_parse_args( (array)$instance, $defaults );

$title = strip_tags( $instance['title'] );
$image_id = strip_tags( $instance['image_id'] );
$default_image_url = THEME_URL . '/inc/images/image.png';
$image_url = $default_image_url;
$link = strip_tags( $instance['link'] );

if ( $image_id ) {
	$image = wp_get_attachment_image_src( $image_id, 'thumbnail' );
	$image_url = $image[0];
}
?>
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>">Заголовок</label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
	</p>

<?php
echo '<div class="field">';
echo '<span class="custom_default_image" style="display:none">' . $default_image_url . '</span>';
echo '<p><label for="' . $this->get_field_id( 'image_id' ) . '">Картинка</label></p>';
echo '<p><input name="' . $this->get_field_name( 'image_id' ) . '" id="' . $this->get_field_id( 'image_id' ) . '" type="hidden" class="custom_upload_image" value="' . $image_id . '"><img src="' . $image_url . '" class="custom_upload_image_new_button custom_preview_image"><input class="custom_upload_image_new_button button" type="button" value="Choose image"></p>';
echo '<p><a href="#" class="custom_clear_image_button">Remove image</a></p>';
echo '</div>';
echo '<p><label for="' . $this->get_field_id( 'link' ) . '">Ссылка</label><input name="' . $this->get_field_name( 'link' ) . '" id="' . $this->get_field_id( 'link' ) . '" type="text" class="widefat" value="' . $link . '"></p>';
?>