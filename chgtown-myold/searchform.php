<?php get_filename(); ?>

<?php
$searchfield = sprintf('<input type="text" value="" name="s" class="searchfield" placeholder="%s" />', __('Поиск', 'pagelines'));
$searchform = sprintf('<form method="get" class="searchform" onsubmit="this.submit();return false;" action="%s/" ><fieldset>%s</fieldset></form>', home_url(), $searchfield);

if ( $echo )
	echo apply_filters('pagelines_search_form', $searchform);
else
	return apply_filters('pagelines_search_form', $searchform);
?>

<?php get_filename(); ?>