<?php get_filename(); ?>

<section id="boxes" class="container no_clone section-boxes fix">
	<div class="texture">
		<div class="content">
			<div class="content-pad">
				<div class="fboxes fix">
					<div class="plgrid ">
						<div class="plgrid-pad">
							<div class="pprow grid-row fix ">
								<?php
								$custom_query = 'boxes_query';
								$args = array(
									'post_type' => 'boxes',
									'posts_per_page' => 4
								);
								${$custom_query} = new WP_Query( $args );
								$i = 1;
								while ( ${$custom_query}->have_posts() ) : ${$custom_query}->the_post();
									$image_url = get_post_meta( $post->ID, 'the_box_icon', true );
									$url = get_post_meta( $post->ID, 'the_box_icon_link', true );
									?>
									<div class="grid-element pp4 img_grid<?php if ( $i == 4 ) { ?> pplast<?php } ?>">
										<div class="grid-element-pad">
											<div class="fbox ">
												<div class="media box-media inline_thumbs">
													<div class="blocks box-media-pad">
														<div class="fboxgraphic img " style="width: 22%; max-width:65px">
															<?php if ( !empty( $url ) ) { ?><a href="<?php echo $url; ?>"><?php } ?>
															<img src="<?php echo $image_url; ?>" style="max-width: 100%">
															<?php if ( !empty( $url ) ) { ?></a><?php } ?>
														</div>
														<div class="fboxinfo fix bd">
															<div class="fboxtitle">
																<h3>
																	<?php if ( !empty( $url ) ) { ?><a href="<?php echo $url; ?>"><?php } ?>
																	<?php the_title(); ?>
																	<?php if ( !empty( $url ) ) { ?></a><?php } ?>
																</h3>
															</div>
															<div class="fboxtext"><?php echo $post->post_content; ?></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								<?php
								$i++;
								endwhile;
								wp_reset_postdata();
								?>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_filename(); ?>