<?php

add_action('init', 'wp_session_start');
function wp_session_start() {
    if (!session_id())
        session_start();
}

define('THEME_URL', get_stylesheet_directory_uri());
define('THEME_PATH', get_stylesheet_directory());
define('SITE_URL', get_home_url());

get_template_part('inc/config');
include_once(THEME_PATH.'/inc/captcha/refresh_captcha.php');

//add_theme_support( 'automatic-feed-links' );

add_theme_support('post-thumbnails'); // the_post_thumbnail( 'thumbnail' );
set_post_thumbnail_size(300, 200, true);
add_image_size('slider', 1051, 290, true); // the_post_thumbnail( 'slider' );
add_image_size('slider-main', 720, 430);
add_image_size('slider-preview', 180);

add_action('init', 'set_role_to_guest');
function set_role_to_guest(){
    if (!is_user_logged_in()){
        $current_user = wp_get_current_user();
        $current_user->add_role('advert');
    }
}

// add_filter( 'image_size_names_choose', 'image_sizes' );
function image_sizes($sizes)
{
    $addsizes = array(
        'slider' => 'Слайдер'
    );
    $newsizes = array_merge($sizes, $addsizes);
    return $newsizes;
}

add_action('widgets_init', 'widgets_init');
function widgets_init()
{

    $sidebars = array(

        'sb_primary' => array(
            'name' => __('Primary Sidebar', 'pagelines'),
            'description' => __('The main widgetized sidebar.', 'pagelines')
        ),
        'sb_secondary' => array(
            'name' => sprintf('%s%s', __('Secondary Sidebar', 'pagelines'), (!VPRO) ? ' (Pro Only)' : ''),
            'description' => __('The secondary widgetized sidebar for the theme.', 'pagelines')
        ),
        'sb_tertiary' => array(
            'name' => __('Tertiary Sidebar', 'pagelines'),
            'description' => __('A 3rd widgetized sidebar for the theme that can be used in standard sidebar templates.', 'pagelines')
        ),
        'sb_universal' => array(
            'name' => __('Universal Sidebar', 'pagelines'),
            'description' => __('A universal widgetized sidebar', 'pagelines'),
            'pro' => true
        ),
        'sb_fullwidth' => array(
            'name' => __('Full Width Sidebar', 'pagelines'),
            'description' => __('Shows full width widgetized sidebar.', 'pagelines')
        ),
        'sb_content' => array(
            'name' => __('Content Sidebar', 'pagelines'),
            'description' => __('Displays a widgetized sidebar inside the main content area. Set it up in the widgets panel.', 'pagelines')
        ),
    );

    $i = 1;
    foreach ($sidebars as $key => $sidebar) {
        register_sidebar(array(
            'name' => $sidebar['name'],
            'id' => 'sidebar-' . $i,
            'description' => $sidebar['description'],
            'before_widget' => '<li id="%1$s" class="%2$s widget fix"><div class="widget-pad">',
            'after_widget' => '</div></li>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));
        $i++;
    }

    register_sidebar(array(
        'name' => 'Footer',
        'id' => 'footer',
        'description' => __('Use this sidebar if you want to use widgets in your footer columns instead of the default.', 'pagelines'),
        'before_widget' => sprintf('<div class="pp%s footcol"><div class="footcol-pad">', 5),
        'after_widget' => '</div></div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));

}

register_nav_menus(array(
    'menu-1' => 'Menu 1',
    'menu-2' => 'Menu 2',
    'menu-3' => 'Menu 3',
    'menu-4' => 'Menu 4',
    'menu-5' => 'Menu 5'
));

// add_action( 'admin_menu', 'admin_menu' );
function admin_menu()
{
    remove_menu_page('edit-comments.php');
}

function pagination($prev = '«', $next = '»')
{
    global $wp_query, $wp_rewrite;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    $pagination = array(
        'base' => @add_query_arg('paged', '%#%'),
        'format' => '',
        'total' => $wp_query->max_num_pages,
        'current' => $current,
        'prev_text' => __($prev),
        'next_text' => __($next),
        'type' => 'plain'
    );
    if ($wp_rewrite->using_permalinks())
        $pagination['base'] = user_trailingslashit(trailingslashit(remove_query_arg('s', get_pagenum_link(1))) . 'page/%#%/', 'paged');

    if (!empty($wp_query->query_vars['s']))
        $pagination['add_args'] = array('s' => get_query_var('s'));

    echo paginate_links($pagination);
}

function wp_corenavi()
{
    global $wp_query, $wp_rewrite;
    $pages = '';
    $max = $wp_query->max_num_pages;
    if (!$current = get_query_var('paged')) $current = 1;
    $a['base'] = ($wp_rewrite->using_permalinks()) ? user_trailingslashit(trailingslashit(remove_query_arg('s', get_pagenum_link(1))) . 'page/%#%/', 'paged') : @add_query_arg('paged', '%#%');
    if (!empty($wp_query->query_vars['s'])) $a['add_args'] = array('s' => get_query_var('s'));
    $a['total'] = $max;
    $a['current'] = $current;

    $total = 0; //1 - выводить текст "Страница N из N", 0 - не выводить
    $a['mid_size'] = 10; //сколько ссылок показывать слева и справа от текущей
    $a['end_size'] = 1; //сколько ссылок показывать в начале и в конце
    $a['prev_text'] = '&laquo;'; //текст ссылки "Предыдущая страница"
    $a['next_text'] = '&raquo;'; //текст ссылки "Следующая страница"

    if ($max > 1) echo '<div class="navigation">';
    if ($total == 1 && $max > 1) $pages = '<span class="pages">Страница ' . $current . ' из ' . $max . '</span>' . "\n";
    echo $pages . paginate_links($a);
    if ($max > 1) echo '</div>';
}

function wp_corenavi_comments()
{
    global $wp_query, $wp_rewrite;
    $pages = '';
    $max = $wp_query->max_num_pages;
    if (!$current = get_query_var('paged')) $current = 1;
    $a['base'] = ($wp_rewrite->using_permalinks()) ? user_trailingslashit(trailingslashit(remove_query_arg('s', get_pagenum_link(1))) . 'page/%#%/', 'paged') : @add_query_arg('paged', '%#%');
    if (!empty($wp_query->query_vars['s'])) $a['add_args'] = array('s' => get_query_var('s'));
    $a['total'] = $max;
    $a['current'] = $current;

    $total = 0; //1 - выводить текст "Страница N из N", 0 - не выводить
    $a['mid_size'] = 3; //сколько ссылок показывать слева и справа от текущей
    $a['end_size'] = 1; //сколько ссылок показывать в начале и в конце
    $a['prev_text'] = '&laquo;'; //текст ссылки "Предыдущая страница"
    $a['next_text'] = '&raquo;'; //текст ссылки "Следующая страница"

    if ($max > 1) echo '<div class="navigation">';
    if ($total == 1 && $max > 1) $pages = '<span class="pages">Страница ' . $current . ' из ' . $max . '</span>' . "\r\n";
    echo $pages . paginate_comments_links('prev_text=<<<&next_text=>>>');
    if ($max > 1) echo '</div>';
}

add_action('wp_head', 'theme_header');
function theme_header()
{
    echo PHP_EOL;
    echo '<meta charset="' . get_bloginfo('charset') . '">' . PHP_EOL;
    echo '<meta name="copyright" content="Wordpress programming © 2013 Aleksandr Zelenin, aleksandr@zelenin.me">' . PHP_EOL;
    echo '<link rel="shortcut icon" href="/favicon.ico">' . PHP_EOL;
    echo '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">' . PHP_EOL;
    echo '<meta name="viewport" content="width=device-width, initial-scale=1">' . PHP_EOL;
    echo '<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">' . PHP_EOL;
}

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'feed_links_extra', 3);
//remove_action( 'wp_head', 'feed_links', 2 );

// add_action( 'template_redirect', 'rss_redirect' );
function rss_redirect()
{
    if (is_feed() && !preg_match('/feedburner|feedvalidator/i', $_SERVER['HTTP_USER_AGENT'])) {
        wp_redirect('http://feeds.feedburner.com/smashingmagazine');
    }
}

add_filter('jpeg_quality', 'jpeg_full_quality');
function jpeg_full_quality($quality)
{
    return 100;
}

function get_filename()
{
    $array = debug_backtrace();
    $file = basename($array[0]['file']);
    echo '<!-- ' . $file . ' -->' . "\n";
}

//show_admin_bar( false );

add_filter('body_class', 'browser_body_class');
function browser_body_class($classes)
{

    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

    if ($is_lynx) $classes[] = 'lynx';
    elseif ($is_gecko) $classes[] = 'gecko';
    elseif ($is_opera) $classes[] = 'opera';
    elseif ($is_NS4) $classes[] = 'ns4';
    elseif ($is_safari) $classes[] = 'safari';
    elseif ($is_chrome) $classes[] = 'chrome';
    elseif ($is_IE) $classes[] = 'ie';
    else $classes[] = 'unknown';
    if ($is_iphone) $classes[] = 'iphone';
    return $classes;

}

add_filter('post_class', 'post_classes');
function post_classes($classes)
{
    global $wp_query;

    if ($wp_query->found_posts < 1) return $classes;
    if ($wp_query->current_post == 0) $classes[] = 'post-first';
    if ($wp_query->current_post == ($wp_query->post_count - 1)) $classes[] = 'post-last';
    $classes[] = ($wp_query->current_post + 1) % 2 ? 'post-notsecond' : 'post-second';
    $classes[] = ($wp_query->current_post + 1) % 3 ? 'post-notthird' : 'post-third';
    $classes[] = ($wp_query->current_post + 1) % 4 ? 'post-notfourth' : 'post-fourth';
    $classes[] = ($wp_query->current_post + 1) % 5 ? 'post-notfifth' : 'post-fifth';
    return $classes;

}


//*** Новый код под Объявления**/////
function getChildTerms($id)
{
    $array_cterms = get_terms('adverts', array('hide_empty' => 0, 'hierarchical' => false, 'parent' => $id));
    return $array_cterms;
};

function cp_cat_menu_child($slug = false)
{
    $terms_array = get_terms('adverts', array('hide_empty' => 0, 'slug' => $slug));
    foreach ($terms_array as $key => $value) {
        $child_array = getChildTerms($value->term_id);
        if (count($child_array) != 0) {
            $str = '<div class="w-menu"><ul class="nav nav-tabs">';
            foreach ($child_array as $key => $value) {
                $str .= '<li ><a href="' . get_term_link((int)$value->term_id, 'adverts') . '">' . $value->name . '</span></a></li>';
            }
            echo $str . '</ul></div>';
        }
    }
}

function cp_cat_menu()
{
    $str = '<div class="w-menu"><ul class="nav nav-tabs">';
    $terms_array = get_terms('adverts', array('hide_empty' => 0, 'parent' => 0));
    foreach ($terms_array as $key => $value) {
        if ($value->parent == 0) {
            $str .= '<li ><a  href="' . get_term_link((int)$value->term_id, 'adverts') . '">' . $value->name . '</span></a></li>';
        }
    }
    echo $str . '</ul></div>';
}


function style_adverts()
{
    wp_enqueue_style('true_style', get_stylesheet_directory_uri() . '/css/note.css');
}
add_action('wp_enqueue_scripts', 'style_adverts');
function words_limit($input_text, $limit = 10, $end_str = '') {
    $input_text = strip_tags(trim($input_text));
    $words = explode(' ', $input_text); // создаём из строки массив слов
    if ($limit < 1 || sizeof($words) <= $limit) { // если лимит указан не верно или количество слов меньше лимита, то возвращаем исходную строку
        return $input_text;
    }
    $words = array_slice($words, 0, $limit); // укорачиваем массив до нужной длины
    $out = implode(' ', $words);
    return $out.$end_str; //возвращаем строку + символ/строка завершения
}

//function count_cat_inTerm($slug)
//{
//    $args = array(
//        'tax_query' => array(
//            array(
//                'taxonomy' => 'adverts',
//                'field' => 'slug',
//                'terms' => $slug
//            )
//        )
//    );
//    $query = new WP_Query($args);
//    return $query->post_count;
//}

add_action('wp_ajax_get_advert_params', 'get_advert_params');
add_action('wp_ajax_nopriv_get_advert_params', 'get_advert_params');
function get_advert_params($post_id = null, $param = false,$filter=false)
{
    /**
     * if(!$post_id){
     *  if($_POST['page']=='filter')
     *     отображаем поля на фильтр с аяксом
     * elseif($_POST['page']=='nofilter')
     *     отображаем поля на добавление объявления
     * }
     * else{
     *  отображаем при загрузке страницы с фильтром
     * }
     */

    $id = (isset($_POST['id_term']) ? $_POST['id_term'] : ($post_id ? $post_id : ''));

    if (!empty($id)) {
        $args = array(
            'numberposts' => -1,
            'post_type' => 'acf',
            'post_status' => 'publish',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'rule',
                    'value' => $id,
                    'compare' => 'LIKE'
                )
            )
        );
        if ($_POST['page']!='nofilter') {
            $args['meta_query'][] = array(
                'key' => '_hide_on_filter',
                'value' => '',
                'compare' => '='
            );
        }
        // get results
        $the_query = get_posts($args);
        ob_start();
        foreach ($the_query as $key => $val) {
            $fields = apply_filters('acf/field_group/get_fields', FALSE, $val->ID);
            foreach ($fields as $field) {
                if ($post_id && $field['type'] == 'repeater') { //TODO: возможно нужно добавит ьпроверку на subField==image
                    continue;
                } elseif ($field['name'] == 'price_advert' && ((!$post_id && ($_POST['page'] == 'filter')) || $post_id)) {
                    echo '<select name="fields[price_advert][]">
                    <option value="0" selected="selected">Цена от </option>
                    <option value="0">0</option>
                    <option value="1000">1000</option>
                    <option value="2000">2000</option>
                    <option value="3000">3000</option>
                    <option value="5000">5000</option>
                    <option value="10000">10000</option>
                    <option value="20000">20000</option>
                    <option value="50000">50000</option>
                    <option value="100000">100000</option>
                    <option value="200000">200000</option>
                    <option value="300000">300000</option>
                    <option value="400000">400000</option>
                    <option value="500000">500000</option>
                    <option value="1000000">1000000</option>
                    <option value="5000000">5000000</option>
                    <option value="10000000">10000000</option>

                    </select>';
                    echo '<select name="fields[price_advert][]">
                    <option value="1000000000" selected="selected">Цена до </option>
                    <option value="1000">1000 </option>
                    <option value="2000">2000 </option>
                    <option value="3000">3000 </option>
                    <option value="5000">5000 </option>
                    <option value="10000">10000 </option>
                    <option value="20000">20000 </option>
                    <option value="50000">50000 </option>
                    <option value="100000" >100000</option>
                    <option value="200000">200000</option>
                    <option value="300000">300000</option>
                    <option value="400000">400000</option>
                    <option value="500000">500000</option>
                    <option value="1000000">1000000</option>
                    <option value="5000000">5000000</option>
                    <option value="10000000">10000000</option>
                    <option value="1000000000">>10000000</option>
                    </select>';
                } else {
                    $data = $field;
                    $data['name'] = 'fields[' . $field['name'] . ']';
                    $data['value'] = $param[$field['name']];
                    do_action('acf/create_field', $data);

                    /*do_action('acf/create_field', array(
                        'type'          => $field['type'],
                        'placeholder'   => $field['placeholder'],
                        'name'          => 'fields[' . $field['name'] . ']',
                        'value'         => $param[$field['name']],
                        'choices'       => @$field['choices'],
                        //'allow_null'    => @$field['label'],
                    ));*/
                }
            }
        }
        $out = ob_get_clean();
        echo $out;

        if ($post_id)
            return;
    }

    die();
}

add_action('wp_ajax_get_advert_subCat', 'get_advert_subCat');
//print select input
add_action('wp_ajax_nopriv_get_advert_subCat', 'get_advert_subCat');
function get_advert_subCat($id_cat = null, $active = false)
{
    $id = (isset($_POST['id_term']) ? $_POST['id_term'] : ($id_cat ? $id_cat : ''));
    if (!empty($id)) {
        $args = get_terms('adverts', array('hide_empty' => 0, 'parent' => $id));
        $str = '';
        if (count($args) > 0) {
            ob_start();
            $str .= '<select id="acf-field-adverts_sub_cat" name="adverts_sub_cat">';
            $str .= '<option value="">-- Выбрать --</option>';
            foreach ($args as $key => $value) {
                if ($value->slug == $active) {
                    $sel = 'selected="selected"';
                } else {
                    $sel = '';
                }
                $str .= '<option ' . $sel . ' value="' . $value->slug . '" data-catid="' . $value->term_id . '"> ' . $value->name . '</option>';
            }
            $str .= '</select>';
            ob_end_clean();
        }
        echo $str;
        if ($id_cat)
            return;
    }
    die();
}

function post_custom_options()
{
    global $post;

    if ($post->post_type == 'acf')
        echo '<div class="misc-pub-section misc-pub-hide-on-filter">
       <label for="hide-on-filter">Спрятать в фильтре:</label>
       <input name="_hide_on_filter" type="checkbox" value="1" id="hide-on-filter" ' . __checked_selected_helper('1', (int)get_post_meta($post->ID, "_hide_on_filter", true), false, 'checked') . ' />
    </div>';
}

add_action('post_submitbox_misc_actions', 'post_custom_options');

function update_post_custom_options()
{
    global $post;
    if ($post->post_type == 'acf') {
        $_hide_on_filter = $_REQUEST['_hide_on_filter'];
        if (!update_post_meta($post->ID, '_hide_on_filter', $_hide_on_filter))
            add_post_meta($post->ID, '_hide_on_filter', $_hide_on_filter, true);
    }
}

add_action('save_post', 'update_post_custom_options');

//return int (parent id)
function getParentTerm()
{
    $id_term = null;
    $obj_term = get_term_by('slug', get_query_var('term'), 'adverts');
    if ($obj_term->parent != 0)
        $id_term = get_term($obj_term->parent, 'adverts')->term_id;
    else
        $id_term = $obj_term->term_id;
    return $id_term;
}

function save_advert()
{
    if ($_POST) {
        if (!function_exists('wp_handle_upload'))
            require_once(ABSPATH . 'wp-admin/includes/file.php');

        require_once(ABSPATH . 'wp-admin/includes/image.php');

        // create user
        if (is_user_logged_in()) {
            $author_id = get_current_user_id();
            $userdata = get_userdata($author_id);
            $user_first_name=get_user_meta($author_id,'first_name',true);
            $user_phone=get_user_meta($author_id,'phone_number',true);
            $user_mail=$userdata->data->user_email;
            //*условия для изменения параметров юзера если он изменит свои данные при создании объявления*/
            if ($_POST['user_advert']['mail'] !== $user_mail) {
                wp_update_user(
                    array(
                        'ID' => $author_id,
                        'user_email' => $_POST['user_advert']['mail'],
                        'user_login' => $_POST['user_advert']['mail']
                    )
                );
            }
            if (!empty($_POST['user_advert']['name']) && $_POST['user_advert']['name'] !== $user_first_name) {
                update_user_meta($author_id, 'first_name', $_POST['user_advert']['name']);
            }
            if (!empty($_POST['user_advert']['phone']) && $_POST['user_advert']['phone'] !== $user_phone) {
                update_user_meta($author_id, 'phone_number', $_POST['user_advert']['phone']);
            }
            /***конец условий изменений параметров****/
        } else {
            $author_id = email_exists($_POST['user_advert']['mail']);
            if (!$author_id) {
                $author_id = wp_insert_user(array(
                    'user_pass'  => '111111',
                    'user_login' => trim($_POST['user_advert']['mail']),
                    'user_email' => trim($_POST['user_advert']['mail']),
                    'first_name' => trim($_POST['user_advert']['name']),
                    'role'       => 'advert'
                ));

                global $wpdb;
                $wpdb->update($wpdb->users, array( 'user_status' => 1 ), array( 'user_login' => trim($_POST['user_advert']['mail']) ) );

                update_user_meta($author_id, 'phone_number', $_POST['user_advert']['phone']);
            }
        }

        $args = array(
            'post_type'     => 'advert',
            'post_status'   => 'publish',//pending
            'post_title'    => esc_textarea($_POST['title_advert']),
            'post_content'  => nl2br(wp_strip_all_tags($_POST['content_advert'])),
            'tax_input'     => array('adverts' => array()),
            'post_author'   => $author_id
        );

        if (!empty($_POST['adverts_cat'])) {
            $cat = get_term_by('slug', $_POST['adverts_cat'], 'adverts');
            $args['tax_input']['adverts'][] = $cat->term_id;
        };

        if (!empty($_POST['adverts_sub_cat'])) {
            $sub_cat = get_term_by('slug', $_POST['adverts_sub_cat'], 'adverts');
            $args['tax_input']['adverts'][] = $sub_cat->term_id;
        };

        $post_id = wp_insert_post($args);

        if ($post_id && !is_wp_error($post_id)) {
            if (count($_POST['fields']) > 0){
                foreach ($_POST['fields'] as $name => $val){
                    add_post_meta($post_id, $name, trim($val));
                    add_post_meta($post_id, '_'.$name, acf_get_field_key($name));
                }
            }

            if ($_FILES['imageForAdvert']['size'] > 0) { //проверяем если ли что-то вообще
                $_files = reformat_files_array($_FILES['imageForAdvert']);
                $_files_count = count($_files);

                $field_images = apply_filters('acf/load_field', FALSE, 'field_528b616838aba');
                $field_images_val = array();
                for ($i = 0; $i < $_files_count; $i++) {
                    $wp_filetype = wp_check_filetype($_files[$i]['name']);

                    if ($_files[$i]['size'] > 0 && wp_ext2type($wp_filetype['ext']) == 'image') {
                        $movefile = wp_handle_upload($_files[$i], array('test_form' => FALSE));

                        if(isset($movefile['file'])){
                            $attachment = array(
                                'guid' => $movefile['url'],
                                'post_mime_type' => $wp_filetype['type'],
                                'post_title' => preg_replace('/\.[^.]+$/', '', basename($movefile['file'])),
                                'post_content' => '',
                                'post_status' => 'inherit'
                            );
                            $attach_id = wp_insert_attachment($attachment, $movefile['file'], $post_id);
                            $attach_data = wp_generate_attachment_metadata($attach_id, $movefile['file']);
                            wp_update_attachment_metadata($attach_id, $attach_data);

                            $field_images_val[] = array($field_images['sub_fields'][0]['key'] => $attach_id);
                        }
                    }
                }

                do_action('acf/update_value', $field_images_val, $post_id, $field_images);
            };

            wp_redirect('advert_preview/?ad=' . $post_id);

        }
        else{
            wp_redirect('advert_preview/?message=2');
        }
    }
    else{
        wp_redirect('advert_preview/?message=1');
    }

    exit;
}

function reformat_files_array($vector)
{
    $result = array();
    foreach ($vector as $key1 => $value1)
        foreach ($value1 as $key2 => $value2)
            $result[$key2][$key1] = $value2;
    return $result;
}

function setSearchParam($post_param)
{
    echo '<div class="divline2"><div class="petrjus-select" id="subCategory_and_params">';

    if (!empty($post_param['adverts_cat'])) {
        $id_cat_term = get_term_by('slug', $post_param['adverts_cat'], 'adverts');
        if (!empty($post_param['adverts_sub_cat'])) {
            $sub_active_id = $post_param['adverts_sub_cat'];
            if ($id_cat_term->term_id) {
                get_advert_subCat($id_cat_term->term_id, $sub_active_id);
            }
        } elseif ($id_cat_term->term_id) {
            get_advert_subCat($id_cat_term->term_id);
        }
    }
    if (!empty($post_param['adverts_sub_cat'])) {
        $id_sub_cat_term = get_term_by('slug', $post_param['adverts_sub_cat'], 'adverts');
        get_advert_params($id_sub_cat_term->term_id, $post_param['fields'],'filter');
    }
    echo '</div></div>';
}

function advert_preview($id_post)
{
    $Allpost = get_post($id_post);
    return $Allpost;
}

function printCategory($ar)
{
    $str = '';
    foreach ($ar as $key=>$val) {
        $key==0?$str .= ' ' .$val->name:$str .= ' &#9654 ' .$val->name;
    }
    return $str == '' ? false : $str;
}

//обновляем пароли пользователю
function update_userdata($params)
{
    global $wpdb;
    if (trim($params['fpassword']) == trim($params['frepassword']) && !empty($params['fpassword'])) {
        $user_id = email_exists($params['email']);
        if ($user_id != null) {
            $userdata = get_userdata($user_id);
            $user_status=$userdata->data->user_status;
            if($user_status==1){
                wp_update_user(
                    array(
                        'ID' => $user_id,
                        'user_pass' => $params['fpassword']
                    )
                );
                $wpdb->update($wpdb->users, array( 'user_status' => 0 ), array( 'user_login' => trim($params['email']) ) );
            }
        }
        return true;
    } else {
        return false;
    }
}

function generateActiveKey($mail,$pass){
    $str=$mail+$pass+'key';
    // Now insert the key, hashed, into the DB.
    if ( empty( $wp_hasher ) ) {
        require_once ABSPATH . 'wp-includes/class-phpass.php';
        $wp_hasher = new PasswordHash( 8, true );
    }
    $hashed = $wp_hasher->HashPassword($str);
    $message = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
    $message .= network_home_url( '/' ) . "\r\n\r\n";
    $message .= sprintf(__('Username: %s'), $mail) . "\r\n\r\n";
    $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
    $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
    $message .= '<' . network_site_url("advert_seccess.php?action=rp&key=$hashed&login=" . rawurlencode($mail), 'login') . ">\r\n";
    $message && !wp_mail($mail, 'Активация аккаунта на chgTown.ru', $message);
    return $hashed;

};

//Добавляю поле телефона

function show_profile_fields($user)
{
    ?>
    <h3>Дополнительная информация</h3>
    <table class="form-table">
        <tr>
            <th><label for="city">Номер телефона</label></th>
            <td><input type="text" name="phone_number" id="phone"
                       value="<?php echo esc_attr(get_the_author_meta('phone_number', $user->ID)); ?>"
                       class="regular-text"/><br/></td>
        </tr>

        </tr>
    </table>
<?php
}

add_action('show_user_profile', 'show_profile_fields');
add_action('edit_user_profile', 'show_profile_fields');

function save_profile_fields($user_id)
{
    if (!current_user_can('edit_user', $user_id))
        return false;
    update_usermeta($user_id, 'phone_number', $_POST['phone_number']);
}

add_action('personal_options_update', 'save_profile_fields');
add_action('edit_user_profile_update', 'save_profile_fields');

add_action('wp_ajax_send_question', 'send_question');
//print select input
add_action('wp_ajax_nopriv_send_question', 'send_question');
function send_question()
{
    $userdata = get_userdata($_POST['post_author']);
    if($userdata){
        $user_mail = $userdata->data->user_email;
        $headers = 'From: ' . $_POST["name"] . ' <' . $_POST["email"] . '>' . "\r\n"; // в виде строки
        if (wp_mail($user_mail, 'Вопрос по объявлению на ЧгТаун', $_POST['mess'], $headers)) {
            echo '0';
        } else
            echo '1';
    }else{
        echo '2';
    }
}

add_action('wp_ajax_search_question', 'search_question');
//print select input
add_action('wp_ajax_nopriv_search_question', 'search_question');
function search_question()
{
    global $wpdb;
    $str="";
    $ch=$_GET['q'];
    $search_tags = $wpdb->get_results("SELECT post_title FROM $wpdb->posts WHERE post_type='advert' AND post_status='publish' AND post_title LIKE '%$ch%'");
    //var_dump($search_tags);
    foreach ($search_tags as $mytag) {
        echo $mytag->post_title. "\n";
    };
    echo $str;
    exit();
}

add_action('wp_ajax_check_captcha', 'check_captcha');
add_action('wp_ajax_nopriv_check_captcha', 'check_captcha');
function check_captcha(){
    if(strtoupper($_GET['captcha']) == $_SESSION['captcha'])
        echo 'true';
    else
        echo 'false';

    die(0);
}

function acf_get_field_key($field_name){
    global $wpdb;

    return $wpdb->get_var($wpdb->prepare(
        "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value LIKE %s LIMIT 1",
        '_'.$field_name,
        'field\_%'
    ));
}