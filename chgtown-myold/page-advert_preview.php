<?php
global $current_user;
if(isset($_POST['update_user'])){
    if(update_userdata($_POST)){
        wp_redirect('/advert_success');
        $error=false;
    }else{
        $error="Пароли не совпадают!";
    }
}
if (isset($_GET['ad'])){
    $post_id=(int)$_GET['ad'];
    $post=get_post($post_id);
}else{
    wp_redirect('/404.php');
    exit;
}

get_header();
get_filename();
?>
<div id="page-main" class="container-group">
    <div id="dynamic-content" class="outline">

        <section id="content" class="container no_clone section-content-area fix">
            <div class="texture">
                <div class="content">
                    <div class="content-pad">
                        <div id="pagelines_content" class="one-sidebar-right fix">
                            <div id="column-wrap" class="fix">
                                <div id="column-main" class="mcolumn fix">
                                    <div class="mcolumn-pad">
                                        <section id="postloop" class="copy no_clone section-postloop">
                                            <div class="copy-pad">
                                                <article <?php post_class('fpost'); ?>>
                                                    <div class="hentry-pad">
                                                        <div class="entry_wrap fix">
                                                            <div class="entry_content">
                                                                <?php
                                                                $fields = get_field_objects();
                                                                $category=wp_get_post_terms($post_id,'adverts');
                                                                $user_id = (int)$post->post_author;
                                                                $userdata = get_userdata($user_id);
                                                                $user_first_name=get_user_meta($user_id,'first_name',true);
                                                                $user_mail=$userdata->data->user_email;
                                                                $user_phone=get_user_meta($user_id,'phone_number',true);
//                                                                var_dump($post,$userdata->data,$user_first_name);
                                                                if(!empty($fields)){?>
                                                                    <div class="w-info-read">
                                                                        <ul>
                                                                            <li>
                                                                                <ul>
                                                                                    <li class="w-listname">Ваше имя</li>
                                                                                    <li class="w-listresult"><?php echo $user_first_name?></li>
                                                                                </ul>
                                                                            </li>

                                                                            <li>
                                                                                <ul>
                                                                                    <li class="w-listname">Электронная почта</li>
                                                                                    <li class="w-listresult"><?php echo $user_mail?></li>
                                                                                </ul>
                                                                            </li>

                                                                            <li>
                                                                                <ul>
                                                                                    <li class="w-listname">Номер телефона</li>
                                                                                    <li class="w-listresult"><?php echo $user_phone?></li>
                                                                                </ul>
                                                                            </li>

                                                                            <li>
                                                                                <ul>
                                                                                    <li class="w-listname">Название объявления</li>
                                                                                    <li class="w-listresult"><?php the_title() ?></li>
                                                                                </ul>
                                                                            </li>
                                                                            <li>
                                                                                <ul>
                                                                                    <li class="w-listname">Категория</li>
                                                                                    <li class="w-listresult"><?php echo printCategory($category)?printCategory($category):'Не указано'; ?></li>
                                                                                </ul>
                                                                            </li>

                                                                            <li>
                                                                                <ul>
                                                                                    <li class="w-listname">Описание объявления</li>
                                                                                    <li class="w-listresult"><?php echo $post->post_content ?></li>
                                                                                </ul>
                                                                            </li>

                                                                            <li>
                                                                                <ul>
                                                                                    <li class="w-listname">Фотографии</li>
                                                                                    <li class="w-listresult">
                                                                                        <?php if (get_field('user_images_group_advance')) {
                                                                                            while (has_sub_field('user_images_group_advance'))
                                                                                                echo '<div class="w-listresult-images">'. wp_get_attachment_image(get_sub_field('user_image_advance'), 'slider-preview') .'</div>';
                                                                                        } else {
                                                                                            echo '<img src="' . THEME_URL . '/images/no_photo.png">';
                                                                                        }?>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <?php if (!is_user_logged_in() && $userdata->data->user_status != 0){ ?>
                                                                        <div class="w-status-info"><label>Объявление успешно добавлено!  <a href="/adverts">Вернуться в каталог</a> </label> </div>
<!--                                                                    <div class="w-miniform w-info-read">-->
<!--                                                                        <span class="w-zag">Придумайте пароль для входа на сайт</span>-->
<!--                                                                        <ul>-->
<!--                                                                            <form method="post" id="new-pass-form" action="">-->
<!--                                                                                  <input type="hidden" value="--><?php //echo $post_id ?><!--" readonly>-->
<!--                                                                                  <input type="hidden" name="email" value="--><?php //echo $user_mail ?><!--" readonly>-->
<!--                                                                            <li>-->
<!--                                                                                <ul>-->
<!--                                                                                    <li class="w-listname"><label>Электронная почта</label></li>-->
<!--                                                                                    <li class="w-listresult">--><?php //echo $user_mail?><!-- </li>-->
<!--                                                                                </ul>-->
<!--                                                                            </li>-->
<!---->
<!--                                                                            <li>-->
<!--                                                                                <ul>-->
<!--                                                                                    <li class="w-listname"><label>Пароль</label></li>-->
<!--                                                                                    <li class="w-listresult"><input type="password"   name="fpassword" /></li>-->
<!--                                                                                </ul>-->
<!--                                                                            </li>-->
<!--                                                                            <li>-->
<!--                                                                                <ul>-->
<!--                                                                                    <li class="w-listname"><label>Повторите пароль</label></li>-->
<!--                                                                                    <li class="w-listresult"><input type="password"  name="frepassword" /></li>-->
<!--                                                                                </ul>-->
<!--                                                                            </li>-->
<!--                                                                             --><?php ////}?>
<!---->
<!--                                                                                --><?php //if($error){?>
<!--                                                                                    <li>-->
<!--                                                                                        <ul>-->
<!--                                                                                            <li class="w-listname"><label>Ошибка</label></li>-->
<!--                                                                                            <li class="w-listresult">--><?php //echo $error?><!-- </li>-->
<!--                                                                                        </ul>-->
<!--                                                                                    </li>-->
<!--                                                                                --><?php //}?>
<!--                                                                            <li>-->
<!--                                                                                <ul>-->
<!--                                                                                    <li class="w-listname"><label></label></li>-->
<!--                                                                                    <li class="w-listresult"><input type="submit" value="Далее"  name="update_user" /></li>-->
<!--                                                                                </ul>-->
<!--                                                                            </li>-->
<!--                                                                            </form>-->
<!--                                                                        </ul>-->
<!--                                                                    </div>-->

                                                               <?php }else{
                                                                        echo '<div class="w-status-info">
                                                                            <label>Объявление успешно добавлено!   <a href="/adverts">Вернуться в каталог</a> </label></div>';
                                                                    }}else{
                                                                    die("Такой пост не существует");
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>

                                                <div class="clear"></div>
                                            </div>
                                        </section>

                                        <?php
                                        wp_reset_postdata();
                                        ?>

                                    </div>
                                </div>

                            </div>

                            <div id="sidebar-wrap" class="">
                                <div  class="scolumn">
                                </div>
                                <div id="sidebar1" class="scolumn">
                                    <div class="scolumn-pad">    </div>
                                </div>
                                <section id="sb_primary">
                                    <div class="copy-pad">
                                        <ul id="list_sb_primary"
                                            class="sidebar_widgets fix"><?php if (!dynamic_sidebar('sidebar-1')) : ?><?php endif; ?></ul>
                                        <div class="clear"></div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="morefoot_area" class="container-group">
        <section id="sb_footcols" class="container no_clone section-sb_footcols fix">
            <div class="texture">
                <div class="content">
                    <div class="content-pad">
                        <div class="fcolumns ppfull pprow">
                            <div
                                class="fcolumns-pad fix"><?php if (!dynamic_sidebar('footer')) : ?><?php endif; ?></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="clear"></div>

</div>

</div>
</div>
<script src="<?php echo get_template_directory_uri() ?>/js/jquery.validate.min.js"></script>
<script>
    jQuery.validator.setDefaults({
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        }
    });

    jQuery().ready(function($) {
        // validate signup form on keyup and submit
        $("#new-pass-form").validate({
            errorClass: "w-info-error",
            rules: {
                'fpassword': {
                    required: true,
                    minlength: 6
                },
                'frepassword':{
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                'password': "Минимум 6 символов!",
                'repassword': "Минимум 6 символов!"

            }
        });

</script>
<?php
get_filename();
get_footer();
?>
