<!doctype html>
<html>
<head>

    <title>

        <?php
        global $page, $paged;
        wp_title('|', true, 'right');
        bloginfo('name');
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page())) echo " | $site_description";
        if ($paged >= 2 || $page >= 2) echo ' | ' . sprintf('Страница %s', max($paged, $page));
        ?></title>



    <?php wp_head(); ?>
    <script type="text/javascript" src="/js/b.js"></script>
    <script type="text/javascript" src="/js/open.js"></script>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?86"></script>

    <link href='http://fonts.googleapis.com/css?family=PT+Serif:400,700,400italic&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>

    <script type="text/javascript">
        VK.init({apiId: 3523458, onlyWidgets: true});
    </script>


    <script src="//api-maps.yandex.ru/2.0/?load=package.standard,package.route&lang=ru-RU"
            type="text/javascript"></script>

    <script type="text/javascript">
        ymaps.ready(init);

        function init() {
            var myMap = new ymaps.Map("map", {
                center: [55.911083, 38.06693],
                zoom: 10
            });

            ymaps.route([
                    'Московская область, Черноголовка',
                    'Москва, метро Щелковская'
                ]).then(function (route) {
                    route.getPaths().options.set({
                        strokeColor: '740C00ff',
                        opacity: 0.6
                    });

                    var points = route.getWayPoints(),
                        lastPoint = points.getLength() - 1;

                    points.options.set('preset', 'twirl#redStretchyIcon');
                    points.get(0).properties.set('iconContent', '');
                    points.get(lastPoint).properties.set('iconContent', '<b>Чг-Москва</b>, м. Щелковская<br/>' + route.getHumanJamsTime() + ' ');

                    myMap.geoObjects.add(route);
                });


            ymaps.route([
                    'Москва, метро Щелковская',
                    'Московская область, Черноголовка'

                ]).then(function (route) {
                    route.getPaths().options.set({
                        strokeColor: '000000ff',
                        opacity: 0.6
                    });

                    var points = route.getWayPoints(),
                        lastPoint = points.getLength() - 1;

                    points.options.set('preset', 'twirl#blackStretchyIcon');
                    points.get(0).properties.set('iconContent', '');
                    points.get(lastPoint).properties.set('iconContent', '<b>Москва-Чг</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br> ' + route.getHumanJamsTime() + '');

                    myMap.geoObjects.add(route);
                });

        }
    </script>
    <meta name="google-site-verification" content="dSgrFnAR9I32TSCufef8kqxg-rFbffUbgybDkz-7wHc"/>
    <style type="text/css" id="pagelines-custom">
        body 

        h4 {
            font-size: 21px;
        }

        body, .font1, .font-primary, .commentlist, p, li, h1, h2, h3, h4, h5, h6, .ol .site-title {
            font-family: 'PT Serif', georgia, times, sans-serif;
        }

        #feature_slider .fcontent {
            width: 40%;
        }

        .home .copy-pad .widget-title {
            font-size: 1.2em;
        }

        .tw-recent-posts .featured-post .excerpt {
            font-size: 1.1em;
        }

        .fcontent .ftitle {
            margin-top: -25px;
            font-size: 1.4em;
        }

        .fset_height, #feature_slider .text-bottom .fmedia .dcol-pad, #feature_slider .text-bottom .feature-pad, #feature_slider .text-none .fmedia .dcol-pad {
            height: 290px;
        }

        .hentry table td, #comments table td {
            font-size: 1.2em;
        }

        .wp-post-image {
            border: 1px solid #999;
        }

        .post-comments a {
            font-weight: bold;
            color: #000;
        }

        body.fixed_width #page, body.fixed_width #footer, body.canvas .page-canvas {
            width: 100%;
            max-width: 1400px;
        }

        #page {
            padding-top: 0px !important;
            box-shadow: 0 -2px 10px -3px black;
        }

        #banners .content-pad {
            padding: 0px;
        }

        #banners .banner-text-pad {
            padding: 0 !important;
        }

        #banners .banner-title {
            margin-bottom: 0;
        }

        #banners .banner-area {
            margin: 0;
        }

        #brandnav .content-pad {
            background: #f2f2f2;
            padding-bottom: 1em;
            padding-top: 1em;
        }

        #brandnav .brandnav-nav {
            bottom: 0;
            display: inline-block;
            vertical-align: bottom;
            float: none;
            margin-left: 0;
        }

        .navbar {
            float: left;
            background: url('/pix/logo_header.jpg');
            margin-top: 30px;
            background-repeat: no-repeat;
            border: 0;
            box-shadow: inset 0 0 0;
            min-height: 55px;
            padding-left: 250px;
            padding-top: 75px;
        }

        #brandnav .mainlogo-link, #brandnav .title-container {
            display: inline-block;
            line-height: 0.7em;
            padding: 0 1%;
            float: none;
        }

        #nav .content-pad {
            padding: 0;
            font-size: 0.9em;
            background: rgba(0, 0, 0, 0.7);
        }

        #nav .content-pad .navigation_wrap {
            border: 0;
        }

        #nav .sf-menu li {
            background: transparent;
            color: white;
        }

        #nav .sf-menu li a {
            color: white;
        }

        #nav .main-nav li a:hover {
            background: rgba(0, 0, 0, 0.3);
        }

        .main-nav li.current_page_item a, .main-nav li.current-menu-item a {
            background: rgba(116, 12, 0, 0.8);
        }

        .one-sidebar-right #column-main .mcolumn-pad, .two-sidebar-right #column-main .mcolumn-pad {
            padding: 0px 30px 0 15px;
            top: -100px;
        }

        p, .p, .hentry ul, .hentry ol {
            margin: 0.6em 0;
        }

        #brandnav ul > li > a, .navbar .navline > li > a {
            float: none;
            display: block;
            padding: 6px 15px;
            line-height: 19px;
            color: rgba(0, 0, 0, 0.8);
            text-shadow: none;
            font-size: 1.2em;
            background: none;
        }

        #brandnav ul > li > a:hover, .navbar .navline > li > a:hover {
            color: #740c00;
            transition: all 0.2s ease-in 0s;
        }

        #brandnav ul > li {
            border-bottom: 2px solid transparent;
        }

        #brandnav ul > li.menu-item-2416 {
            background: white;
            border-radius: 6px;
            margin: 0 0 0 15px;
        }

        #brandnav ul > li:hover, .navbar .navline > li:hover {
            border-bottom-color: rgba(0, 0, 0, 0.3);
            transition: all 0.2s ease-in 0s;
        }

        #brandnav .brandnav-nav > ul > li.current-menu-item {
            border-bottom-color: #a60a0a;
        }

        .main_nav ul li {
            background: #f2f2f2;
        }

        a > span.sf-sub-indicator {
            top: 9px;
            right: 0px;
        }

        .navbar-content-pad {
            font-size: 1.2em;
            font-family: verdana, tahoma, sans-serif;
            font-weight: 500;
            color: rgba(0, 0, 0, 0.8);
            text-decoration: underline;
        }

        .dropdown.open .dropdown-toggle, .dropdown.open .dropdown-toggle:hover {
            color: black;
        }

        .navbar .caret {
            border-top: 4px solid #000;
        }

        .one-sidebar-right #pagelines_content #column-wrap {
            width: 80%;
        }

        .one-sidebar-right #pagelines_content #sidebar-wrap {
            width: 20%;
            float: right;
        }

        p, .p, .hentry ul, .hentry ol {
            font-size: 1.2em;
            line-height: 1.39em;
        }

        #sb_universal .tw-recent-posts {
            width: 50%;
            display: inline-block;
            vertical-align: top;
        }

        .tw-recent-posts .post-time {
            text-align: left;
            color: rgba(0, 0, 0, 0.5);
            font-size: 1em;
        }

        .tw-recent-posts .featured-post .excerpt {
            line-height: 1.5em;
        }

        .two-sidebar-right #pagelines_content #column-wrap {
            width: 65%;
        }

        .two-sidebar-right #pagelines_content #sidebar-wrap {
            width: 35%;
            max-width: 480px;
        }

        .current_posts_info {
            display: none;
        }

        #highlight .hl-image-bottom {
            margin: -36px -15px -30px -15px;
        }

        #boxes .content-pad {
            padding-bottom: 0px;
            font-size: 1.1em;
        }

        a, #subnav_row li.current_page_item a, #subnav_row li a:hover, .branding h1 a:hover {
            color: #740c00;
            font-size: 1em;
        }

        .widget ul {
            margin: 0 0 10px;
        }

        .widget ul li {
            padding: 0 0 10px;
        }

        .main_nav, h6.site-description, .widget-title, #footer li h5 a, .subhead, .main_nav, #postauthor .subtext, .author-details, .post-nav, .current_posts_info, #secondnav {
            font-size: 100%;
        }

        .footcol-pad .menu li {
            display: inline-block;
            height: 24px;
            width: 100px;
        }

        .thead, h1, h2, h3, h4, h5, h6, .site-title {
            font-family: "Lucida Grande", Tahoma, sans-serif;
        }

        #respond form {
            margin-left: 0;
        }

        #respond h3 {
            padding-bottom: 15px;
        }

        #morefoot_area {
            background: #f2f2f2;
            border-top: 1px dashed rgba(0, 0, 0, 0.2);
        }

        #morefoot_area .content {
            background: transparent;
        }

        #post-1554 .span4 b {
            font-weight: bold;
            font-size: 1.1em;
        }

        #post-685 .span4 a {
            font-weight: bold;
            font-size: 1.1em;
        }
    </style>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter20649340 = new Ya.Metrika({id: 20649340,
                        webvisor: true,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true});
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="//mc.yandex.ru/watch/20649340" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1270126-21']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </script>

    <script>
        var site_url = '<?php echo site_url(); ?>';
    </script>
</head>

<body <?php body_class('custom responsive pagelines_v2.2.55 category fixed_width   ext-gecko ext-gecko3 customize-support'); ?>>

<?php get_filename(); ?>

<div id="site" class="one-sidebar-right">
    <div id="page" class="thepage">
        <div class="page-canvas">

            <header id="header" class="container-group">
                <div class="outline">

                    <!-- Banners | Section Template -->
                    <section id="banners" class="container no_clone section-banners fix">
                        <div class="texture">
                            <div class="content">
                                <div class="content-pad">
                                    <div class="banner_container fix banners default-banners">
                                        <div class="banner-area pprand-pad banner_right no-pad">
                                            <div class="banner-text pprand" style="width:100%;">
                                                <div class="banner-text-pad pprand-pad" style="padding: 20px 40px">
                                                    <div class="banner-text-wrap">
                                                        <div class="banner-title">
                                                            <h2></h2>
                                                        </div>

                                                        <div class="banner-content">
                                                            <div
                                                                style="height:100px; vertical-align: center; text-align: center; background: #96b6de url('http://chgtown.ru/pix/3d.jpg') no-repeat left top; border-top: 1px solid #c1dbfa; border-bottom: 1px solid #799bc4; position: relative;">
                                                                <a href="http://paola-apmed.ru/" target="blank"
                                                                   style="display: block; width: 100%; height: 100px; text-decoration: none">
                                                                    <img src="http://paola-apmed.ru/i/logo.png"
                                                                         style="margin: 20px 0px 0px 0px;"
                                                                         id="vt-center"/ >

                                                                    <div id="vt-hide"
                                                                         style="text-align: left; width:24em; position: absolute; right: -1%;  top: 10%;">
                                                                        <h2 style="color:#0c3360; margin: 1em;  font-size:1.1em">
                                                                            3D Мезонити &mdash; инновационная методика
                                                                            безоперационной подтяжки</h2>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <style>
                                                                #vt-center {
                                                                    padding-right: 0%;
                                                                }

                                                                #vt-hide {
                                                                    display: block
                                                                }

                                                                @media all and (max-width: 960px) {
                                                                    #vt-hide {
                                                                        display: none
                                                                    }

                                                                    #vt-center {
                                                                        padding-right: 0;
                                                                    }
                                                                }

                                                                @media all and (min-width: 1200px) {
                                                                    #vt-hide {
                                                                        display: block;
                                                                        margin-right: 5em;
                                                                    }

                                                                    #vt-center {
                                                                        padding-right: 0;
                                                                    }
                                                                }
                                                            </style>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="banner-media pprand" style="width:0%;">
                                                <div class="banner-media-pad pprand-pad"></div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- BrandNav | Section Template -->
                    <section id="brandnav" class="container no_clone section-brandnav fix">
                        <div class="texture">
                            <div class="content">
                                <div class="content-pad">
                                    <a class="plbrand mainlogo-link" href="<?php echo SITE_URL; ?>/"
                                       title="ChgTown.Ru — Город Черноголовка"><img class="mainlogo-img"
                                                                                    src="http://chgtown.ru/wp-content/uploads/2013/03/chgtown.png"
                                                                                    alt="ChgTown.Ru — Город Черноголовка"/></a>

                                    <div class="brandnav-nav main_nav fix">
                                        <?php wp_nav_menu(array(
                                            'container' => 'false',
                                            'items_wrap' => '<ul class="main-nav tabbed-list sf-menu">%3$s</ul>',
                                            'theme_location' => 'menu-1'
                                        )); ?>
                                    </div>
<div style="float:right; margin: 0px 20px 0px -5px; align:center; "><center><a href="/kormushki"><img src="/pix/korm.png"><br><b>Смастери кормушку!</b></a></center></div>

<!-- <div style="float:right; margin: 48px 40px 0px 0px; "><a href="/donate">Поддержать
                                            ЧгТаун</a></div>  -->


                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </section>


<!--   <?php if (!is_page('kefir')) { ?>
<center><a href="http://chgtown.ru/kefir/" style="font-color:white;">
<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0" bgcolor="66ccff">
<tr style="background-color: #8e0883;">
<td align="center"><p style="font-size:1.2em; color:white;">
<b>Сегодня в 20:00 в Прямом кефире общественный деятель Надежда Межова-Деглина</b></p>
</td>
</tr>
</table>
</a></center>
                    <?php } ?>

-->


<!--     <? if (!is_single('deti')) { ?>
                        <a href="http://chgtown.ru/afisha/deti/" style="font-color:white;">
                            <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0" bgcolor="66ccff">
                                <tr style="background-color: #ff8346;">
                                    <td align="center"><p style="font-size:1.2em; color:white;">
                                            <b>Благотворительная акция &laquo;Чтобы дети смеялись&raquo;. 8 декабря, Дом учёных</b></p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    <?php } ?>

-->

<!--     <? if (!is_single('budget')) { ?>
                        <a href="http://chgtown.ru/news/budget/" style="font-color:white;">
                            <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0" bgcolor="66ccff">
                                <tr style="background-color: #1e4102;">
                                    <td align="center"><p style="font-size:1.2em; color:white;">
                                            <b>2 декабря состоятся публичные слушания по проекту городского бюджета на 2014 год</b></p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    <?php } ?>
-->

                    <?php
                    $post_type = get_post_type();
                    if ($post_type != 'advert' && !is_page('adverts')&& !is_page('add-advert')&& !is_tax('adverts')&&!is_page('kefir')) {
                        get_template_part('boxes');
                    }
                    ?>

                    <?php
//                    if (!is_page('kefir')) {
//                        get_template_part('boxes');
//                    }
                        ?>






                </div>



            </header>

<?php get_filename(); ?>