<?php
get_header();
get_filename();
?>
	<div id="page-main" class="container-group">
		<div id="dynamic-content" class="outline">

			<section id="content" class="container no_clone section-content-area fix">
				<div class="texture">
					<div class="content">
						<div class="content-pad">
							<div id="pagelines_content" class="one-sidebar-right fix">
								<div id="column-wrap" class="fix">
									<div id="column-main" class="mcolumn fix">
										<div class="mcolumn-pad" >

											<!--<section id="postsinfo" class="copy no_clone section-postsinfo">
												<div class="copy-pad">
													<div class="current_posts_info">Вы просматриваете категорию: <strong>"<span data-term_id="9" data-taxonomy="category" data-type="input" data-filter="single_cat_title" class="fee-field">Новости</span>"</strong></div>
													<div class="clear"></div>
												</div>
											</section>-->

											<section id="postloop" class="copy no_clone section-postloop">
												<div class="copy-pad">

													<?php while ( have_posts() ) : the_post(); ?>

														<article <?php post_class( 'fpost'); ?>>
															<div class="hentry-pad">
																<section class="post-meta fix post-nothumb ">
																	<section class="bd post-header fix" >
																		<section class="bd post-title-section fix">

																			<hgroup class="post-title fix">
																				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
																			</hgroup>

																			<div class="metabar">
																				<div class="metabar-pad">
																					<em>
																						<span class="author vcard sc"><span class="fn"><?php the_author_posts_link(); ?></span></span> &middot;
																						<time class="date time published updated sc" datetime="<?php echo get_the_time( 'c' ); ?>"><?php echo get_the_time( get_option( 'date_format' ) ); ?></time>  &middot;
																						<span class="post-comments sc"><a href="<?php echo get_comments_link(); ?>"><?php comments_number( 'Комментарии: 0', 'Комментарии: 1', 'Комментарии: %' ); ?></a></span> &middot;
																						<span class="categories sc"><a href="" rel="category tag"><?php post_type_archive_title(); ?></a></span>   &middot;
																						<span class="tags sc">Содержит метки: <?php echo get_the_term_list( $post->ID, 'tag', $before = '', ', ', $after = '' ); ?></span>
                                                                                        <?php if(current_user_can('administrator')){?>
                                                                                            [<a class="post-edit-link" href="<?php echo get_edit_post_link( $post->ID ); ?>" title="Редактировать запись"><span class='editpage sc'>Редактировать</span></a>]
																					    <?php }?>
                                                                                    </em>
																				</div>
																			</div>

																		</section>
																	</section>
																</section>

																<div class="entry_wrap fix">
																	<div class="entry_content">
																		<?php the_content( 'Весь текст &rarr;' ); ?>
                                                                        <?php if(current_user_can('administrator')){?>
                                                                        <a class="pledit" href="<?php echo get_edit_post_link( $post->ID ); ?>"><span class="pledit-pad">(<em>edit</em>)</span></a>
																	    <?php }?>
                                                                    </div>
																</div>

															</div>
														</article>

													<?php
													endwhile;
													wp_reset_postdata();
													?>

													<div class="clear"></div>
												</div>
											</section>

											<section id="pagination" class="copy no_clone section-pagination"><div class="copy-pad"><div class='wp-pagenavi'>
														<span class='pages'>Страница 1 из 8</span><span class='current'>1</span><a href='http://chgtown.ru/category/news/page/2/' class='page larger'>2</a><a href='http://chgtown.ru/category/news/page/3/' class='page larger'>3</a><a href='http://chgtown.ru/category/news/page/4/' class='page larger'>4</a><a href='http://chgtown.ru/category/news/page/5/' class='page larger'>5</a><span class='extend'>...</span><a href='http://chgtown.ru/category/news/page/2/' class='nextpostslink'>→</a><a href='http://chgtown.ru/category/news/page/8/' class='last'>Последняя →</a>
													</div><div class="clear"></div></div></section>					</div>
									</div>

								</div>

								<div id="sidebar-wrap" class="">
									<div id="sidebar1" class="scolumn" >
										<div class="scolumn-pad">

										</div>
									</div>


									<!-- Primary Sidebar | Section Template -->
									<section id="sb_primary" class="copy no_clone section-sb_primary"><div class="copy-pad"><ul id="list_sb_primary" class="sidebar_widgets fix"><li id="text-5" class="widget_text widget fix"><div class="widget-pad"><h3 class="widget-title"><span data-type="input" data-filter="widget_title" class="fee-field"></span></h3>			<div class="textwidget"><div data-type="rich" data-filter="widget_text" class="fee-field fee-clearfix"><center><b>6-7 июля 2013 года</b><br><a href="http://chgtown.ru/2013/06/nardy"><img src="/pix/nardy.jpg"><br>В Черноголовке пройдёт I-й чемпионат Московской области по спортивным нардам</a></b><br><br></b><a href="http://chgtown.ru/category/events/" target="_self" class="btn btn-success btn-small"> Вся афиша
																	</a></center></div></div>
													</div></li><li id="text-2" class="widget_text widget fix"><div class="widget-pad"><h3 class="widget-title"><span data-type="input" data-filter="widget_title" class="fee-field"></span></h3>			<div class="textwidget"><div data-type="rich" data-filter="widget_text" class="fee-field fee-clearfix"><center><h5>Реклама</h5><br>
																	<div>
																		<script type="text/javascript">
																			document.write('<a href="'+ad2[ri2].url+'"> '+ad2[ri2].alt+'<img style="border:0" src="'+ad2[ri2].image+'" alt="'+ad2[ri2].alt+'" width="'+ad2[ri2].w+'" height="'+ad2[ri2].h+'"><br/>'+ad2[ri2].alt1+'</a>');
																		</script>
																	</div><br><br>

																	<div>
																		<script type="text/javascript">
																			document.write('<a href="'+ad3[ri3].url+'"> '+ad3[ri3].alt+'<img style="border:0" src="'+ad3[ri3].image+'" alt="'+ad3[ri3].alt+'" width="'+ad3[ri3].w+'" height="'+ad3[ri3].h+'"><br/>'+ad3[ri3].alt1+'</a>');
																		</script>
																	</div><br><br>


																	<div>
																		<script type="text/javascript">
																			document.write('<a href="'+ad[ri].url+'"> '+ad[ri].alt+'<img style="border:0" src="'+ad[ri].image+'" alt="'+ad[ri].alt+'" width="'+ad[ri].w+'" height="'+ad[ri].h+'"><br/>'+ad[ri].alt1+'</a>');
																		</script>
																	</div><br><br>
																	<a href="/kpm/"><img src="/pix/kpm.jpg" ></a>
																	<br><br></center></div></div>
													</div></li>		<li id="recent-comments-2" class="widget_recent_comments widget fix"><div class="widget-pad"><div data-widget_id="recent-comments-2" data-sidebar_id="sidebar-1" data-type="widget" data-filter="dynamic_sidebar_params" class="fee-field fee-clearfix">			<h3 class="widget-title">Свежие комментарии</h3>			<ul id="recentcomments"><li class="recentcomments"><span class="recentcommentsauthor">кирилл</span>: <a href="http://chgtown.ru/bench/#comment-15694">я то уэшников пнул, только одно заявление- капля в море&#8230; насчет возможнос...</a></li><li class="recentcomments"><span class="recentcommentsauthor">Илья Мурзин</span>: <a href="http://chgtown.ru/2013/06/pushkina/#comment-15671">Если же вы про деньги в глобальном смысле &#8212; мол будет кризис и мы все умр...</a></li><li class="recentcomments"><span class="recentcommentsauthor">Илья Мурзин</span>: <a href="http://chgtown.ru/2013/06/pushkina/#comment-15670">Чатланин, если вы про деньги в сугубо утилитарном смысл, то мне не кажется это...</a></li><li class="recentcomments"><span class="recentcommentsauthor">Чатланин</span>: <a href="http://chgtown.ru/2013/06/pushkina/#comment-15665">Илья, идея то хорошая, но пожалуйста ответьте на один вопрос, заданный ещё Влад...</a></li><li class="recentcomments"><span class="recentcommentsauthor">Илья Мурзин</span>: <a href="http://chgtown.ru/2013/06/pushkina/#comment-15642">Мне идея Университета, студенческого городка и прочего очень нравится. Но я бы...</a></li><li class="recentcomments"><span class="recentcommentsauthor">Чатланин</span>: <a href="http://chgtown.ru/2013/06/doghunters/#comment-15633">Это сказал скрипач, который думает на языках, продолжения которых он и сам не з...</a></li><li class="recentcomments"><span class="recentcommentsauthor">Чатланин</span>: <a href="http://chgtown.ru/2013/06/doghunters/#comment-15632">Браво, Яга!!! Браво! Бурные и продолжительные аплодисменты! Это было феерично!...</a></li><li class="recentcomments"><span class="recentcommentsauthor">Баба Яга</span>: <a href="http://chgtown.ru/2013/06/doghunters/#comment-15625">Кстати, да, Ольга. Иногда так мучают умирающих животных своими лечениями и опер...</a></li><li class="recentcomments"><span class="recentcommentsauthor">Тор</span>: <a href="http://chgtown.ru/2013/06/doghunters/#comment-15621">Да торолль- он и есть тролль. Чё его кормить,то? Сам ничё толком сказать не мож...</a></li><li class="recentcomments"><span class="recentcommentsauthor">Чатланин</span>: <a href="http://chgtown.ru/2013/06/doghunters/#comment-15617">Потрудитесь сами ответить на ранее заданные Вам вопросы. Их там целая куча и ма...</a></li></ul>
														</div></div></li><li id="text-10" class="widget_text widget fix"><div class="widget-pad"><h3 class="widget-title"><span data-type="input" data-filter="widget_title" class="fee-field"></span></h3>			<div class="textwidget"><div data-type="rich" data-filter="widget_text" class="fee-field fee-clearfix"><center><h2>Погода</h2><br><link rel="stylesheet" type="text/css" href="http://www.gismeteo.ru/static/css/informer2/gs_informerClient.min.css">
																	<div id="gsInformerID-UEKt2u085226aI" class="gsInformer" style="width:240px;height:192px">
																		<div class="gsIContent">
																			<div id="cityLink">
																				<a href="http://www.gismeteo.ru/city/daily/4368/" target="_blank">Погода в Москве</a>
																			</div>
																			<div class="gsLinks">
																				<table>
																					<tr>
																						<td>
																							<div class="leftCol">
																								<a href="http://www.gismeteo.ru" target="_blank">
																									<img alt="Gismeteo" title="Gismeteo" src="http://www.gismeteo.ru/static/images/informer2/logo-mini2.png" align="absmiddle" border="0" />
																									<span>Gismeteo</span>
																								</a>
																							</div>
																							<div class="rightCol">
																								<a href="http://www.gismeteo.ru/city/weekly/4368/" target="_blank">Прогноз на 2 недели</a>
																							</div>
																						</td>
																					</tr>
																				</table>
																			</div>
																		</div>
																	</div>
																	<script src="http://www.gismeteo.ru/ajax/getInformer/?hash=UEKt2u085226aI" type="text/javascript"></script><br><span style="margin-top: -10px; "><img src="/pix/zaplatka.jpg"></span></div></div>
													</div></li></ul><div class="clear"></div></div></section>		</div>
							</div>
						</div></div></div></section>				</div>
		<div id="morefoot_area" class="container-group">


			<!-- Footer Columns Sidebar | Section Template -->
			<section id="sb_footcols" class="container no_clone section-sb_footcols fix"><div class="texture"><div class="content"><div class="content-pad"><div class="fcolumns ppfull pprow"><div class="fcolumns-pad fix"><div class="pp5 footcol"><div class="footcol-pad"><div data-widget_id="search-3" data-sidebar_id="sidebar-10" data-type="widget" data-filter="dynamic_sidebar_params" class="fee-field fee-clearfix"><form method="get" class="searchform" onsubmit="this.submit();return false;" action="http://chgtown.ru/" ><fieldset><input type="text" value="" name="s" class="searchfield" placeholder="Поиск" /></fieldset></form></div></div></div><div class="pp5 footcol"><div class="footcol-pad"><h3 class="widget-title"><span data-type="input" data-filter="widget_title" class="fee-field"></span></h3>			<div class="textwidget"><div data-type="rich" data-filter="widget_text" class="fee-field fee-clearfix" title="Содержимое текстового виджета">[пусто]</div></div>
										</div></div><div class="pp5 footcol"><div class="footcol-pad"><h3 class="widget-title"><span data-type="input" data-filter="widget_title" class="fee-field"></span></h3>			<div class="textwidget"><div data-type="rich" data-filter="widget_text" class="fee-field fee-clearfix"><a href="http://vk.com/chgtown"><img src="http://chgtown.ru/pix/vk.png"></a>&nbsp;&nbsp;
													<a href="http://www.odnoklassniki.ru/group/50599211565272"><img src="http://chgtown.ru/pix/odnoklassniki.png"></a>&nbsp;&nbsp;
													<a href="http://www.facebook.com/groups/chgtown/"><img src="http://chgtown.ru/pix/fb.png"></a>&nbsp;&nbsp;<a href="http://ru_chg.livejournal.com"><img src="http://chgtown.ru/pix/lj.png"></a>&nbsp;&nbsp;
													<a href="http://www.youtube.com/chgtownru"><img src="http://chgtown.ru/pix/youtube.png"></a>&nbsp;&nbsp;
													<a href="http://twitter.com/ChgTownRu"><img src="http://chgtown.ru/pix/tw.png"></a>&nbsp;&nbsp;
													<a href="http://instagram.com/chgtown"><img src="http://chgtown.ru/pix/instagram.png"></a></div></div>
										</div></div><div class="pp5 footcol"><div class="footcol-pad"><h3 class="widget-title"><span data-type="input" data-filter="widget_title" class="fee-field"></span></h3>			<div class="textwidget"><div data-type="rich" data-filter="widget_text" class="fee-field fee-clearfix" title="Содержимое текстового виджета">[пусто]</div></div>
										</div></div><div class="pp5 footcol"><div class="footcol-pad"><h3 class="widget-title"><span data-type="input" data-filter="widget_title" class="fee-field"></span></h3>			<div class="textwidget"><div data-type="rich" data-filter="widget_text" class="fee-field fee-clearfix"><a href="/" style="float: left;"><img src="http://chgtown.ru/pix/logo/bird.png" style="float:left; margin-right: 10px; margin-bottom: 15px;"></a>© 2013, <a href="/about/">редакция ChgTown.Ru</a><br/>+7 926 33 88 555</div></div>
										</div></div></div></div><div class="clear"></div></div></div></div></section>				</div>
		<div class="clear"></div>
	</div>
	</div>
	</div>

<?php
get_filename();
get_footer();
?>