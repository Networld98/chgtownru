<?php get_filename(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<div <?php post_class(); ?>>
		<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<div class="post-summary"><?php the_excerpt(); ?></div>
	</div>
<?php endwhile; ?>

<?php get_filename(); ?>