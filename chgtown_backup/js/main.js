jQuery(document).ready(function ($) {
	if (typeof($.fn.placeholder) == 'function') {
		$('input, textarea').placeholder();
	}

	if ('noValidate' in document.createElement('form')) {
		$('form').attr("novalidate", "novalidate");
	}

	/* AUTH */
	var $auth_popup = $('#auth-popup'),
	    $msg = ($('.entry_content').find('.auth-message').length > 0 ? $('.entry_content').find('.auth-message') : $auth_popup.find('.auth-message')),
	    authRemoveMsg = function () {
		    $msg.html('').hide(0);
	    },
	    authSubmitHandler = function (form) {
		    authRemoveMsg();

		    $.post(
			    '/wp-admin/admin-ajax.php',
			    {
				    action: 'authreg',
				    data  : $(form).serializeArray()
			    },
			    function (response) {
				    if (!response.success) {
					    $msg.addClass('error').removeClass('success').html(response.data.msg).fadeIn();
				    }
				    else {
					    if (response.data.redirect) {
						    if (response.data.redirect == 'reload')
							    document.location.reload();
						    else
							    document.location = response.data.redirect;
					    }
					    else if (response.data.msg) {
						    $auth_popup.find('.iblock:visible').slideUp();
						    $('#resetpassform').slideUp();

						    $msg.addClass('success').removeClass('error').html(response.data.msg).fadeIn();
					    }
				    }
			    },
			    'json'
		    );

		    return false;
	    };

	$auth_popup.on('click', '.close', function () {
		authRemoveMsg();

		$auth_popup.find('.iblock:visible').slideUp();
		$auth_popup.find('label.error').remove();
	});

	$('.open-auth-popup').on('click', function (e) {
		var $this = $(this);

		e.preventDefault();

		var $target = $($this.attr('href'));

		if ($target.is(':visible')) {
			return;
		}

		var $other_block = $auth_popup.find('.iblock:visible');

		authRemoveMsg();

		if ($other_block.length > 0) {
			$other_block.slideUp(function () {
				$auth_popup.find('label.error').remove();
				$target.slideDown();
			});
		}
		else {
			$target.slideDown();
		}
	});

	$("#authform").find('form').validate({
		submitHandler: authSubmitHandler,
		rules        : {
			login: {
				required: true
			},
			pass : {
				required: true
			}
		},
		messages     : {
			login: {
				required: "Введите E-mail"
			},
			pass : {
				required: "Введите Пароль"
			}
		}
	});

	$("#regform").find('form').validate({
		submitHandler: authSubmitHandler,
		rules        : {
			name : "required",
			email: {
				required: true,
				email   : true
			},
			pass : {
				required: true
			}
		},
		messages     : {
			name : {
				required: "Введите Имя"
			},
			email: {
				required: "Введите E-mail",
				email   : "Некорректный E-mail"
			},
			pass : {
				required: "Введите Пароль"
			}
		}
	});

	$("#lostpasswordform").find('form').validate({
		submitHandler: authSubmitHandler,
		rules        : {
			login: {
				required: true
			}
		},
		messages     : {
			login: {
				required: "Введите E-mail"
			}
		}
	});

	$("#resetpassform").validate({
		submitHandler: authSubmitHandler,
		rules        : {
			pass: {
				required: true
			}
		},
		messages     : {
			pass: {
				required: "Введите Новый пароль"
			}
		}
	});
	/* END AUTH */

	$('#allcatalog').click(function () {
		$('.w-catlog_all').toggleClass('visible');
		return false;
	});
	$('.fancybox').fancybox({
		'padding'   : 0,
		// 'modal': true,
		'nextEffect': 'fade',
		'prevEffect': 'fade'
		// 'nextEffect': 'none',
		// 'prevEffect': 'none'
	});

	$('#cycle.fclone').cycle({
		slideResize  : 0,
		fit          : 1,
		fx           : 'fade',
		sync         : 1,
		timeout      : 10000,
		speed        : 1000,
		cleartype    : true,
		cleartypeNoBg: true,
		pager        : '#featurenav.fclone'
	});
	/*jQuery( "div#featurenav.fclone" ).children( "a" ).each( function () {
	 if ( jQuery( this ).html() == "1" ) {
	 jQuery( this ).attr( "title", "АБТ 2013" );
	 jQuery( this ).html( "АБТ 2013" );
	 }
	 if ( jQuery( this ).html() == "2" ) {
	 jQuery( this ).attr( "title", "+7 926 33 88 555" );
	 jQuery( this ).html( "+7 926 33 88 555" );
	 }
	 } );*/

	var cSel = '#cycle.fclone';
	var ppSel = '.playpause.fclone';

	jQuery(ppSel).click(function () {
		if (jQuery(ppSel).hasClass('pause')) {
			jQuery(cSel).cycle('pause');
			jQuery(ppSel).removeClass('pause').addClass('resume');
		} else {
			jQuery(ppSel).removeClass('resume').addClass('pause');
			jQuery(cSel).cycle('resume', true);
		}
	});
	jQuery('input:checkbox[name^="tax_input"]').click(function () {
		console.log(jQuery(this).parents('ul').parent());
	})

});


