 $( '#slider' ).bjqs({
	width: 700,
	height: 300,
	animation: 'fade',
	animationDuration: 450,
	automatic: true,
	rotationSpeed: 4000,
	hoverPause: true,
	showControls: true,
	centerControls: true,
	nextText: '',
	prevText: '',
	showMarkers: false,
	centerMarkers: true,
	keyboardNav: true,
	useCaptions: false
});