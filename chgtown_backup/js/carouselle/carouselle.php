<?php

add_action( 'init', 'load_carouselle' );
function load_carouselle() {
	if ( !is_admin() ) {
		wp_register_script( 'carouselle', THEME_URL . '/js/carouselle/jcarousellite_1.0.1.pack.js', array( 'jquery' ), '1', true );
		wp_enqueue_script( 'carouselle' );
	}
}

?>