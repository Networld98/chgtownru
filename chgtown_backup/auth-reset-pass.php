<?php
get_header();
get_filename();
?>
	<div id="page-main" class="container-group">
		<div id="dynamic-content" class="outline">

			<section id="content" class="container no_clone section-content-area fix">
				<div class="texture">
					<div class="content">
						<div class="content-pad">
							<div id="pagelines_content" class="one-sidebar-right fix">
								<div id="column-wrap" class="fix">
									<div id="column-main" class="mcolumn fix">
										<div class="mcolumn-pad" >
											<section id="postloop" class="copy no_clone section-postloop">
												<div class="copy-pad">
													<article <?php post_class( 'fpost' ); ?>>
														<div class="hentry-pad">
															<section class="post-meta fix post-nothumb ">
																<section class="bd post-header fix" >
																	<section class="bd post-title-section fix">
																		<hgroup class="post-title fix">
																			<h2 class="entry-title">Восстановление пароля</h2>
																		</hgroup>
																	</section>
																</section>
															</section>

															<div class="entry_wrap fix">
																<div class="entry_content">
																	<div class="auth-message error"></div>

														            <?php
																	global $auth_message;

																	if ( ! empty( $auth_message ) ) :
																		echo "<div class=\"auth-message ". (isset($_GET['lost_password']) ? 'success' : 'error') ."\">{$auth_message}</div>";
																	else :
																	?>
	                                                                    <form id="resetpassform">
																			<input type="hidden" name="action" value="reset_pass">
																			<input type="hidden" name="login" value="<?php echo get_query_var( 'auth_login' ); ?>">
																			<input type="hidden" name="key" value="<?php echo get_query_var( 'auth_key' ); ?>">

																			<div class="field">
																				<label>Новый пароль:</label>
																				<input type="password" name="pass" placeholder="пароль">
																			</div>

																			<div class="field buttons">
																				<input type="submit" name="submit" value="Изменить пароль">
																			</div>
																		</form>
																	<?php endif; ?>
																</div>
															</div>

														</div>
													</article>

													<div class="clear"></div>
												</div>
											</section>
										</div>
									</div>
								</div>

								<div id="sidebar-wrap" class="">
									<div id="sidebar1" class="scolumn" >
										<div class="scolumn-pad"></div>
									</div>

									<section id="sb_primary" class="copy no_clone section-sb_primary">
										<div class="copy-pad">
											<ul id="list_sb_primary" class="sidebar_widgets fix"><?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?><?php endif; ?></ul>
											<div class="clear"></div>
										</div>
									</section>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

		<div id="morefoot_area" class="container-group">
			<section id="sb_footcols" class="container no_clone section-sb_footcols fix">
				<div class="texture">
					<div class="content">
						<div class="content-pad">
							<div class="fcolumns ppfull pprow">
								<div class="fcolumns-pad fix"><?php if ( ! dynamic_sidebar( 'footer' ) ) : ?><?php endif; ?></div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="clear"></div>

	</div>

	</div>
	</div>

<?php
get_filename();
get_footer();
?>