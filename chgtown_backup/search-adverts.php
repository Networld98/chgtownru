<div class="w-search-block">
    <form action="<?php echo site_url('adverts'); ?>" method="get" class="filter-form" id="search_form">
        <div class="w-sea-category">
            <input class="w-inp_sel" id="w-inp_sel" type="text"  onblur="if (this.value==''){this.placeholder='Поиск'}" onfocus="if (this.placeholder=='Поиск') this.placeholder='';" value="<?php  echo isset($_REQUEST['title'])?$_REQUEST['title']:''?>" placeholder="Поиск"  name="title">
            <?php
            $advert_cats_array = array();
            $sel = '';
            $terms_array = get_terms('adverts', array('hide_empty' => 0,'orderby' => 'term_order', 'order' => 'ASC', 'parent' => 0));
            $str = '<select class="w-inp_sel" id="acf-field-adverts_cat" name="adverts_cat"><option value="">Искать во всех категориях</option>';
            foreach ($terms_array as $value) {
                if (!empty($_REQUEST['adverts_cat']) && $_REQUEST['adverts_cat'] == $value->slug) {
                    $sel = 'selected';
                } else {
                    $sel = '';
                }
                $advert_cats_array[$value->slug] = $value->name;
                $child_array = getChildTerms($value->term_id);
                $str .= "<option class=\"w-even\" value=\"$value->slug\" $sel data-catid=\"$value->term_id\">$value->name</option>";
            }
            $str .= '</select>';
            echo $str;
            ?>
            <input type="submit" value="Найти"  name="advert-search-btn"/>
        </div>

        <div class="w-check">
            <label><input type="checkbox" value="1" name="in_title" <?php if(isset($_REQUEST['in_title'])&&$_REQUEST['in_title']==1) echo 'checked="checked"'?> /> <span>искать только в названиях</span></label>
            <label><input type="checkbox" value="1" name="has_foto" <?php if(isset($_REQUEST['has_foto'])&&$_REQUEST['has_foto']==1) echo 'checked="checked"'?> /> <span>с фото</span></label>
        </div>
        <fieldset>
            <?php
            if (isset($_REQUEST['advert-search-btn'])) {
                setSearchParam($_REQUEST);
            } else {?>
                <div class="divline2">
                    <div class="petrjus-select"></div>
                </div>
            <?php }; ?>
        </fieldset>
        <div class="w-add-note">
            <div class="w-adverts_link">
                <a href="/add-advert">Добавить объявление</a>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.autocomplete.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/js/jquery.autocomplete.css" media="screen" />


<script>
    jQuery(document).ready(function ($) {

        $("#w-inp_sel").autocomplete('/wp-admin/admin-ajax.php',
            {
                width:$("#w-inp_sel").outerWidth()-3,
                maxItemsToShow:10,
//                cellSeparator:'|',
                extraParams:{
                    action:'search_question'
                }
            }
        );
        $(document).on('change', '#acf-field-adverts_sub_cat', function (e) {
            _sel = $(this).parent().html();
            _term_id = $(this).find('option:selected').data('catid');
            $(this).parents('form').attr('action', site_url + '/adverts/' + $(this).val());
            $.post("/wp-admin/admin-ajax.php", {'action': 'get_advert_params', 'id_term': _term_id, 'page': 'filter'}, function (data) {
                $('.petrjus-select select').each(function(i,obj){
                    if(i!=0)
                   $(obj).remove();
                })
                $('.petrjus-select').append(data);
            });
        });
        $('#acf-field-adverts_cat').change(function (e) {
            _term_id = $(this).find('option:selected').data('catid');
            $(this).parents('form').attr('action', site_url + '/adverts/' + $(this).val());
            $.post("/wp-admin/admin-ajax.php", {'action': 'get_advert_subCat', 'id_term': _term_id}, function (data) {
                $('.petrjus-select').html(data);
            });
        });
    });
</script>