<?php

if(!empty($_POST)){
    save_advert();
}

get_header();
get_filename();
?>
<div id="page-main" class="container-group">
    <div id="dynamic-content" class="outline">

        <section id="content" class="container no_clone section-content-area fix">
            <div class="texture">
                <div class="content">
                    <div class="content-pad">
                        <div id="pagelines_content" class="one-sidebar-right fix">
                            <div id="column-wrap" class="fix">
                                <div id="column-main" class="mcolumn fix">
                                    <div class="mcolumn-pad">
                                        <section id="postloop" class="copy no_clone section-postloop">
                                            <div class="copy-pad">
                                                <article <?php post_class('fpost'); ?>>
                                                    <div class="w-cap">
                                                        <span class="w-zag"><?php echo the_title() ?></span>
                                                    </div>
                                                    <div class="hentry-pad">
                                                        <div class="entry_wrap fix">
                                                            <div class="entry_content">
                                                                <div class="w-form">
                                                                    <form action="" name="advert-add-formvalidate" id="add-form"  enctype="multipart/form-data" method="post" >
                                                                    <ul>
                                                                        <?php
                                                                            if (is_user_logged_in()){
                                                                                $user_adverts = get_current_user_id();
                                                                                $userdata = get_userdata($user_adverts);
                                                                                $user_first_name=get_user_meta($user_adverts,'first_name',true);
                                                                                $user_mail=$userdata->data->user_email;
                                                                                $user_phone=get_user_meta($user_adverts,'phone_number',true);
                                                                            }else{
                                                                                $user_adverts=false;
                                                                            }
                                                                        ?>
                                                                        <li>
                                                                            <ul>
                                                                                <li class="w-sm"><label>Ваше имя</label></li>
                                                                                <li class="w-lg"><input type="text" class="lg-input input_white_form_2" value="<?php echo  $user_adverts?$user_first_name:''; ?>" name="user_advert[name]" id="firstname"></li>
                                                                            </ul>
                                                                        </li>
                                                                        <li>
                                                                            <ul>
                                                                                <li class="w-sm"><label>Электронная почта</label></li>
                                                                                <li class="w-lg">
                                                                                    <input type="text" value="<?php echo  $user_adverts?$user_mail:''; ?>" class="lg-input input_white_form_2" name="user_advert[mail]" id="email">
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                        <li>
                                                                            <ul>
                                                                                <li class="w-sm"><label>Номер телефона</label></li>
                                                                                <li class="w-lg">
                                                                                    <input type="text" class="lg-input input_white_form_2" value="<?php echo  $user_adverts?$user_phone:''; ?>" name="user_advert[phone]">
                                                                                </li>
                                                                            </ul>
                                                                        </li>

                                                                        <li>
                                                                            <ul>
                                                                                <li class="w-sm"><label>Категория</label></li>
                                                                                <li class="w-lg">
                                                                                    <?php
                                                                                        $advert_cats_array = array();
                                                                                        $terms_array = get_terms('adverts', array('hide_empty' => 0, 'parent' => 0, 'orderby' => 'term_order'));
                                                                                        $str = '<select class="lg-input input_white_form_2" id="acf-field-adverts_cat" name="adverts_cat"><option value="">Выберите категорию</option>';
                                                                                        foreach ($terms_array as $value) {
                                                                                            $advert_cats_array[$value->slug] = $value->name;
                                                                                            $child_array = getChildTerms($value->term_id);
                                                                                            $str .= "<option value='$value->slug' data-catid='$value->term_id'>$value->name</option>";
                                                                                        }
                                                                                        $str .= '</select>';
                                                                                        echo $str;
                                                                                        ?>
                                                                                </li>
                                                                            </ul>
                                                                        </li>

                                                                        <li>
                                                                            <ul>
<!--                                                                                <li class="w-sm"><label></label></li>-->
                                                                                <li class="w-lg">
                                                                                    <fieldset id="getFields" class="gField">
                                                                                        <div class="divline">

                                                                                            </div>
                                                                                    </fieldset>
                                                                                </li>
                                                                            </ul>
                                                                        </li>

                                                                        <li>
                                                                            <ul>
                                                                                <li class="w-sm"><label>Заголовок</label></li>
                                                                                <li class="w-lg"><input class="lg-input input_white_form_2" type="text" value="" name="title_advert" id=""></li>
                                                                            </ul>
                                                                        </li>

                                                                        <li>
                                                                            <ul>
                                                                                <li class="w-sm"><label>Описание</label></li>
                                                                                <li class="w-lg"><textarea  name="content_advert"></textarea></li>
                                                                            </ul>
                                                                        </li>

                                                                        <li id="advert_image_container">
                                                                            <ul>
                                                                                <li class="w-sm">
                                                                                    <label>Фотографии</label>
                                                                                </li>
                                                                                <li class="w-lg">
                                                                                    <div class="file-container"><input class="image_file_load" type="file" data-st="0" title="Выберите файл" name="imageForAdvert[]" accept="image/x-png, image/gif, image/jpeg" /></div>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
<!--                                                                        <li class="w-lg"><a href="#" class="addNewImage">Добавить еще один файл</a></li>-->

                                                                        <li>
                                                                            <ul>
                                                                                <li class="w-sm">Капча:</li>
                                                                                <li class="w-lg">
                                                                                    <div>
                                                                                        <img id="captcha-image" src="<?php echo get_template_directory_uri(); ?>/inc/captcha/generate_captcha.php?<?php echo time(); ?>" width="132" height="46" alt="Captcha image" />
                                                                                        <input type="text" name="captcha" id="captcha" />
                                                                                        <br><a id="refresh-captcha" href="#">Обновить изображение</a>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                        <li>
                                                                            <ul>
                                                                                <li class="w-sm"> </li>
                                                                                <li class="w-lg"><input type="submit" value="Отправить объявление"></li>
                                                                            </ul>
                                                                        </li>

                                                                    </ul>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>

                                                <div class="clear"></div>
                                            </div>
                                        </section>

                                        <?php
                                        wp_reset_postdata();
                                        ?>

                                    </div>
                                </div>

                            </div>

                            <div id="sidebar-wrap" class="">
                                <div  class="scolumn">
                                </div>
                                <div id="sidebar1" class="scolumn">
                                    <div class="scolumn-pad">    </div>
                                </div>
                                <section id="sb_primary">
                                    <div class="copy-pad">
                                        <ul id="list_sb_primary"
                                            class="sidebar_widgets fix"><?php if (!dynamic_sidebar('sidebar-1')) : ?><?php endif; ?></ul>
                                        <div class="clear"></div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="morefoot_area" class="container-group">
        <section id="sb_footcols" class="container no_clone section-sb_footcols fix">
            <div class="texture">
                <div class="content">
                    <div class="content-pad">
                        <div class="fcolumns ppfull pprow">
                            <div class="fcolumns-pad fix"><?php if (!dynamic_sidebar('footer')) : ?><?php endif; ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="clear"></div>

</div>

</div>
</div>


<script>
    jQuery(document).ready(function ($) {
        $(document).on('change','#acf-field-adverts_sub_cat',function (e) {
            _sel=$(this).parent().html();
            _term_id = $(this).find('option:selected').data('catid');
            $.post("/wp-admin/admin-ajax.php", {'action': 'get_advert_params', 'id_term': _term_id, 'page':'nofilter'}, function (data) {
                $('.divline select').each(function(i,obj){
                    if(i!=0)
                        $(obj).remove();
                })
                $('.divline input').each(function(i,obj){
                        $(obj).remove();
                })
                $('.divline').append(data);
            });
        });
        $('#acf-field-adverts_cat').change(function(e){
            _term_id = $(this).find('option:selected').data('catid');
            $.post("/wp-admin/admin-ajax.php", {'action': 'get_advert_subCat', 'id_term': _term_id}, function (data) {
                $('.divline').html(data);
                $('#subCategory_and_params').html('');
            });
        });

        $('body').on('change', '.image_file_load', function(){
            var len = $('#advert_image_container li.w-lg > div').length;

            if(len < 10 && len == $(this).parent().index() + 1){
                $(this).parents('.file-container').append('<span class="deleteNewImage" title="Удалить">&#215;</span>');
                $('#advert_image_container li.w-lg').append('<div class="file-container"><input type="file" class="image_file_load" name="imageForAdvert[]" accept="image/x-png, image/gif, image/jpeg"></div>');

                $(':file').nicefileinput({
                    label: 'Добавить изображение'
                });
            }
        });

        $(document).on('click','#advert_image_container .deleteNewImage',function (e) {
            e.preventDefault();
            $(this).parent().fadeOut(function(){
                $(this).remove();
            });
            return false
        });
    });
</script>

<script src="<?php echo get_template_directory_uri() ?>/js/jquery.validate.min.js"></script>
<script>
    jQuery(document).ready(function($) {
        $('#refresh-captcha').click(function(e){
            e.preventDefault();

            $.get('/wp-admin/admin-ajax.php?', {'action': 'refresh_captcha'}, function(data){
                $('#captcha-image').attr('src', 'data:image/png;base64,'+data);
            }, 'text');
        });

        $(':file').nicefileinput({
            label: 'Добавить изображение'
        });

        $("#add-form").validate({
            errorClass: "w-info-error",
            rules: {
//                'user_advert[name]': "required",
                'title_advert':{
                    required:true
                },
                'adverts_cat':{
                    required:true
                },
                'content_advert':{
                    required:true
//                    minlength: 50
                },
                'adverts_sub_cat':{
                    required:true
                },
//                'user_advert[mail]': {
//                    required: true,
//                    email: true
//                },
                'fields[price_advert]':{
                    maxlength: 20
                },
                'captcha': {
                    required: true,
                    remote: "/wp-admin/admin-ajax.php/?action=check_captcha"
                }
            },
            messages: {
                'fields[price_advert]':'Слишком большая цена',
                'adverts_sub_cat': "Выберите подкатегорию",
                'content_advert': "Введите описание",
                'adverts_cat': "Выберите одну из категорий",
                'title_advert': "Введите корректное название",
                'user_advert[name]': "Введите свое имя",
                'user_advert[phone]': "Введите корректный номер телефона",
                'user_advert[mail]': "Введите корректный e-mail",
                'captcha': "  Символы введены неверно"
            },
            submitHandler: function(form) {                
				console.log($('#add-form input[type=submit]').attr('disabled','disabled'));				
				form.submit();
            }
        });
    });
</script>
<?php
get_filename();
get_footer();
?>
