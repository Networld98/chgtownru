UPDATE `wp_posts`
    INNER JOIN `wp_term_relationships` 
        ON (`wp_posts`.`ID` = `wp_term_relationships`.`object_id`)
    INNER JOIN `wp_term_taxonomy` 
        ON (`wp_term_relationships`.`term_taxonomy_id` = `wp_term_taxonomy`.`term_taxonomy_id`)
    INNER JOIN `wp_terms` 
        ON (`wp_terms`.`term_id` = `wp_term_taxonomy`.`term_id`)
   SET `wp_posts`.`post_type` = "news"
WHERE (`wp_terms`.`term_id` = "9")



SELECT
    `wp_posts`.`ID` AS `post_id`
    , `wp_posts`.`post_title` AS `post_title`
    , `wp_terms`.`name` AS `term_name`
    , `wp_terms`.`term_id` AS `term_id`
    , `wp_posts`.`post_status` AS `post_status`
FROM
    `wp_posts`
    INNER JOIN `wp_term_relationships` 
        ON (`wp_posts`.`ID` = `wp_term_relationships`.`object_id`)
    INNER JOIN `wp_term_taxonomy` 
        ON (`wp_term_relationships`.`term_taxonomy_id` = `wp_term_taxonomy`.`term_taxonomy_id`)
    INNER JOIN `wp_terms` 
        ON (`wp_terms`.`term_id` = `wp_term_taxonomy`.`term_id`)
WHERE (`wp_terms`.`term_id` = "9");


UPDATE `wp_term_taxonomy`
SET `taxonomy` = "tag"
WHERE (`taxonomy` = "post_tag" )