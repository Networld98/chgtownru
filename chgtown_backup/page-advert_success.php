<?php
get_header();
get_filename();
?>
<div id="page-main" class="container-group">
    <div id="dynamic-content" class="outline">

        <section id="content" class="container no_clone section-content-area fix">
            <div class="texture">
                <div class="content">
                    <div class="content-pad">
                        <div id="pagelines_content" class="one-sidebar-right fix">
                            <div id="column-wrap" class="fix">
                                <div id="column-main" class="mcolumn fix">
                                    <div class="mcolumn-pad">
                                        <section id="postloop" class="copy no_clone section-postloop">
                                            <div class="copy-pad">
                                                <article <?php post_class('fpost'); ?>>
                                                    <div class="hentry-pad">
                                                        <div class="entry_wrap fix">
                                                            <div class="entry_content">
                                                                    <div class="w-status-info">
                                                                        <label> Вы успешно зарегистрировались!</label>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </article>

                                                <div class="clear"></div>
                                            </div>
                                        </section>

                                        <?php
                                        wp_reset_postdata();
                                        ?>

                                    </div>
                                </div>

                            </div>

                            <div id="sidebar-wrap" class="">
                                <div  class="scolumn">
                                </div>
                                <div id="sidebar1" class="scolumn">
                                    <div class="scolumn-pad">    </div>
                                </div>
                                <section id="sb_primary">
                                    <div class="copy-pad">
                                        <ul id="list_sb_primary"
                                            class="sidebar_widgets fix"><?php if (!dynamic_sidebar('sidebar-1')) : ?><?php endif; ?></ul>
                                        <div class="clear"></div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="morefoot_area" class="container-group">
        <section id="sb_footcols" class="container no_clone section-sb_footcols fix">
            <div class="texture">
                <div class="content">
                    <div class="content-pad">
                        <div class="fcolumns ppfull pprow">
                            <div
                                class="fcolumns-pad fix"><?php if (!dynamic_sidebar('footer')) : ?><?php endif; ?></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="clear"></div>

</div>

</div>
</div>
<?php
get_filename();
get_footer();
?>
