<?php get_filename(); ?>

	<section id="features" class="copy no_clone section-features">
		<div class="copy-pad">
			<div id="feature_slider" class="fclone_wrap features fix">
				<div id="feature-area" class="fset_height">
					<div id="cycle" class="fclone">
						<?php
						$custom_query = 'slider_query';
						$args = array(
							'post_type' => 'slider',
							'posts_per_page' => 10
						);
						${$custom_query} = new WP_Query( $args );
						while ( ${$custom_query}->have_posts() ) : ${$custom_query}->the_post();
							$image_id = get_post_meta( $post->ID, 'to_picture', true );
							$image_src = wp_get_attachment_image_src( $image_id, 'slider' );
							$image_url = $image_src[0];
							$url = get_post_meta( $post->ID, 'to_url', true );
							?>

							<div class="fcontainer text-left fstyle-darkbg-overlay fix">
								<?php
								$html = '';
								if ( !empty( $url ) ) {
									$html .= '<a class="feature-wrap fset_height bg_cover" href="' . $url . '" style="background-image: url(\'' . $image_url . '\');background-size:100% 100%;" >';
								} else {
									$html .= '<div class="feature-wrap fset_height bg_cover" style="background-image: url(\'' . $image_url . '\');background-size:100% 100%;">';
								}

								$content = get_the_content();

								if ( !empty( $content ) ) {
									$html .= '<div class="feature-pad fset_height fix">
																				<div class="fcontent scale_text fset_height">
																					<div class="dcol-pad fix">
																						<div class="fheading"> <h2 class="ftitle">' . get_the_title() . '</h2> </div>
																						<div class="ftext"><div class="fexcerpt">' . $content . '</div></div>
																					</div>
																				</div>
																				<div class="fmedia fset_height" style=""><div class="dcol-pad"></div></div>
																				<div class="clear"></div>
																			</div>';
								}

								if ( !empty( $url ) ) {
									$html .= '</a>';
								} else {
									$html .= '</div>';
								}
								echo $html;
								?>
							</div>

						<?php
						endwhile;
						wp_reset_postdata();
						?>
					</div>
				</div>
				<div id="feature-footer" class="features names fix">
					<div class="feature-footer-pad"><span class="playpause pause fclone"><span>&nbsp;</span></span>

						<div id="featurenav" class="fclone subtext fix"></div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
			<div class="clear"></div>
		</div>
	</section>

<?php get_filename(); ?>