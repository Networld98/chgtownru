<?php if ( ! is_user_logged_in() ) : ?>
	<section id="auth-popup">
		<span class="close">&times;</span>

		<div class="auth-message error"></div>

		<?php
		global $confirmation_email_message;

		if ( is_array( $confirmation_email_message ) ) {
			echo "<div id=\"confirmation_email_message\" class=\"auth-message ". ($confirmation_email_message['success'] ? 'success' : 'error') ."\" style=\"display: block;\">{$confirmation_email_message['msg']}</div>";
		}
		?>

		<div id="regform" class="iblock">
			<form>
				<input type="hidden" name="action" value="register">

				<div class="field">
					<input type="text" name="name" placeholder="Отображаемое Имя">
				</div>

				<div class="field">
					<input type="text" name="email" placeholder="E-mail">
				</div>

				<div class="field">
					<input type="password" name="pass" placeholder="Пароль">
				</div>

				<div class="field buttons">
					<input type="submit" name="submit" value="Зарегистрироваться">
				</div>
			</form>

			<div class="another-action">
				<a href="#authform" class="open-auth-popup">Авторизируйтесь</a>
			</div>
		</div>

		<div id="authform" class="iblock">
			<form>
				<input type="hidden" name="action" value="login">

				<div class="field">
					<input type="text" name="login" placeholder="E-mail">
				</div>

				<div class="field">
					<input type="password" name="pass" placeholder="Пароль">
				</div>

				<div class="field buttons">
					<input type="submit" name="submit" value="Войти">
				</div>
				<br>

				<div class="field remember">
					<label>
						<input type="checkbox" name="remember" value="1">
						Запомнить меня
					</label>
				</div>

				<div class="field">
					<a class="open-auth-popup" href="#lostpasswordform">Забыли пароль?</a>
				</div>
			</form>

			<div class="another-action ulogin_panel">
				<p>Войти через соцсети:</p>
				<?php echo ulogin_panel(); ?>
			</div>

			<div class="another-action">
				<a href="#regform" class="open-auth-popup">Зарегистрируйтесь</a>
			</div>
		</div>

		<div id="lostpasswordform" class="iblock">
			<form>
				<input type="hidden" name="action" value="lost_password">

				<div class="field">
					<input type="text" name="login" placeholder="E-mail">
				</div>

				<div class="field buttons">
					<input type="submit" name="submit" value="Восстановить пароль">
				</div>

				<p>На вашу почту придет письмо со ссылкой на страницу восстановления пароля</p>
			</form>

			<div class="another-action ulogin_panel">
				<p>Войти через соцсети:</p>
				<?php echo ulogin_panel(); ?>
			</div>

			<div class="another-action">
				<a href="#regform" class="open-auth-popup">Зарегистрируйтесь</a>
			</div>
		</div>
	</section>
<?php endif; ?>