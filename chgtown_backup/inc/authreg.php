<?php

add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );
function filter_plugin_updates( $value ) {
	unset( $value->response['ulogin/ulogin.php'] );

	return $value;
}

add_action( 'wp_enqueue_scripts', 'add_scripts' );
function add_scripts() {
	wp_enqueue_script( 'jquery-validate', THEME_URL . '/js/jquery.validate.min.js', array( 'jquery' ), '1.11.1', true );
}

// Forms handler
add_action( 'wp_ajax_authreg', 'authreg_handle' );
add_action( 'wp_ajax_nopriv_authreg', 'authreg_handle' );
function authreg_handle() {
	$data = array();
	foreach ( $_POST['data'] as $postdata ) {
		$data[ $postdata["name"] ] = $postdata["value"];
	}

	$function = $data['action'] . '_handle';

	if ( function_exists( $function ) ) {
		$function( $data );
	}
}

function login_handle( $postdata ) {
	$creds = array(
		'user_login'    => $postdata['login'],
		'user_password' => $postdata['pass'],
		'remember'      => $postdata['remember']
	);

	$user = wp_signon( $creds, false );

	if ( is_wp_error( $user ) ) {
		if ( in_array( $user->get_error_code(), array( 'incorrect_password', 'invalid_username' ) ) ) {
			$error_message = 'Неверное имя пользователя или пароль.';
		}
		else {
			$error_message = $user->get_error_message();
		}

		wp_send_json_error( array( 'msg' => $error_message ) );
	}

	wp_send_json_success( array( 'redirect' => 'reload' ) );
}

function register_handle( $postdata ) {
	if ( username_exists( $postdata['name'] ) ) {
		wp_send_json_error( array( 'msg' => 'Пользователь с таким именем уже существует.' ) );
	}

	if ( email_exists( $postdata['email'] ) ) {
		wp_send_json_error( array( 'msg' => 'Пользователь с таким e-mail уже существует.' ) );
	}

	$name = $lastname = explode( ' ', $postdata['name'] );

	unset( $lastname[0] );
	$lastname = implode( ' ', $lastname );

	$user_id = wp_insert_user( array(
		'user_login'           => sanitize_title( $postdata['email'] ),
		'user_email'           => $postdata['email'],
		'user_pass'            => $postdata['pass'],
		'nickname'             => $name[0],
//		'display_name'         => $name[0],
		'first_name'           => $name[0],
		'last_name'            => $lastname,
		'show_admin_bar_front' => 'false',
		'role'                 => 'subscriber'
	) );

	if ( is_wp_error( $user_id ) ) {
		wp_send_json_error( array( 'msg' => $user_id->get_error_message() ) );
	}

	new_user_set_activation_key( $user_id );

	wp_send_json_success( array( 'msg' => 'На ваш e-mail выслано письмо для подтверждения регистрации.' ) );
}

function lost_password_handle( $postdata ) {
	global $wpdb, $wp_hasher;

	$user_data = null;

	if ( is_email( $postdata['login'] ) ) {
		$user_data = get_user_by( 'email', trim( $postdata['login'] ) );
	}
	/*else {
		$user_data = get_user_by( 'login', trim( $postdata['login'] ) );
	}*/

	if ( empty( $user_data ) ) {
		wp_send_json_error( array( 'msg' => 'Пользователя с таким Ником или E-mail адресом в системе нет.' ) );
	}

	$key = wp_generate_password( 20, false );

	if ( empty( $wp_hasher ) ) {
		require_once ABSPATH . 'wp-includes/class-phpass.php';
		$wp_hasher = new PasswordHash( 8, true );
	}

	$hashed_key = $wp_hasher->HashPassword( $key );

	$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed_key ), array( 'ID' => $user_data->ID ) );

	user_notification_reset_pass( $user_data->ID, $key );

	wp_send_json_success( array( 'msg' => 'На ваш адрес выслана ссылка для восстановления пароля' ) );
}

function reset_pass_handle( $postdata ) {
	$postdata['login'] = urldecode( $postdata['login'] );
	$user              = check_password_reset_key( $postdata['key'], $postdata['login'] );

	if ( is_wp_error( $user ) ) {
		wp_send_json_error( array( 'msg' => $user->get_error_message() ) );
	}

	reset_password( $user, $postdata['pass'] );

	$creds = array(
		'user_login'    => $postdata['login'],
		'user_password' => $postdata['pass'],
		'remember'      => 1
	);
	wp_signon( $creds );

	wp_send_json_success( array( 'msg' => '<p>Пароль успешно изменен.</p><p>Вы вошли под именем ' . $postdata['login'] . '</p>' ) );
}

//==============================================================================================================================================================

add_action( 'init', 'add_custom_rewrite_rule' );
function add_custom_rewrite_rule() {
	add_rewrite_rule( '^auth/(email-confirmation|retrieve-password)/([^/]*)/([^/]*)/?$', 'index.php?auth_action=$matches[1]&auth_login=$matches[2]&auth_key=$matches[3]', 'top' );
	flush_rewrite_rules();
}

add_filter( 'query_vars', 'add_query_vars' );
function add_query_vars( $vars ) {
	$vars[] = 'auth_action';
	$vars[] = 'auth_key';
	$vars[] = 'auth_login';

	return $vars;
}

add_filter( 'template_include', 'auth_template_loader' );
function auth_template_loader( $template ) {
	switch ( get_query_var( 'auth_action' ) ) {
		case 'email-confirmation':
			auth_action_email_confirmation();

			$template = locate_template( array( 'front-page.php' ) );
			break;

		case 'retrieve-password':
			global $auth_message;

			$user = check_password_reset_key( get_query_var( 'auth_key' ), urldecode( get_query_var( 'auth_login' ) ) );

			if ( is_wp_error( $user ) ) {
				$auth_message = $user->get_error_message();
			}

			$template = locate_template( array( 'auth-reset-pass.php' ) );
			break;
	}

	return $template;
}

// Разрешение авторизации по e-mail
add_action( 'wp_authenticate', 'login_with_email_address' );
function login_with_email_address( $username ) {
	if ( is_email( $username ) ) {
		$user = get_user_by( 'email', $username );

		if ( ! empty( $user->user_login ) ) {
			$username = $user->user_login;
		}
	}

	return $username;
}

// Проверка подтверждения статуса юзера при авторизации
add_filter( 'wp_authenticate_user', 'check_user_email_confirmation' );
function check_user_email_confirmation( $user ) {
	if ( $user->user_status == '2' ) {
		$user = new WP_Error( 1, 'Вы не подтвердили email адрес.' );
	}

	return $user;
}

function auth_action_email_confirmation() {
	global $wpdb, $confirmation_email_message;

	$user = check_password_reset_key( get_query_var( 'auth_key' ), urldecode( get_query_var( 'auth_login' ) ) );

	if ( ! is_wp_error( $user ) ) {
		$wpdb->update( $wpdb->users, array(
			'user_activation_key' => '',
			'user_status'         => 0
		), array( 'ID' => $user->ID ) );

		$creds = array(
			'user_login'    => $user->user_login,
			'user_password' => $user->user_pass,
			'remember'      => 1
		);
		wp_signon( $creds );

		$confirmation_email_message = array( 'success' => true, 'msg' => "Ваш адрес электронной почты успешно подтверждён." );
	}
	else {
		$confirmation_email_message = array( 'success' => false, 'msg' => $user->get_error_message() );
	}
}

function new_user_set_activation_key( $user_id ) {
	global $wpdb, $wp_hasher;

	$key = wp_generate_password( 20, false );

	if ( empty( $wp_hasher ) ) {
		require_once ABSPATH . 'wp-includes/class-phpass.php';
		$wp_hasher = new PasswordHash( 8, true );
	}

	$hashed_key = $wp_hasher->HashPassword( $key );

	$wpdb->update( $wpdb->users, array(
		'user_activation_key' => $hashed_key,
		'user_status'         => 2
	), array( 'ID' => $user_id ) );

	wp_new_user_notification( $user_id );

	user_notification_email_confirm( $user_id, $key );
}

function user_notification_email_confirm( $user_id, $key ) {
	$user = get_userdata( $user_id );

	$message = "
Здравствуйте, {$user->user_login}\r\n
Спасибо за регистрацию на сайте " . get_bloginfo( 'name' ) . ".
Для завершения регистрации, пожалуйста, перейдите по этой ссылке:\r\n
" . site_url( "auth/email-confirmation/" . rawurlencode( $user->user_login ) . "/{$key}/" ) . "\r\n";

	wp_mail( $user->user_email, "Подтверждение E-mail", $message );
}

function user_notification_reset_pass( $user_id, $key ) {
	$user = get_userdata( $user_id );

	$message = "
Здравствуйте, {$user->user_login}\r\n
Чтобы сменить ваш пароль на сайте " . get_bloginfo( 'name' ) . ", пожалуйста, перейдите по этой ссылке:\r\n
" . site_url( "auth/retrieve-password/" . rawurlencode( $user->user_login ) . "/{$key}/" ) . "\r\n";

	wp_mail( $user->user_email, "Восстановление пароля", $message );
}

add_filter( 'comment_reply_link', 'custom_comment_reply_link', 10, 4 );
function custom_comment_reply_link( $link, $args, $comment, $post ) {
	if ( $link && get_option( 'comment_registration' ) && ! is_user_logged_in() ) {
		$link = '<a rel="nofollow" class="comment-reply-login open-auth-popup" href="#authform">' . $args['login_text'] . '</a>';
	}

	return $link;
}